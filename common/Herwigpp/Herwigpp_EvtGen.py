## Run EvtGen afterburner on top of Pythia 8
assert hasattr(genSeq, "Herwigpp")
include("MC15JobOptions/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['HerwigppInclusiveP8.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "HerwigppInclusiveP8.pdt"

# quick "fix" to the mismatch between Herwig 7 and EvtGen of the masses below
cmds1 = """
set /Herwig/Particles/B'_c1+:NominalMass 7.3
set /Herwig/Particles/B'_c1-:NominalMass 7.3
set /Herwig/Particles/B_c1+:NominalMass 7.3
set /Herwig/Particles/B_c1-:NominalMass 7.3
set /Herwig/Particles/B_c2+:NominalMass 7.35
set /Herwig/Particles/B_c2-:NominalMass 7.35
set /Herwig/Particles/B*_c0-:NominalMass 7.25
set /Herwig/Particles/B*_c0-:NominalMass 7.25
"""
genSeq.Herwigpp.Commands += cmds1.splitlines()

del cmds1
