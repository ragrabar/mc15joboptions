﻿include("MC15JobOptions/AntiKt4TruthJets_pileup.py")

if not hasattr( filtSeq, "MultiBjetFilter" ):
        from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
        filtSeq += MultiBjetFilter()
        pass

# Example configuration
"""
MultiBjetFilter = filtSeq.MultiBjetFilter
MultiBjetFilter.LeadJetPtMin = 0
MultiBjetFilter.LeadJetPtMax = 50000
MultiBjetFilter.BottomPtMin = 5.0
MultiBjetFilter.BottomEtaMax = 3.0
MultiBjetFilter.JetPtMin = 15000
MultiBjetFilter.JetEtaMax = 2.7
MultiBjetFilter.DeltaRFromTruth = 0.3
MultiBjetFilter.TruthContainerName = "AntiKt4TruthJets"
"""
