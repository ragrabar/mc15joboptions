from MadGraphControl.MadGraphUtils import *

# General settings
nevents=150000
mode=0
nJobs=1
doCluster=False
doGridPack=False
gridpack_mode=False
gridpack_dir=None

if doGridPack:
    gridpack_mode=True
    gridpack_dir='madevent/'
cluster_type=None
cluster_queue=None
# Merging settings
maxjetflavor=5
ickkw=0
nJetMax=4
ktdurham=30      
dparameter=0.2   

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    nJobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = nJobs
    print opts
    njobs=nJobs
    mode=2

### DSID lists (extensions can include filters, systematics samples, etc.)
Wenu_5fl_NpX=[311445,311446,311451]

Wmunu_5fl_NpX=[311447,311448,311452]

Wtaunu_5fl_NpX=[311449,311450,311453] 

nevents_forHT={'lowHT':50000,'midlowHT':90000,'midHT':150000,'midhighHT':150000,'highHT':130000}

minEvents=200
if runArgs.runNumber in [311445, 311446, 311449]:
    minEvents=200
elif runArgs.runNumber in [311447,311448,311450,311451,311452,311453]:
    minEvents=1000

# Setting min events
evgenConfig.minevents=minEvents

### Electrons
if runArgs.runNumber in Wenu_5fl_NpX:
    mgproc="""
generate p p > eall veall @0
add process p p > eall veall j @1
add process p p > eall veall j j @2
add process p p > eall veall j j j @3
"""

    name='Wenu_NpX'
    process="pp>LEPTONS,NEUTRINOS"
    
    nevents=nevents_forHT[HTrange]
    if doGridPack:
        gridpack_mode=True
        gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='lsf'
        cluster_queue='8nh'
        nJobs=100

### Muons    
elif runArgs.runNumber in Wmunu_5fl_NpX:
    mgproc="""
generate p p > muall vmall @0
add process p p > muall vmall j @1
add process p p > muall vmall j j @2
add process p p > muall vmall j j j @3
"""
    name='Wmunu_NpX'
    process="pp>LEPTONS,NEUTRINOS"
    
    nevents=nevents_forHT[HTrange]
    if doGridPack:
        gridpack_mode=True
        gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='pbs'
        cluster_queue='medium'
        nJobs=20
    
### Taus
elif runArgs.runNumber in Wtaunu_5fl_NpX:
    mgproc="""
generate p p > taall vtall @0
add process p p > taall vtall j @1
add process p p > taall vtall j j @2
add process p p > taall vtall j j j @3
"""
    name='Wtaunu_NpX'
    process="pp>LEPTONS,NEUTRINOS"
    
    nevents=nevents_forHT[HTrange]
    if doGridPack:
        gridpack_mode=True
        gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='pbs'
        cluster_queue='medium'
        nJobs=20
    
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define eall = e+ e-
define veall = ve ve~
define muall = mu+ mu-
define vmall = vm vm~
define taall = ta+ ta-
define vtall = vt vt~
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'    : '3.0',
           'cut_decays'     : 'F', 
           'pdlabel'        : "'lhapdf'",
           'lhaid'          : 260000,  
           'maxjetflavor'   : maxjetflavor,
           'asrwgtflavor'   : maxjetflavor,
           'ickkw'          : 0,
           'ptj'            : 20,      
           'ptb'            : 20,          
           'ihtmin'         : ihtmin,
           'ihtmax'         : ihtmax,
           'mmjj'           : 0,
           'drjj'           : 0,
           'drll'           : 0,
           'drjl'           : 0.,       
           'ptl'            : 0,
           'etal'           : 10,
           'etab'           : 6,
           'etaj'           : 6,
           'ptllmin'        : 100.0,
           'ktdurham'       : ktdurham,    
           'dparameter'     : dparameter,
           'event_norm'     : 'sum',
           'use_syst'       : 'T'}
##########################################################################################
process_dir = new_process(grid_pack=gridpack_dir)
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)

print_cards()
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,
         grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,cluster_type=cluster_type,cluster_queue=cluster_queue,
         nevents=nevents,random_seed=runArgs.randomSeed)

arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz')
#### Shower 
evgenConfig.contact  = [ "nicolas.gilberto.gutierrez.ortiz@cern.ch" ]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=['W','jets']
evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
if hasattr(runArgs,'outputTXTFile'):                                                                                                                         
    runArgs.inputGeneratorFile=runArgs.outputTXTFile
    evgenConfig.inputfilecheck=runArgs.outputTXTFile.split('.tar.gz')[0]  


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")    
include("MC15JobOptions/Pythia8_MadGraph.py")

PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process                                                      
PYTHIA8_nQuarksMerge=maxjetflavor
include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands += ["Merging:unorderedASscalePrescrip = 0"]


include("MC15JobOptions/VBFMjjIntervalFilter.py")
    
filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0 
filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 35.*GeV
filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 35.*GeV
filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
filtSeq.VBFMjjIntervalFilter.LowMjj = 800.*GeV
filtSeq.VBFMjjIntervalFilter.HighMjj = 14000.*GeV
    
filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = True
filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = False
filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = False
filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = False
filtSeq.VBFMjjIntervalFilter.ApplyWeighting = False;   
filtSeq.VBFMjjIntervalFilter.ApplyDphi = True; 
