from MadGraphControl.MadGraphUtils import *

# merging parameters
bwcutoff=15
dparameter=0.4
nJetMax=1
maxjetflavor=4
ptj=0
ickkw_value=0


beamEnergy = -999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")

# generation parameters

param_card_extras = {
      "ALPPARS": { 'CWtil':CWtil, 'fa':fa, 'CBtil': -0.28702319343*CWtil, 'CaPhi': CaPhi},
      "MASS": { 'Ma':Ma }
      }

# writting proc_ard

fcard = open('proc_card_mg5.dat','w')

fcard.write("""
import model sm
import model ALP_invisible_linear_UFO
display particles
display multiparticles
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
display interactions
generate p p > ax a
output -f""")
fcard.close()


#Missing Et Filter
print 'evgenlog.info'
include('MC15JobOptions/MissingEtFilter.py')
print 'included the filter py'
filtSeq.Expression = "MissingEtFilter"




runName='run_01'     
process_dir = new_process()

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'  :'3.0',
          'pdlabel'      :"'nn23lo1'",
          'lhaid'        :247000,
          'ickkw'        :ickkw_value,
          'maxjetflavor' :maxjetflavor,
          'asrwgtflavor' :maxjetflavor,
          'ptj'          :ptj,
	  'pta'          :0.0,
          'drjj'         :0.0,
          'etaj'         :5,
          'etab'         :5,
	  'etaa'         :5,
          'ktdurham'     :ktdurham,
          'dparameter'   :dparameter,
          'bwcutoff'     :bwcutoff,
          'scalefact'    :scalefact,
          'alpsfact'     :alpsfact,
	      'ptgmin'       :ptgmin
}


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut = qcut,
               nevts=runArgs.maxEvents*extraEvents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


if( build_param_card(param_card_old=process_dir + "/Cards/param_card.dat",param_card_new='param_card.dat',params=param_card_extras) == -1):
    raise RuntimeError("Could not create param_card.dat")
    
print_cards()
    
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00000.events.tar.gz',lhe_version=3)
  

############################
# Shower JOs will go here

evgenConfig.description = 'ALP linear to photon'
evgenConfig.keywords+=['monophoton','exotic','BSM']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00000.events.tar.gz'
evgenConfig.contact = ["Sergio Gonzalez <sergio.gonzalez.fernandez@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process='pp>axa'
PYTHIA8_TMS=ktdurham
PYTHIA8_nQuarksMerge=maxjetflavor
#include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")

genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on"]

bonus_file = open('pdg_extras.dat','w')
bonus_file.write('51\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'
