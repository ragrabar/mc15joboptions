from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WZ', 'electroweak', '3lepton', '2jet']
evgenConfig.contact = ['despoina.sampsonidou@cern.ch']

gridpack_dir='madevent/'

# ---------------------------------------------------------------------------
# Process type based on runNumber:
# ---------------------------------------------------------------------------

if runArgs.runNumber == 367515:
	runName = 'epemepvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367516:
	runName = 'epememvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367517:
	runName = 'epemmupvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367518:
	runName = 'epemmumvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367519:
	runName = 'epemtapvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367520:
	runName = 'epemtamvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367521:
	runName = 'mupmumepvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367522:
	runName = 'mupmumemvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367535:
	runName = 'mupmummupvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367536:
	runName = 'mupmummumvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367537:
	runName = 'mupmumtapvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367538:
	runName = 'mupmumtamvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367539:
	runName = 'taptamepvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367540:
	runName = 'taptamemvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367541:
	runName = 'taptammupvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367542:
	runName = 'taptammumvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367543:
	runName = 'taptamtapvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367544:
	runName = 'taptamtamvjj_EW6_LSMT_t0_T0_2p40_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 T0^2==1
"""
elif runArgs.runNumber == 367545:
	runName = 'epemepvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367546:
	runName = 'epememvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367547:
	runName = 'epemmupvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367548:
	runName = 'epemmumvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367549:
	runName = 'epemtapvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367550:
	runName = 'epemtamvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367551:
	runName = 'mupmumepvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367552:
	runName = 'mupmumemvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367553:
	runName = 'mupmummupvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367554:
	runName = 'mupmummumvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367555:
	runName = 'mupmumtapvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367556:
	runName = 'mupmumtamvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367557:
	runName = 'taptamepvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367558:
	runName = 'taptamemvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367559:
	runName = 'taptammupvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367560:
	runName = 'taptammumvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367561:
	runName = 'taptamtapvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367562:
	runName = 'taptamtamvjj_EW6_LSMT_t1_T1_1p60_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 T1^2==1
"""
elif runArgs.runNumber == 367563:
	runName = 'epemepvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367564:
	runName = 'epememvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367565:
	runName = 'epemmupvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367566:
	runName = 'epemmumvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367567:
	runName = 'epemtapvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367568:
	runName = 'epemtamvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367569:
	runName = 'mupmumepvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367570:
	runName = 'mupmumemvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367571:
	runName = 'mupmummupvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367572:
	runName = 'mupmummumvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367573:
	runName = 'mupmumtapvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367574:
	runName = 'mupmumtamvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367575:
	runName = 'taptamepvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367576:
	runName = 'taptamemvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367577:
	runName = 'taptammupvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367578:
	runName = 'taptammumvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367579:
	runName = 'taptamtapvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367580:
	runName = 'taptamtamvjj_EW6_LSMT_t2_T2_5p50_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 T2^2==1
"""
elif runArgs.runNumber == 367581:
	runName = 'epemepvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367582:
	runName = 'epememvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367583:
	runName = 'epemmupvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367584:
	runName = 'epemmumvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367585:
	runName = 'epemtapvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367586:
	runName = 'epemtamvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367587:
	runName = 'mupmumepvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367588:
	runName = 'mupmumemvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367589:
	runName = 'mupmummupvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367590:
	runName = 'mupmummumvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367591:
	runName = 'mupmumtapvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367592:
	runName = 'mupmumtamvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367593:
	runName = 'taptamepvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367594:
	runName = 'taptamemvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367595:
	runName = 'taptammupvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367596:
	runName = 'taptammumvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367597:
	runName = 'taptamtapvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367598:
	runName = 'taptamtamvjj_EW6_LSMT_m0_M0_27_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 M0^2==1
"""
elif runArgs.runNumber == 367599:
	runName = 'epemepvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367600:
	runName = 'epememvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367601:
	runName = 'epemmupvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367602:
	runName = 'epemmumvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367603:
	runName = 'epemtapvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367604:
	runName = 'epemtamvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367605:
	runName = 'mupmumepvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367606:
	runName = 'mupmumemvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367607:
	runName = 'mupmummupvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367608:
	runName = 'mupmummumvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367609:
	runName = 'mupmumtapvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367610:
	runName = 'mupmumtamvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367611:
	runName = 'taptamepvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367612:
	runName = 'taptamemvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367613:
	runName = 'taptammupvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367614:
	runName = 'taptammumvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367615:
	runName = 'taptamtapvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367616:
	runName = 'taptamtamvjj_EW6_LSMT_m1_M1_28_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 M1^2==1
"""
elif runArgs.runNumber == 367617:
	runName = 'epemepvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367618:
	runName = 'epememvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367619:
	runName = 'epemmupvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367620:
	runName = 'epemmumvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367621:
	runName = 'epemtapvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367622:
	runName = 'epemtamvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367623:
	runName = 'mupmumepvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367624:
	runName = 'mupmumemvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367625:
	runName = 'mupmummupvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367626:
	runName = 'mupmummumvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367627:
	runName = 'mupmumtapvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367628:
	runName = 'mupmumtamvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367629:
	runName = 'taptamepvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367630:
	runName = 'taptamemvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367631:
	runName = 'taptammupvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367632:
	runName = 'taptammumvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367633:
	runName = 'taptamtapvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367634:
	runName = 'taptamtamvjj_EW6_LSMT_s1_S1_128_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 S1^2==1
"""
elif runArgs.runNumber == 367635:
	runName = 'epemepvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- e+ vl j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367636:
	runName = 'epememvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- e- vl~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367637:
	runName = 'epemmupvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- mu+ vm j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367638:
	runName = 'epemmumvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- mu- vm~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367639:
	runName = 'epemtapvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- ta+ vt j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367640:
	runName = 'epemtamvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 S0^2==1 S2=0
add process p p > e+ e- ta- vt~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367641:
	runName = 'mupmumepvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- e+ vl j j  QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367642:
	runName = 'mupmumemvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- e- vl~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367643:
	runName = 'mupmummupvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- mu+ vm j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367644:
	runName = 'mupmummumvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- mu- vm~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367645:
	runName = 'mupmumtapvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- ta+ vt j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367646:
	runName = 'mupmumtamvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 S0^2==1 S2=0
add process p p > mu+ mu- ta- vt~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367647:
	runName = 'taptamepvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- e+ vl j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367648:
	runName = 'taptamemvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- e- vl~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367649:
	runName = 'taptammupvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- mu+ vm j j  QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367650:
	runName = 'taptammumvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- mu- vm~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367651:
	runName = 'taptamtapvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- ta+ vt j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367652:
	runName = 'taptamtamvjj_EW6_LSMT_s0_S02_82_int'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 S0^2==1 S2=0
add process p p > ta+ ta- ta- vt~ j j QCD=0 S2^2==1 S0=0
"""
elif runArgs.runNumber == 367653:
	runName = 'epemepvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e+ vl j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367654:
	runName = 'epememvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- e- vl~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367655:
	runName = 'epemmupvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu+ vm j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367656:
	runName = 'epemmumvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- mu- vm~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367657:
	runName = 'epemtapvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta+ vt j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367658:
	runName = 'epemtamvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > e+ e- ta- vt~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367659:
	runName = 'mupmumepvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e+ vl j j  QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367660:
	runName = 'mupmumemvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- e- vl~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367661:
	runName = 'mupmummupvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu+ vm j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367662:
	runName = 'mupmummumvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- mu- vm~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367663:
	runName = 'mupmumtapvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta+ vt j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367664:
	runName = 'mupmumtamvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > mu+ mu- ta- vt~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367665:
	runName = 'taptamepvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e+ vl j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367666:
	runName = 'taptamemvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- e- vl~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367667:
	runName = 'taptammupvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu+ vm j j  QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367668:
	runName = 'taptammumvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- mu- vm~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367669:
	runName = 'taptamtapvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta+ vt j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367670:
	runName = 'taptamtamvjj_EW6_LSMT_t1_sm'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > ta+ ta- ta- vt~ j j QCD=0 T1^2==0
"""
elif runArgs.runNumber == 367523:
	runName = 'lllvjj_EW6_LSMT_S02_82_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 S02^2==2 S2=0
add process p p > l+ l- l- vl~ j j QCD=0 S02^2==2 S2=0
add process p p > l+ l- l+ vl j j QCD=0 S2^2==2 S0=0
add process p p > l+ l- l- vl~ j j QCD=0 S2^2==2 S0=0
add process p p > l+ l- l+ vl j j QCD=0 S0^2==1 S2^2==1
add process p p > l+ l- l- vl~ j j QCD=0 S0^2==1 S2^2==1
"""
elif runArgs.runNumber == 367524:
	runName = 'lllvjj_EW6_LSMT_S1_128_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 S1^2==2
add process p p > l+ l- l- vl~ j j QCD=0 S1^2==2
"""
elif runArgs.runNumber == 367525:
	runName = 'lllvjj_EW6_LSMT_T0_2p40_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T0^2==2
add process p p > l+ l- l- vl~ j j QCD=0 T0^2==2
"""
elif runArgs.runNumber == 367526:
	runName = 'lllvjj_EW6_LSMT_T1_1p60_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T1^2==2
add process p p > l+ l- l- vl~ j j QCD=0 T1^2==2
"""
elif runArgs.runNumber == 367527:
	runName = 'lllvjj_EW6_LSMT_T2_5p50_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T2^2==2
add process p p > l+ l- l- vl~ j j QCD=0 T2^2==2
"""
elif runArgs.runNumber == 367528:
	runName = 'lllvjj_EW6_LSMT_M0_27_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 M0^2==2
add process p p > l+ l- l- vl~ j j QCD=0 M0^2==2
"""
elif runArgs.runNumber == 367529:
	runName = 'lllvjj_EW6_LSMT_M1_28_quad'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 M1^2==2
add process p p > l+ l- l- vl~ j j QCD=0 M1^2==2
"""
elif runArgs.runNumber == 367530:
	runName = 'lllvjj_EW6_LSMT_S02_82_S1_128_cross'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 S0^2==1 S1^2==1
add process p p > l+ l- l- vl~ j j QCD=0 S0^2==1 S1^2==1
add process p p > l+ l- l+ vl j j QCD=0 S2^2==1 S1^2==1
add process p p > l+ l- l- vl~ j j QCD=0 S2^2==1 S1^2==1
"""
elif runArgs.runNumber == 367531:
	runName = 'lllvjj_EW6_LSMT_T0_2p40_T1_1p60_cross'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T0^2==1 T1^2==1
add process p p > l+ l- l- vl~ j j QCD=0 T0^2==1 T1^2==1
"""
elif runArgs.runNumber == 367532:
	runName = 'lllvjj_EW6_LSMT_T0_2p41_T2_5p50_cross'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T0^2==1 T2^2==1
add process p p > l+ l- l- vl~ j j QCD=0 T0^2==1 T2^2==1
"""
elif runArgs.runNumber == 367533:
	runName = 'lllvjj_EW6_LSMT_T1_1p60_T2_5p50_cross'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 T1^2==1 T2^2==1
add process p p > l+ l- l- vl~ j j QCD=0 T1^2==1 T2^2==1
"""
elif runArgs.runNumber == 367534:
	runName = 'lllvjj_EW6_LSMT_M0_27_M1_28_cross'
	description = 'MadGraph_' + runName
	mgproc = """
generate p p > l+ l- l+ vl j j QCD=0 M0^2==1 M1^2==1
add process p p > l+ l- l- vl~ j j QCD=0 M0^2==1 M1^2==1
"""
else:
    raise RuntimeError(
        "runNumber %i not recognised in these jobOptions." % runArgs.runNumber)

evgenConfig.description = description

# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
import model SM_LSMT_Ind5_UFO
define l+ = e+ mu+ ta+
define  vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
""" + mgproc + """
output -f
""")
fcard.close()

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(5000 * safefactor)

#Fetch default LO run_card.dat and set parameters
extras = {
    'gridpack': '.true.',
    'pdlabel': "'lhapdf'",
    'lhaid': 260000,
    'dynamical_scale_choice': 2,
    'maxjetflavor': 5,
    'asrwgtflavor': 5,
    'auto_ptj_mjj': False,
    'cut_decays': True,
    'ptl': 4.0,
    'ptj': 15.0,
    'ptb': 15.0,
    'drbb': 0.2,
    'etal': 3.0,
    'etaj': 5.5,
    'etab': 5.5,
    'mmll': 0.0,
    'dral': 0.1,
    'drbj': 0.2,
    'drll': 0.2,
    'drbl': 0.2,
    'drjl': 0.2,
    'drjj': 0.2,
    'use_syst': True,
    'systematics_program': 'systematics',
    'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', xqcut=0,
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy)
modify_run_card(run_card='run_card.dat',
                run_card_backup='run_card_backup.dat',
                settings=extras)
print_cards()

param_card_name = 'common/param_card_' + str(runArgs.runNumber) + '.dat'

generate(required_accuracy=0.001,run_card_loc='run_card.dat',
         param_card_loc=param_card_name,mode=0,njobs=1,cluster_type=None,
         cluster_queue=None,proc_dir=process_dir,run_name=runName,grid_pack=True,
         gridpack_compile=False,gridpack_dir=gridpack_dir,
         nevents=nevents,random_seed=runArgs.randomSeed)

# replaced output_suffix+'._00001.events.tar.gz' with runArgs.outputTXTFile
arrange_output(run_name=runName,proc_dir=process_dir,
               outputDS=runArgs.outputTXTFile,lhe_version=3,saveProcDir=True)

## Provide config information for Pythia8
runArgs.inputGeneratorFile=runArgs.outputTXTFile

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# fix for VBS processes (requires version>=8.230)
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["SpaceShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["TimeShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=on"]
genSeq.Pythia8.Commands += ["TimeShower:limitPTmaxGlobal=on"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalRecoil=1"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoilMode=2"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalBranch=1"]
genSeq.Pythia8.Commands += ["TimeShower:weightGluonToQuark=1"]
genSeq.Pythia8.Commands += ["Check:epTolErr=1e-2"]
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

