cmdsBR = """
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio      0.5770
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio      0.0291
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio      0.00000
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio     0.000219
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio   0.0632
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio         0.0857
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio 0.00228
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio       0.0264
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio       0.2150
decaymode h0->Z0,gamma; 0.00154 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:BranchingRatio    0.00154
decaymode h0->s,sbar; 0.000246 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff On
set /Herwig/Particles/h0/h0->s,sbar;:BranchingRatio      0.000246
"""
# Set the command vector
genSeq.Herwigpp.Commands += cmdsBR.splitlines()

# clean up
del cmdsBR
