evgenConfig.minevents = 5000

import math
import re

#parse channel, component and mass from joboption name
jo_name = runArgs.jobConfig[0]
if "tautauhh" in jo_name:
    channel = "hadhad"
elif "tautaulh" in jo_name:
    channel = "lephad"
elif "tautaull" in jo_name:
    channel = "leplep"
else:
    raise Exception("Unable to parse subchannel from joboption name.")

# factor of events more needed as LHE because of channel filter (+5 sigma safety):
calc_factor = lambda n, br : (5*math.sqrt(n)+n)/(n*br)

if runArgs.maxEvents > 0:
    nEvents = runArgs.maxEvents
    evgenConfig.minevents = nEvents
else:
    nEvents = evgenConfig.minevents

if channel == "hadhad":
    lhe_factor = calc_factor(nEvents, 0.42)
elif channel == "lephad":
    lhe_factor = calc_factor(nEvents, 0.456)
elif channel == "leplep":
    lhe_factor = calc_factor(nEvents, 0.124)

nLHEvents = int(nEvents * lhe_factor)

include("MC15JobOptions/aMcAtNloControl_bbH4FS.py")
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch = 1",
                            "SpaceShower:pTmaxFudge = 1",
                            "SpaceShower:MEcorrections = off",
                            "TimeShower:pTmaxMatch = 1",
                            "TimeShower:pTmaxFudge = 1",
                            "TimeShower:MEcorrections = off",
                            "TimeShower:globalRecoil = on",
                            "TimeShower:limitPTmaxGlobal = on",
                            "TimeShower:nMaxGlobalRecoil = 1",
                            "TimeShower:globalRecoilMode = 2",
                            "TimeShower:nMaxGlobalBranch = 1.",
                            "Check:epTolErr = 1e-2",
                            '25:onMode = off',
                            '25:onIfMatch = 15 15']


# ... Filter H->VV->Children
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2

if channel == "lephad":
    filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
    filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]
elif channel == "hadhad":
    filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
    filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]
elif channel == "leplep":
    filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
    filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [11,13]
