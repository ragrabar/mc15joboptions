#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.05484272E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05423977E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414403E+03   # H
        36     2.00000000E+03   # A
        37     2.00171729E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989276E+03   # ~u_L
   2000002     4.99994935E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989276E+03   # ~c_L
   2000004     4.99994935E+03   # ~c_R
   1000005     4.99924746E+03   # ~b_1
   2000005     5.00091175E+03   # ~b_2
   1000006     4.98138822E+03   # ~t_1
   2000006     5.02300730E+03   # ~t_2
   1000011     5.00008192E+03   # ~e_L
   2000011     5.00007598E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008192E+03   # ~mu_L
   2000013     5.00007598E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99953334E+03   # ~tau_1
   2000015     5.00062513E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.40000000E+03   # ~g
   1000022     2.04339193E+03   # ~chi_10
   1000023    -2.05164938E+03   # ~chi_20
   1000025     2.06166059E+03   # ~chi_30
   1000035     3.00143959E+03   # ~chi_40
   1000024     2.05093454E+03   # ~chi_1+
   1000037     3.00143927E+03   # ~chi_2+
   1000039     3.14166509E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.89816853E-01   # N_11
  1  2    -1.25431302E-02   # N_12
  1  3    -5.75535758E-01   # N_13
  1  4    -5.66319116E-01   # N_14
  2  1     1.06115437E-02   # N_21
  2  2    -1.53251021E-02   # N_22
  2  3     7.06927062E-01   # N_23
  2  4    -7.07040781E-01   # N_24
  3  1     8.07466868E-01   # N_31
  3  2     1.04231776E-02   # N_32
  3  3     4.11111020E-01   # N_33
  3  4     4.22937753E-01   # N_34
  4  1     8.55812373E-04   # N_41
  4  2    -9.99749553E-01   # N_42
  4  3     6.70541630E-04   # N_43
  4  4     2.23528401E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.67396087E-04   # U_11
  1  2     9.99999532E-01   # U_12
  2  1     9.99999532E-01   # U_21
  2  2    -9.67396087E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.16175693E-02   # V_11
  1  2    -9.99500040E-01   # V_12
  2  1     9.99500040E-01   # V_21
  2  2     3.16175693E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07587134E-01   # cos(theta_t)
  1  2    -7.06626102E-01   # sin(theta_t)
  2  1     7.06626102E-01   # -sin(theta_t)
  2  2     7.07587134E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.83945760E-01   # cos(theta_b)
  1  2     7.29532862E-01   # sin(theta_b)
  2  1    -7.29532862E-01   # -sin(theta_b)
  2  2    -6.83945760E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05181747E-01   # cos(theta_tau)
  1  2     7.09026589E-01   # sin(theta_tau)
  2  1    -7.09026589E-01   # -sin(theta_tau)
  2  2    -7.05181747E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90198200E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52041159E+02   # vev(Q)              
         4     4.87814092E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52690069E-01   # gprime(Q) DRbar
     2     6.26585368E-01   # g(Q) DRbar
     3     1.07753986E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02726933E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73175868E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79577193E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.05484272E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.65346619E+06   # M^2_Hd              
        22    -9.26738559E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36785276E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.83321616E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.71766352E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.90280544E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.67131898E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.90187738E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.08003117E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.13754593E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.56750969E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.19835581E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.67131898E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.90187738E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.08003117E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.13754593E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.56750969E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.19835581E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.05750200E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.60206930E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     9.06566124E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.22499332E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.71185455E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.48308151E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.48308151E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.48308151E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.48308151E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.49723961E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.49723961E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.40663509E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.19476220E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.83221834E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.84461974E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.73499642E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.81417556E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.46091327E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.31183145E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.57020214E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.60991371E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02373500E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.57371488E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.39812304E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.03006300E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.78728425E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.40929841E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.94160388E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.98753492E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.80943664E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.25188296E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.88229509E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.21775584E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.77554135E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.15358539E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.98295704E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.42287355E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.51671226E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.18619259E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.05942038E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32485487E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.13077985E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.99851773E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.82135018E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.90839341E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.27166879E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.86792938E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.24536030E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.43039124E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.48921172E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.69939755E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.65777444E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54476793E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.98406482E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.87451871E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.92582190E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.55802130E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.82141955E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.08929601E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.14470469E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.41800016E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.24858640E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.33914560E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.49869443E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.69998315E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60287529E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.99430192E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.28872834E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.43263659E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.97970022E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88571768E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.82135018E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.90839341E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.27166879E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.86792938E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.24536030E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.43039124E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.48921172E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.69939755E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.65777444E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54476793E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.98406482E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.87451871E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.92582190E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.55802130E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.82141955E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.08929601E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.14470469E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.41800016E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.24858640E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.33914560E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.49869443E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.69998315E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60287529E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.99430192E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.28872834E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.43263659E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.97970022E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88571768E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.82497724E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.89449313E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.18126747E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02928599E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.82418713E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.95054321E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.65665049E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.70906636E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.49522500E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12768141E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.50364298E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.33937395E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.82497724E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.89449313E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.18126747E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02928599E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.82418713E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.95054321E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.65665049E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.70906636E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.49522500E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12768141E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.50364298E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.33937395E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.26650552E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.67886918E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.94122121E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.05956820E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.74974853E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25727068E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.50461559E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.60752300E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.27419672E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.56796679E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.18663709E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.12012306E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.76433879E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.52966381E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.53385505E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.82478055E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.69330369E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.16851307E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.39036710E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.82939105E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     9.56071235E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.65051265E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.82478055E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.69330369E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.16851307E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.39036710E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.82939105E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     9.56071235E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.65051265E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.82700023E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.68883347E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.16681041E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.38299404E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.82716949E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.74086192E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.64607233E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.64609178E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.27849338E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     6.49860084E-08    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.56992879E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.01172934E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18998232E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18881645E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.11693112E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.49033741E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53996198E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49040119E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47079348E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.97584176E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.50125917E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.13759204E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.67097750E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.86755065E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51289419E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.61954004E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     4.17406629E-08    2     1000039        35   # BR(~chi_10 -> ~G        H)
     1.47003137E-06    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.19793859E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.51656162E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.03878354E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.03643095E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     4.59672623E-08    2     1000039        35   # BR(~chi_20 -> ~G        H)
     2.55451262E-09    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41002221E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82593635E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15507502E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81803982E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.17094381E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.16746412E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.08019209E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.33018597E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.33018597E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.33018597E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.53644423E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.53644423E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.17881484E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17881484E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.05999712E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.05999712E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.39813370E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.99041761E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.56600481E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.97877647E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.47134228E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     2.63869351E-11    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.02371736E-09    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.61583965E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.93082260E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.14056606E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.91991075E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.68368774E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.34640708E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.34523692E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.05349051E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.91951717E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.91951717E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.91951717E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.51692540E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.84931535E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.95914406E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.83207211E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.46475412E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.33615128E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.33538815E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.10092568E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.66848801E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.66848801E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.66848801E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.15112845E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.15112845E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.97608974E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.97608974E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.17035481E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.17035481E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.16687858E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.16687858E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.27672554E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.27672554E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.49041171E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.91117329E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.32736699E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.22388192E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47622790E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47622790E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.32437212E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17587708E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.98357383E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.73527077E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.63784701E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.03314524E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.59504589E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.15099030E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07410444E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16720330E-01    2           5        -5   # BR(h -> b       bb     )
     6.39290932E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26285445E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79519382E-04    2           3        -3   # BR(h -> s       sb     )
     2.06878382E-02    2           4        -4   # BR(h -> c       cb     )
     6.71721348E-02    2          21        21   # BR(h -> g       g      )
     2.30582631E-03    2          22        22   # BR(h -> gam     gam    )
     1.54194511E-03    2          22        23   # BR(h -> Z       gam    )
     2.01247050E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56899778E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78124825E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48722925E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429955E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71221637E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547175E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666522E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052362E-01    2           6        -6   # BR(H -> t       tb     )
     7.97574548E-04    2          21        21   # BR(H -> g       g      )
     2.73324849E-06    2          22        22   # BR(H -> gam     gam    )
     1.16036314E-06    2          23        22   # BR(H -> Z       gam    )
     3.33511522E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66301041E-04    2          23        23   # BR(H -> Z       Z      )
     9.00644511E-04    2          25        25   # BR(H -> h       h      )
     8.55886141E-24    2          36        36   # BR(H -> A       A      )
     3.48754572E-11    2          23        36   # BR(H -> Z       A      )
     5.85200061E-12    2          24       -37   # BR(H -> W+      H-     )
     5.85200061E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387984E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48996922E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894076E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62253664E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675697E-06    2           3        -3   # BR(A -> s       sb     )
     9.96161598E-06    2           4        -4   # BR(A -> c       cb     )
     9.96981094E-01    2           6        -6   # BR(A -> t       tb     )
     9.43661546E-04    2          21        21   # BR(A -> g       g      )
     3.04657330E-06    2          22        22   # BR(A -> gam     gam    )
     1.35301924E-06    2          23        22   # BR(A -> Z       gam    )
     3.25020563E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74515022E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39975998E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49234978E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81135680E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52585795E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45680913E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08728987E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403360E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33180926E-04    2          24        25   # BR(H+ -> W+      h      )
     5.24399507E-13    2          24        36   # BR(H+ -> W+      A      )
