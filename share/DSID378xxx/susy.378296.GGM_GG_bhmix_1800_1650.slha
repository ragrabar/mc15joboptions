#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.65426550E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05418609E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414414E+03   # H
        36     2.00000000E+03   # A
        37     2.00174369E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013258E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989273E+03   # ~u_L
   2000002     4.99994937E+03   # ~u_R
   1000003     5.00013258E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989273E+03   # ~c_L
   2000004     4.99994937E+03   # ~c_R
   1000005     4.99941006E+03   # ~b_1
   2000005     5.00074920E+03   # ~b_2
   1000006     4.98547272E+03   # ~t_1
   2000006     5.01894664E+03   # ~t_2
   1000011     5.00008195E+03   # ~e_L
   2000011     5.00007595E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008195E+03   # ~mu_L
   2000013     5.00007595E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99963980E+03   # ~tau_1
   2000015     5.00051869E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.64327479E+03   # ~chi_10
   1000023    -1.65186334E+03   # ~chi_20
   1000025     1.66138696E+03   # ~chi_30
   1000035     3.00146709E+03   # ~chi_40
   1000024     1.65111151E+03   # ~chi_1+
   1000037     3.00146680E+03   # ~chi_2+
   1000039     1.68207099E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.01207998E-01   # N_11
  1  2    -8.65503234E-03   # N_12
  1  3    -5.70749940E-01   # N_13
  1  4    -5.59212428E-01   # N_14
  2  1     1.31771556E-02   # N_21
  2  2    -1.66427321E-02   # N_22
  2  3     7.06867651E-01   # N_23
  2  4    -7.07027232E-01   # N_24
  3  1     7.98983686E-01   # N_31
  3  2     7.55374644E-03   # N_32
  3  3     4.17814754E-01   # N_33
  3  4     4.32433628E-01   # N_34
  4  1     6.12666917E-04   # N_41
  4  2    -9.99795504E-01   # N_42
  4  3    -3.66903336E-03   # N_43
  4  4     1.98774253E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.17670396E-03   # U_11
  1  2     9.99986601E-01   # U_12
  2  1     9.99986601E-01   # U_21
  2  2     5.17670396E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.81128177E-02   # V_11
  1  2    -9.99604757E-01   # V_12
  2  1     9.99604757E-01   # V_21
  2  2     2.81128177E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07704441E-01   # cos(theta_t)
  1  2    -7.06508616E-01   # sin(theta_t)
  2  1     7.06508616E-01   # -sin(theta_t)
  2  2     7.07704441E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.78196182E-01   # cos(theta_b)
  1  2     7.34880901E-01   # sin(theta_b)
  2  1    -7.34880901E-01   # -sin(theta_b)
  2  2    -6.78196182E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04688152E-01   # cos(theta_tau)
  1  2     7.09517166E-01   # sin(theta_tau)
  2  1    -7.09517166E-01   # -sin(theta_tau)
  2  2    -7.04688152E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90197110E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51962082E+02   # vev(Q)              
         4     4.63081234E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52728857E-01   # gprime(Q) DRbar
     2     6.26831934E-01   # g(Q) DRbar
     3     1.08213137E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02684592E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72867577E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79656707E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.65426550E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.02290890E+03   # M^2_Hd              
        22    -7.98030215E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36895234E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.40735223E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.66712568E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.44004875E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.38317764E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.60138873E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.20525580E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.78502877E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.75902560E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.69536965E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.38317764E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.60138873E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.20525580E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.78502877E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.75902560E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.69536965E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.55488144E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.04319310E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.14766462E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.27022267E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.27022267E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.27022267E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.27022267E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.94323551E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.75434593E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.34164400E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.60866429E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.41073759E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.32162136E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.81665495E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.57463319E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.07440637E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.17272053E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.77919985E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.57437959E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.17744735E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.83810590E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.34980743E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.61083394E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.37519536E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.86977584E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.07485457E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.03093849E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.51813703E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.13095892E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.04212633E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.35360011E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.43041100E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.35946491E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.51310973E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.86276196E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.70404666E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.26177772E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.41465403E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.17261683E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22295873E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.32325119E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.35326418E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.66522978E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.48305619E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.06540093E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.96505715E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.93001239E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.06939440E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.47665903E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.07577113E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.59404680E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.91130462E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59285858E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22300681E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.96129342E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.46218591E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.36101497E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.48512360E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.61270671E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.97131080E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.93050277E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00625278E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.80791875E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.82465710E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.68937176E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.04017341E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89500883E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22295873E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.32325119E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.35326418E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.66522978E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.48305619E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.06540093E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.96505715E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.93001239E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.06939440E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.47665903E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.07577113E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.59404680E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.91130462E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59285858E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22300681E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.96129342E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.46218591E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.36101497E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.48512360E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.61270671E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.97131080E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.93050277E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00625278E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.80791875E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.82465710E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.68937176E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.04017341E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89500883E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.89030846E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.84619868E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.56920961E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.11827292E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76347776E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.87663830E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.53288487E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.96267706E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.62692494E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73791860E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.37133520E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.93693675E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.89030846E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.84619868E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.56920961E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.11827292E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76347776E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.87663830E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.53288487E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.96267706E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.62692494E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73791860E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.37133520E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.93693675E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.42612358E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.87719355E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.81885986E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.20927463E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.63488376E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.56060609E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.27326854E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     5.23936227E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43450881E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.75418727E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.05937865E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.26955746E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.65161645E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.26215637E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30678288E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.89011682E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.47625039E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.10569735E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.04557789E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.76704371E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.48369635E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52816397E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.89011682E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.47625039E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.10569735E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.04557789E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.76704371E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.48369635E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52816397E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.89266588E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.47054341E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.10296055E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.04465651E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.76460534E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.72905773E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.52329027E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.96252546E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.26086856E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.55064436E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03273040E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18355444E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18248016E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.24503788E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.57930248E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53565978E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48748819E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46541529E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.93509197E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51792753E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.15827304E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.69176728E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.02967973E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51033065E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.45998962E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.23643648E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.37040909E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02056640E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.95112285E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40491177E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81923224E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16984892E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81195593E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.15522168E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.15201358E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15266579E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.29853950E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.29853950E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.29853950E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.81036818E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.81036818E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.27012281E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.27012281E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.15364641E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.15364641E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.66605372E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.71621803E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.92219569E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.22724056E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.02084205E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     8.33306732E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.11642406E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.70703533E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.11500974E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.51170906E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.86039415E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.85894265E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.49054261E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.93524072E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.93524072E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.93524072E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.59926362E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.66074741E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.10847882E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.64557216E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.06454680E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.06387617E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.56869459E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.12598341E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.12598341E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.12598341E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.77470912E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.77470912E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.61811247E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.61811247E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.91563812E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.91563812E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.91251335E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.91251335E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.11795644E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.11795644E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.57944989E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10151313E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.07865711E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.29447487E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46897771E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46897771E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.22073242E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.71264603E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.88498962E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.19203594E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.63869546E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.25121145E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.03197343E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.57437698E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07663768E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16942281E-01    2           5        -5   # BR(h -> b       bb     )
     6.38891596E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26144095E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79219878E-04    2           3        -3   # BR(h -> s       sb     )
     2.06750129E-02    2           4        -4   # BR(h -> c       cb     )
     6.71315075E-02    2          21        21   # BR(h -> g       g      )
     2.30397100E-03    2          22        22   # BR(h -> gam     gam    )
     1.54095716E-03    2          22        23   # BR(h -> Z       gam    )
     2.01137733E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56740141E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78122579E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48326916E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431792E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71228131E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548009E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666797E-05    2           4        -4   # BR(H -> c       cb     )
     9.96055086E-01    2           6        -6   # BR(H -> t       tb     )
     7.97599572E-04    2          21        21   # BR(H -> g       g      )
     2.73583881E-06    2          22        22   # BR(H -> gam     gam    )
     1.16021470E-06    2          23        22   # BR(H -> Z       gam    )
     3.33182529E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66136972E-04    2          23        23   # BR(H -> Z       Z      )
     9.02344781E-04    2          25        25   # BR(H -> h       h      )
     8.67438884E-24    2          36        36   # BR(H -> A       A      )
     3.48798858E-11    2          23        36   # BR(H -> Z       A      )
     5.54188803E-12    2          24       -37   # BR(H -> W+      H-     )
     5.54188803E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386350E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48603733E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895118E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62257348E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676183E-06    2           3        -3   # BR(A -> s       sb     )
     9.96165854E-06    2           4        -4   # BR(A -> c       cb     )
     9.96985354E-01    2           6        -6   # BR(A -> t       tb     )
     9.43665579E-04    2          21        21   # BR(A -> g       g      )
     3.03486660E-06    2          22        22   # BR(A -> gam     gam    )
     1.35284412E-06    2          23        22   # BR(A -> Z       gam    )
     3.24699309E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74517239E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39112731E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236789E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81142083E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52033300E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45683905E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729582E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403687E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32861552E-04    2          24        25   # BR(H+ -> W+      h      )
     5.65955698E-13    2          24        36   # BR(H+ -> W+      A      )
