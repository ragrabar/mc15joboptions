evgenConfig.description = "Single Higgs produced in range of R(transverse radius),z and pt between 0-500 GeV"
evgenConfig.keywords = ["singleParticle", "SUSY"]
evgenConfig.contact  = [ "sai.neha.santpur@cern.ch" ]
include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
import ROOT
import math

##For convenince define c in mm/s
CLIGHT=299.792458

genSeq.ParticleGun.sampler.pid = 25
#pt and mass in MeV
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[0,500000], eta=[-2.5,2.5], mass=125000.0)
#Distances in mm and t in seconds

#Create a class to set vertex time = vertex radius/c
class PosRZphiTSampler(PG.PosSampler):
    "A position sampler in R,z,phi space and setting t=decay_distance/c"
    
    def __init__(self,r,z,phi,t):
        self.r=r
        self.z=z
        self.phi=phi
        self.t=t

    @property
    def r(self):
        "R sampler"
        return self._r
    @r.setter
    def r(self, x):
        self._r = PG.mksampler(x)

    @property
    def z(self):
        "z0 sampler"
        return self._z
    @z.setter
    def z(self, x):
        self._z = PG.mksampler(x)
    
    @property
    def phi(self):
        "Phi sampler"
        return self._phi
    @r.setter
    def phi(self, x):
        self._phi = PG.mksampler(x)

    @property
    def t(self):
        "t sampler"
        return self._t
    @t.setter
    def t(self, x):
        self._t = PG.mksampler(x)
        
    def shoot(self):
        r = self.r()
        z = self.z()
        phi = self.phi()
        t = (math.sqrt( math.fabs(self.r()*self.r()) + math.fabs(self.z()*self.z()))/CLIGHT)
        
        return ROOT.TLorentzVector(r*math.cos(phi),r*math.sin(phi),z,t)



genSeq.ParticleGun.sampler.pos = PosRZphiTSampler(r=[0,1550],z=[-1000.0,1000.0],phi=[0, 2*math.pi],t=0)

include("MC15JobOptions/EvtGen_Fragment.py")

genSeq.EvtInclusiveDecay.whiteList+=[25]
genSeq.EvtInclusiveDecay.OutputLevel = 3

genSeq.EvtInclusiveDecay.pdtFile = "dispH0_myinclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "dispH0_myinclusive.dec"
genSeq.EvtInclusiveDecay.userDecayFile  = "dispH0_mydecay.dec"

evgenConfig.auxfiles += ['dispH0_myinclusive.pdt','dispH0_myinclusive.dec','dispH0_mydecay.dec']
 
