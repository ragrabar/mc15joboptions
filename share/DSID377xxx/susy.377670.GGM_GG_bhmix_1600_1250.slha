#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410181E+01   # W+
        25     1.25000000E+02   # h
        35     2.00402449E+03   # H
        36     2.00000000E+03   # A
        37     2.00150868E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989244E+03   # ~u_L
   2000002     4.99994927E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989244E+03   # ~c_L
   2000004     4.99994927E+03   # ~c_R
   1000005     4.99957300E+03   # ~b_1
   2000005     5.00058668E+03   # ~b_2
   1000006     4.98954687E+03   # ~t_1
   2000006     5.01489031E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     1.20171209E+03   # ~chi_10
   1000023    -1.25008717E+03   # ~chi_20
   1000025     1.28888765E+03   # ~chi_30
   1000035     3.00348743E+03   # ~chi_40
   1000024     1.24662776E+03   # ~chi_1+
   1000037     3.00348540E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.17341245E-01   # N_11
  1  2    -2.99894418E-02   # N_12
  1  3     4.94135250E-01   # N_13
  1  4    -4.90257612E-01   # N_14
  2  1     3.50043432E-03   # N_21
  2  2    -3.65192012E-03   # N_22
  2  3    -7.06974455E-01   # N_23
  2  4    -7.07220991E-01   # N_24
  3  1     6.96712311E-01   # N_31
  3  2     3.24928186E-02   # N_32
  3  3    -5.05168244E-01   # N_33
  3  4     5.08272779E-01   # N_34
  4  1     1.11379673E-03   # N_41
  4  2    -9.99015272E-01   # N_42
  4  3    -2.86796078E-02   # N_43
  4  4     3.38337990E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.05322789E-02   # U_11
  1  2     9.99178230E-01   # U_12
  2  1     9.99178230E-01   # U_21
  2  2     4.05322789E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.78227309E-02   # V_11
  1  2     9.98855839E-01   # V_12
  2  1     9.98855839E-01   # V_21
  2  2     4.78227309E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07898881E-01   # cos(theta_t)
  1  2     7.06313793E-01   # sin(theta_t)
  2  1    -7.06313793E-01   # -sin(theta_t)
  2  2     7.07898881E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68539552E-01   # cos(theta_b)
  1  2     7.43676588E-01   # sin(theta_b)
  2  1    -7.43676588E-01   # -sin(theta_b)
  2  2     6.68539552E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03859950E-01   # cos(theta_tau)
  1  2     7.10338772E-01   # sin(theta_tau)
  2  1    -7.10338772E-01   # -sin(theta_tau)
  2  2     7.03859950E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90204712E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52155528E+02   # vev(Q)              
         4     3.42292153E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784139E-01   # gprime(Q) DRbar
     2     6.27183460E-01   # g(Q) DRbar
     3     1.08402972E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02542795E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71770964E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79804270E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29735257E+06   # M^2_Hd              
        22    -6.90564756E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052408E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.59556654E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.21259015E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.26964239E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.37305734E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.62977811E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.39972474E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.21790266E-04    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.09674989E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.09503451E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.54644256E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.62977811E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.39972474E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.21790266E-04    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.09674989E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.09503451E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.54644256E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.64730200E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.67447839E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     9.58016012E-04    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     4.65147910E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.48108545E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.30956122E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     8.30956122E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     8.30956122E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     8.30956122E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     3.35130138E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     3.35130138E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.26322647E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.55982996E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.91544933E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.87066948E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03659437E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.03857562E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.05587330E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.51758274E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.17289297E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.47034180E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.85505210E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.90877469E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.40373118E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.39573576E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.78857458E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.51777899E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.47300896E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.41917574E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.71549071E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.32619029E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.30642359E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.05044239E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.63212129E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.48747792E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.62286147E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.27783776E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.89485152E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.78572022E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.71646954E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.43109489E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.45912687E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.99972041E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33721814E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.84216814E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.37843330E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87554592E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.30519765E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25360224E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.61105922E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.97851671E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.19206248E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.21582133E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.22334801E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.05190989E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.46499105E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57322141E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33726404E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.94273036E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.31936145E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.78871867E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.30841320E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.33730694E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.61622888E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.97896927E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12194504E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.72271089E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.34901311E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.29938669E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.36642423E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88977761E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33721814E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.84216814E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.37843330E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87554592E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.30519765E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25360224E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.61105922E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.97851671E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.19206248E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.21582133E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.22334801E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.05190989E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.46499105E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57322141E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33726404E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.94273036E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.31936145E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.78871867E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.30841320E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.33730694E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.61622888E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.97896927E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12194504E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.72271089E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.34901311E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.29938669E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.36642423E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88977761E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94774507E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.22045651E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.65219681E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.04178232E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70280479E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91807246E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.41416999E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17866081E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.19203253E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.22389832E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.80783932E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.75989610E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94774507E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.22045651E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.65219681E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.04178232E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70280479E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91807246E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.41416999E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17866081E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.19203253E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.22389832E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.80783932E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.75989610E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.56758851E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.75211291E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48541028E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.60513350E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53332671E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.20179795E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.07152260E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.40087921E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56765570E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60253958E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.64112671E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67599038E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56963205E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.98237758E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14419862E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94779886E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10716988E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.84283670E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.47097400E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.70911916E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.66991453E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40973013E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94779886E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10716988E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.84283670E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.47097400E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.70911916E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.66991453E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40973013E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.95062445E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10610963E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.84107195E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.46381960E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.70652483E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.62374693E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40456200E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16374701E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.46817282E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.29259228E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.27649261E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09753207E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09750191E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08906386E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.67462997E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53122789E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12688934E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46308265E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34073623E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53798759E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.62943946E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.73250738E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98396095E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90397023E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12068817E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.36622824E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.41416242E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.08766971E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.85712606E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.18916931E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17196137E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.51748321E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16414374E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.51727216E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38346379E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.46553886E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.46541636E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.43096334E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.92078810E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.92078810E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.92078810E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.33724104E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.33724104E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.93289880E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.93289880E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.45747043E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.45747043E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.43679587E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.43679587E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.21054394E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.21054394E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.23012129E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.70751743E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.07940737E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.13092914E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.21487931E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.76123072E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.08234533E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29082014E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.37790547E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29635262E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.33898652E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26547604E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26545004E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.87617333E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.30508493E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.30508493E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.30508493E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.38623425E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.08975690E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.36338881E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.08910901E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.68394719E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.05622080E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.05587558E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.95844786E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.40914003E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.40914003E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.40914003E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31458691E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31458691E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30731180E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30731180E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.38194976E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.38194976E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.38181337E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.38181337E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.34369696E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.34369696E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.67465700E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.92091820E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51327453E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.13164066E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46390980E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46390980E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15289367E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67877977E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37209555E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82961273E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79897305E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.61215978E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.60087153E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.53960180E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08524515E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17718373E-01    2           5        -5   # BR(h -> b       bb     )
     6.37559944E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25672739E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78220828E-04    2           3        -3   # BR(h -> s       sb     )
     2.06312412E-02    2           4        -4   # BR(h -> c       cb     )
     6.69900398E-02    2          21        21   # BR(h -> g       g      )
     2.30428068E-03    2          22        22   # BR(h -> gam     gam    )
     1.53766485E-03    2          22        23   # BR(h -> Z       gam    )
     2.00738594E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56199189E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78107556E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47074130E-03    2           5        -5   # BR(H -> b       bb     )
     2.46424361E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71201860E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11545550E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667903E-05    2           4        -4   # BR(H -> c       cb     )
     9.96062737E-01    2           6        -6   # BR(H -> t       tb     )
     7.97666511E-04    2          21        21   # BR(H -> g       g      )
     2.70980347E-06    2          22        22   # BR(H -> gam     gam    )
     1.15999190E-06    2          23        22   # BR(H -> Z       gam    )
     3.35447873E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67266467E-04    2          23        23   # BR(H -> Z       Z      )
     9.03792895E-04    2          25        25   # BR(H -> h       h      )
     7.60366112E-24    2          36        36   # BR(H -> A       A      )
     3.01284084E-11    2          23        36   # BR(H -> Z       A      )
     7.00785318E-12    2          24       -37   # BR(H -> W+      H-     )
     7.00785318E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382532E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47366205E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897553E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265958E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677318E-06    2           3        -3   # BR(A -> s       sb     )
     9.96175801E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995309E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675001E-04    2          21        21   # BR(A -> g       g      )
     3.18686508E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257362E-06    2          23        22   # BR(A -> Z       gam    )
     3.26956118E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472810E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36343170E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237096E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143167E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50260768E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693249E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731449E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401483E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35092180E-04    2          24        25   # BR(H+ -> W+      h      )
     2.74486519E-13    2          24        36   # BR(H+ -> W+      A      )
