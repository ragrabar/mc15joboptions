#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410157E+01   # W+
        25     1.25000000E+02   # h
        35     2.00402538E+03   # H
        36     2.00000000E+03   # A
        37     2.00151081E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013293E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989244E+03   # ~u_L
   2000002     4.99994927E+03   # ~u_R
   1000003     5.00013293E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989244E+03   # ~c_L
   2000004     4.99994927E+03   # ~c_R
   1000005     4.99957311E+03   # ~b_1
   2000005     5.00058657E+03   # ~b_2
   1000006     4.98954542E+03   # ~t_1
   2000006     5.01489342E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.20171183E+03   # ~chi_10
   1000023    -1.25008717E+03   # ~chi_20
   1000025     1.28888787E+03   # ~chi_30
   1000035     3.00348747E+03   # ~chi_40
   1000024     1.24662772E+03   # ~chi_1+
   1000037     3.00348545E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.17341014E-01   # N_11
  1  2    -2.99896394E-02   # N_12
  1  3     4.94135424E-01   # N_13
  1  4    -4.90257763E-01   # N_14
  2  1     3.50045341E-03   # N_21
  2  2    -3.65194355E-03   # N_22
  2  3    -7.06974453E-01   # N_23
  2  4    -7.07220993E-01   # N_24
  3  1     6.96712549E-01   # N_31
  3  2     3.24930200E-02   # N_32
  3  3    -5.05168066E-01   # N_33
  3  4     5.08272616E-01   # N_34
  4  1     1.11380987E-03   # N_41
  4  2    -9.99015259E-01   # N_42
  4  3    -2.86797906E-02   # N_43
  4  4     3.38340147E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.05325369E-02   # U_11
  1  2     9.99178219E-01   # U_12
  2  1     9.99178219E-01   # U_21
  2  2     4.05325369E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.78230356E-02   # V_11
  1  2     9.98855824E-01   # V_12
  2  1     9.98855824E-01   # V_21
  2  2     4.78230356E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07898750E-01   # cos(theta_t)
  1  2     7.06313924E-01   # sin(theta_t)
  2  1    -7.06313924E-01   # -sin(theta_t)
  2  2     7.07898750E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68530989E-01   # cos(theta_b)
  1  2     7.43684286E-01   # sin(theta_b)
  2  1    -7.43684286E-01   # -sin(theta_b)
  2  2     6.68530989E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03859791E-01   # cos(theta_tau)
  1  2     7.10338929E-01   # sin(theta_tau)
  2  1    -7.10338929E-01   # -sin(theta_tau)
  2  2     7.03859791E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90206242E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52156801E+02   # vev(Q)              
         4     3.40133889E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784279E-01   # gprime(Q) DRbar
     2     6.27184313E-01   # g(Q) DRbar
     3     1.08044647E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02560764E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71713831E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79803348E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29762258E+06   # M^2_Hd              
        22    -6.85615052E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37052791E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.39529040E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.78666086E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.22804135E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.47406078E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.09284045E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.44689659E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.96967617E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.84097587E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.69659629E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.14253975E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.09284045E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.44689659E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.96967617E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.84097587E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.69659629E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.14253975E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.16675268E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.81984486E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.06516763E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     9.20714121E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.23118615E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.30052659E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.89457675E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.89457675E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.89457675E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.89457675E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.97570510E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.97570510E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.03338977E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.12422630E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.06569005E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.15999997E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11594411E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.11586066E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.21324088E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.25710816E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.92546417E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.75654352E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.06684059E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.56287773E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.52196455E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.01749215E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.02346147E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.22918253E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.23090975E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.90504214E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.53637855E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.59420667E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.43434338E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.14641231E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.88976375E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.34533085E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.38934365E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.48503662E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.08361258E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.03640250E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.89774328E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.58404515E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.82450792E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.78743172E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09988538E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.71678747E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.09023021E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.08488629E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.67941688E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.61667228E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.35957278E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86291162E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.95457774E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.48172325E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.85045415E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.29830739E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.76504786E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.52199081E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09994225E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.15939895E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.46658192E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.54633672E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.68296909E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.59810849E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.36527294E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86342270E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88455514E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.43495838E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.51698789E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.95937568E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.16981882E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87605507E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09988538E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.71678747E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.09023021E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.08488629E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.67941688E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.61667228E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.35957278E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86291162E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.95457774E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.48172325E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.85045415E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.29830739E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.76504786E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.52199081E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09994225E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.15939895E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.46658192E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.54633672E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.68296909E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.59810849E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.36527294E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86342270E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88455514E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.43495838E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.51698789E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.95937568E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.16981882E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87605507E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94775201E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.22042841E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.65222605E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.04178244E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70280558E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91809759E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.41417163E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17866254E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.19202947E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.22391167E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.80784238E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.76003185E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94775201E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.22042841E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.65222605E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.04178244E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70280558E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91809759E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.41417163E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17866254E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.19202947E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.22391167E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.80784238E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.76003185E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.56759261E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.75210897E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48534534E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.60513462E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53332763E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.20180778E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.07152447E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.40087697E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56766018E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60253442E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.64106157E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67598915E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56963417E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.98313986E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14420289E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94780581E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10716870E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.84285852E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.47095783E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.70912002E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.66994954E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40973172E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94780581E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10716870E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.84285852E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.47095783E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.70912002E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.66994954E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40973172E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.95063138E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10610846E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.84109378E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.46380353E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.70652573E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.62376987E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40456365E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16378467E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.46812480E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.29259380E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.27649428E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09753258E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09750241E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08906445E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.67467517E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53123492E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12689350E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46309075E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34074023E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53796431E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.62934587E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.73246579E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98396062E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90397224E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12067137E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.36627469E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.41395273E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.08757724E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.85696116E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.18912961E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17196201E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.51748381E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16414444E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.51727275E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38346560E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.46553907E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.46541657E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.43096381E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.92078778E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.92078778E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.92078778E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.33728026E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.33728026E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.93318926E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.93318926E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.45760117E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.45760117E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.43692648E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.43692648E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.21061929E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.21061929E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.23020293E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.70726834E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.07944694E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.13068415E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.21465064E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.76114004E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.08244273E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29094352E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.37799941E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29647597E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.33866350E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26573510E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26570910E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.87642404E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.30565590E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.30565590E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.30565590E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.38622780E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.08974805E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.36338263E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.08910016E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.68394358E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.05619827E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.05585304E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.95842649E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.40913537E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.40913537E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.40913537E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31458862E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31458862E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30731359E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30731359E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.38195545E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.38195545E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.38181907E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.38181907E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.34370308E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.34370308E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.67470214E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.92094305E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51328151E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.13166132E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46391791E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46391791E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15288371E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67873776E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37208269E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82958756E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79890490E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.61210248E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.60086252E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.53960755E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08584096E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17773958E-01    2           5        -5   # BR(h -> b       bb     )
     6.37469885E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25640861E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78153236E-04    2           3        -3   # BR(h -> s       sb     )
     2.06281904E-02    2           4        -4   # BR(h -> c       cb     )
     6.69801091E-02    2          21        21   # BR(h -> g       g      )
     2.30394609E-03    2          22        22   # BR(h -> gam     gam    )
     1.53744068E-03    2          22        23   # BR(h -> Z       gam    )
     2.00709390E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56161828E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78109333E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46977336E-03    2           5        -5   # BR(H -> b       bb     )
     2.46422807E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71196366E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11544835E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667929E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063012E-01    2           6        -6   # BR(H -> t       tb     )
     7.97666442E-04    2          21        21   # BR(H -> g       g      )
     2.70981327E-06    2          22        22   # BR(H -> gam     gam    )
     1.15998523E-06    2          23        22   # BR(H -> Z       gam    )
     3.35913051E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67498422E-04    2          23        23   # BR(H -> Z       Z      )
     9.03790771E-04    2          25        25   # BR(H -> h       h      )
     7.64846493E-24    2          36        36   # BR(H -> A       A      )
     3.01615877E-11    2          23        36   # BR(H -> Z       A      )
     6.99057153E-12    2          24       -37   # BR(H -> W+      H-     )
     6.99057153E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382342E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47271195E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897674E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266385E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677374E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176295E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995804E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675469E-04    2          21        21   # BR(A -> g       g      )
     3.18686732E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257348E-06    2          23        22   # BR(A -> Z       gam    )
     3.27410782E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472853E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36136706E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237332E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81144002E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50128631E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693687E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731536E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401018E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35559036E-04    2          24        25   # BR(H+ -> W+      h      )
     2.76429009E-13    2          24        36   # BR(H+ -> W+      A      )
