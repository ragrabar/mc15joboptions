evgenConfig.description = "Single mu with flat phi, eta in [-5.0, 5.0], and pT = 5 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=5000, eta=[-5.0, 5.0])
