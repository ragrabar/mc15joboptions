#--------------------------------------------------------------
# Read MadGraph output LHEF, Showering with Pythia8, AU2 tune
#--------------------------------------------------------------
include("MC15JobOptions/nonStandard/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
if (runArgs.runNumber == 415110):
    evgenConfig.description = "SM p p > l nu j j gam gam at 14TeV"
    evgenConfig.keywords = ["SM","diphoton"]

evgenConfig.contact = ['Maosen Zhou <maosen.zhou@cern.ch>']
evgenConfig.inputfilecheck = 'group.phys-gener.MG5_aMC233.415110.sm_lnugamgamjj_14TeV.TXT.mc15_v1'
