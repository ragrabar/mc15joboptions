#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375308E+01   # W+
        25     1.26000000E+02   # h
        35     2.00417498E+03   # H
        36     2.00000000E+03   # A
        37     2.00208058E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.92095315E+03   # ~d_L
   2000001     4.92084233E+03   # ~d_R
   1000002     4.92070551E+03   # ~u_L
   2000002     4.92076430E+03   # ~u_R
   1000003     4.92095315E+03   # ~s_L
   2000003     4.92084233E+03   # ~s_R
   1000004     4.92070551E+03   # ~c_L
   2000004     4.92076430E+03   # ~c_R
   1000005     4.92078103E+03   # ~b_1
   2000005     4.92101593E+03   # ~b_2
   1000006     5.05348612E+03   # ~t_1
   2000006     5.35609622E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001288E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     2.23872670E+03   # ~g
   1000022     2.38941383E+02   # ~chi_10
   1000023    -2.70431190E+02   # ~chi_20
   1000025     3.33148465E+02   # ~chi_30
   1000035     3.12901962E+03   # ~chi_40
   1000024     2.68187859E+02   # ~chi_1+
   1000037     3.12901919E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56432562E-01   # N_11
  1  2    -2.22760211E-02   # N_12
  1  3     5.94491121E-01   # N_13
  1  4    -5.80057661E-01   # N_14
  2  1     1.53219605E-02   # N_21
  2  2    -4.61400128E-03   # N_22
  2  3    -7.05672967E-01   # N_23
  2  4    -7.08356981E-01   # N_24
  3  1     8.30751379E-01   # N_31
  3  2     1.55265645E-02   # N_32
  3  3    -3.85163377E-01   # N_33
  3  4     4.01572217E-01   # N_34
  4  1     4.33080137E-04   # N_41
  4  2    -9.99620636E-01   # N_42
  4  3    -1.59732444E-02   # N_43
  4  4     2.24332840E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25819345E-02   # U_11
  1  2     9.99744996E-01   # U_12
  2  1     9.99744996E-01   # U_21
  2  2     2.25819345E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17212627E-02   # V_11
  1  2     9.99496754E-01   # V_12
  2  1     9.99496754E-01   # V_21
  2  2     3.17212627E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99689646E-01   # cos(theta_t)
  1  2    -2.49120790E-02   # sin(theta_t)
  2  1     2.49120790E-02   # -sin(theta_t)
  2  2     9.99689646E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13935080E-01   # cos(theta_b)
  1  2     8.57829082E-01   # sin(theta_b)
  2  1    -8.57829082E-01   # -sin(theta_b)
  2  2     5.13935080E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89409664E-01   # cos(theta_tau)
  1  2     7.24371669E-01   # sin(theta_tau)
  2  1    -7.24371669E-01   # -sin(theta_tau)
  2  2     6.89409664E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90211183E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51797038E+02   # vev(Q)              
         4     3.83558176E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53062027E-01   # gprime(Q) DRbar
     2     6.28967336E-01   # g(Q) DRbar
     3     1.07892628E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02720707E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72320853E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79953671E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99420331E+06   # M^2_Hd              
        22    -5.44345577E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37848083E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.75998808E-03   # gluino decays
#          BR         NDA      ID1       ID2
     4.08163378E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.91378229E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.73692454E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.63147209E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.64909022E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.99189607E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.98981146E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.01289102E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.78613966E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.63147209E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.64909022E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.99189607E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.98981146E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.01289102E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.78613966E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.76758704E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.09735300E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.06148742E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.39955472E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.07885144E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.57433231E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.62222810E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.62222810E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.62222810E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.62222810E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34920092E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34920092E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.73330112E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.68627628E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.32541988E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.71490171E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.69419694E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.78013683E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.37247320E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.30070962E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.96053256E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.64700224E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.55970840E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.48772975E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08725813E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.46086230E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.42438564E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.16827499E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.86690241E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.34367469E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -5.49555492E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.03392384E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.63272328E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     4.54501809E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.36902372E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.17938577E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.65481876E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.48441512E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -9.78537360E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.00930413E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.96574372E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.20818421E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.87715204E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.27976738E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.95456990E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.42521752E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.78140173E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.40240194E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.58009319E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.75074813E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.86094663E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.66908573E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.03552718E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.89846876E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.57824212E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.00161574E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.15587334E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.88893003E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.75772607E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.82843720E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.38467981E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.05865105E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.98411099E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.41115267E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.86100571E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.59069233E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.56581171E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.94123423E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.57995380E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.01440355E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.16053085E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.88956221E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.68015269E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.78222022E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.62158680E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.06152760E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.04207577E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84598881E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.86094663E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.66908573E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.03552718E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.89846876E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.57824212E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.00161574E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.15587334E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.88893003E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.75772607E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.82843720E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.38467981E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.05865105E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.98411099E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.41115267E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.86100571E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.59069233E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.56581171E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.94123423E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.57995380E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.01440355E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.16053085E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.88956221E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.68015269E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.78222022E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.62158680E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.06152760E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.04207577E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84598881E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275667E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87985753E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10929058E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61554973E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513851E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11865521E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409642E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125725E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10542474E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35161318E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89222294E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99471584E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275667E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87985753E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10929058E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61554973E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513851E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11865521E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409642E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125725E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10542474E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35161318E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89222294E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99471584E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122841E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88604788E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46962339E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15063618E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31198799E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95803291E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591064E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736189E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64275126E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64648717E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92629446E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00330532E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599818E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67336094E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89411571E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261734E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82475710E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21877398E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41403304E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734547E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40467873E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088021E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261734E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82475710E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21877398E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41403304E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734547E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40467873E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088021E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581904E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81582833E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21738324E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41241950E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438166E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54381634E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496046E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.52227676E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64490106E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33996561E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33996561E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332286E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332286E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09305858E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41069608E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06996675E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33235212E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02065384E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85564516E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07664324E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52654034E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.54999880E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25202739E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51924251E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29937666E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00953945E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08142022E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00761871E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92338865E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.89926401E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77626610E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.18851041E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15556486E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14106712E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69572143E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20448323E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55905398E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20448323E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55905398E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25926546E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55800712E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55800712E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48792633E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10378850E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10378850E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10378850E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18374606E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18374606E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18374606E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18374606E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94582085E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94582085E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94582085E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94582085E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.18523699E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.18523699E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43401077E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99748759E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.76507451E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.34699521E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.26369346E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41073330E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39780783E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01545181E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20484126E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01746207E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01746207E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33801880E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26850105E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02264953E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68826383E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20870995E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02335954E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83896894E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74759955E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22123079E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54386514E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54386514E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42026627E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67431410E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77961692E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06212580E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44199673E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21723312E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01647975E-01    2           5        -5   # BR(h -> b       bb     )
     6.22570866E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20362945E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66245151E-04    2           3        -3   # BR(h -> s       sb     )
     2.01139614E-02    2           4        -4   # BR(h -> c       cb     )
     6.64269174E-02    2          21        21   # BR(h -> g       g      )
     2.31863567E-03    2          22        22   # BR(h -> gam     gam    )
     1.62686775E-03    2          22        23   # BR(h -> Z       gam    )
     2.16833503E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80884451E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01158209E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38981029E-03    2           5        -5   # BR(H -> b       bb     )
     2.32280182E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21196922E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05141983E-06    2           3        -3   # BR(H -> s       sb     )
     9.48914718E-06    2           4        -4   # BR(H -> c       cb     )
     9.38911509E-01    2           6        -6   # BR(H -> t       tb     )
     7.51927919E-04    2          21        21   # BR(H -> g       g      )
     2.63834331E-06    2          22        22   # BR(H -> gam     gam    )
     1.09240300E-06    2          23        22   # BR(H -> Z       gam    )
     3.18106296E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58619260E-04    2          23        23   # BR(H -> Z       Z      )
     8.52999759E-04    2          25        25   # BR(H -> h       h      )
     8.61587913E-24    2          36        36   # BR(H -> A       A      )
     3.41152653E-11    2          23        36   # BR(H -> Z       A      )
     2.64153521E-12    2          24       -37   # BR(H -> W+      H-     )
     2.64153521E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23220317E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15139956E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93494157E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.02031884E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96791115E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32268274E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54731725E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05622250E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39270133E-03    2           5        -5   # BR(A -> b       bb     )
     2.29923689E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12863298E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164291E-06    2           3        -3   # BR(A -> s       sb     )
     9.39100912E-06    2           4        -4   # BR(A -> c       cb     )
     9.39873467E-01    2           6        -6   # BR(A -> t       tb     )
     8.89608092E-04    2          21        21   # BR(A -> g       g      )
     2.66245214E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397179E-06    2          23        22   # BR(A -> Z       gam    )
     3.09978707E-04    2          23        25   # BR(A -> Z       h      )
     6.12206065E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74298850E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47853372E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11569460E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99560011E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.12887962E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45915404E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97574122E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23448322E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822084E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30180893E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42065872E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14112356E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438825E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41572715E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17694314E-04    2          24        25   # BR(H+ -> W+      h      )
     1.28929269E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79119534E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30732495E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98187081E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
