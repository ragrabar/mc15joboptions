#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.30000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373700E+01   # W+
        25     1.26000000E+02   # h
        35     2.00417076E+03   # H
        36     2.00000000E+03   # A
        37     2.00208931E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.90936645E+03   # ~d_L
   2000001     4.90925540E+03   # ~d_R
   1000002     4.90911830E+03   # ~u_L
   2000002     4.90917726E+03   # ~u_R
   1000003     4.90936645E+03   # ~s_L
   2000003     4.90925540E+03   # ~s_R
   1000004     4.90911830E+03   # ~c_L
   2000004     4.90917726E+03   # ~c_R
   1000005     4.90921184E+03   # ~b_1
   2000005     4.90941151E+03   # ~b_2
   1000006     5.04244228E+03   # ~t_1
   2000006     5.34571287E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984134E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984134E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984134E+03   # ~nu_tauL
   1000021     2.32579342E+03   # ~g
   1000022     1.91314896E+02   # ~chi_10
   1000023    -2.16764916E+02   # ~chi_20
   1000025     2.96413697E+02   # ~chi_30
   1000035     3.12904245E+03   # ~chi_40
   1000024     2.14535335E+02   # ~chi_1+
   1000037     3.12904204E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69223787E-01   # N_11
  1  2    -2.32975055E-02   # N_12
  1  3     6.31885444E-01   # N_13
  1  4    -6.16447119E-01   # N_14
  2  1     1.80284456E-02   # N_21
  2  2    -4.70682066E-03   # N_22
  2  3    -7.05102559E-01   # N_23
  2  4    -7.08860496E-01   # N_24
  3  1     8.82895144E-01   # N_31
  3  2     1.29546224E-02   # N_32
  3  3    -3.21416673E-01   # N_33
  3  4     3.42081371E-01   # N_34
  4  1     4.21127165E-04   # N_41
  4  2    -9.99633558E-01   # N_42
  4  3    -1.55721012E-02   # N_43
  4  4     2.21378066E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20148648E-02   # U_11
  1  2     9.99757643E-01   # U_12
  2  1     9.99757643E-01   # U_21
  2  2     2.20148648E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13037343E-02   # V_11
  1  2     9.99509918E-01   # V_12
  2  1     9.99509918E-01   # V_21
  2  2     3.13037343E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99637100E-01   # cos(theta_t)
  1  2    -2.69382313E-02   # sin(theta_t)
  2  1     2.69382313E-02   # -sin(theta_t)
  2  2     9.99637100E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71084239E-01   # cos(theta_b)
  1  2     8.82088227E-01   # sin(theta_b)
  2  1    -8.82088227E-01   # -sin(theta_b)
  2  2     4.71084239E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84679496E-01   # cos(theta_tau)
  1  2     7.28844282E-01   # sin(theta_tau)
  2  1    -7.28844282E-01   # -sin(theta_tau)
  2  2     6.84679496E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90211766E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51770303E+02   # vev(Q)              
         4     3.85523406E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099822E-01   # gprime(Q) DRbar
     2     6.29225404E-01   # g(Q) DRbar
     3     1.07821969E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02734575E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72356938E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79965299E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.30000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02014638E+06   # M^2_Hd              
        22    -5.40738511E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961915E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.74921009E-03   # gluino decays
#          BR         NDA      ID1       ID2
     4.10715955E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.31773720E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.10840665E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.84066220E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.45985974E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.55918338E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.43564894E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.74022718E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.96774342E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.84066220E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.45985974E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.55918338E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.43564894E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.74022718E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.96774342E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.99682737E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.10217566E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.60919496E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.57938085E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.10202265E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.81073756E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.19980608E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.19980608E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.19980608E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.19980608E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35577662E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35577662E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.66423444E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00762357E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.36070069E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.64592159E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.74462750E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.32104600E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.47387266E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.20601662E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.26318009E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.83264098E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.49534552E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.30240185E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.10869887E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.87902930E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.43147728E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.20720206E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.88745587E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.25845794E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -4.79674666E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.24589074E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.65708706E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.80659594E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.07947099E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.51616539E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.49508515E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.48207924E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.43585339E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -4.24940197E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -4.88981746E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.46661044E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.02424265E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.47889046E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.15111945E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -7.59005897E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.05509705E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.84235982E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.12835704E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.24159083E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.78920015E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     4.30194308E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.84165446E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.27006700E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.69213524E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.02685072E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.38361021E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.85339415E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.68743910E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.35257459E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99517392E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.77001443E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.89046099E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.38754154E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78926137E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.27671194E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.75002913E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.38790577E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69386591E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00246493E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38840415E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.85405685E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60998245E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.54417606E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.22799099E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.24989580E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.01946824E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.83951637E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78920015E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.30194308E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.84165446E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.27006700E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.69213524E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.02685072E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.38361021E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.85339415E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.68743910E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.35257459E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99517392E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.77001443E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.89046099E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.38754154E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78926137E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.27671194E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.75002913E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.38790577E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69386591E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00246493E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38840415E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.85405685E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60998245E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.54417606E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.22799099E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.24989580E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.01946824E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.83951637E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543621E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03174611E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04677681E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80157188E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484153E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77877103E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342854E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528979E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20877384E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25796452E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78796753E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60440821E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543621E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03174611E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04677681E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80157188E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484153E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77877103E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342854E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528979E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20877384E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25796452E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78796753E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60440821E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238570E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35969272E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64032829E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72940102E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460029E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259530E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107292E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677899E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832744E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14138162E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06878215E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45972136E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197414E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84883186E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92605017E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80529023E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74911122E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53664735E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62263308E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698347E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37060622E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022962E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80529023E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74911122E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53664735E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62263308E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698347E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37060622E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022962E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849908E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74254258E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53489166E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62077915E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401629E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51125859E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430283E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.27138564E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76319236E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387663E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387663E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462636E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462636E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271771E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38827988E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03051534E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48707012E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98335667E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91827848E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03602785E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91769369E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94100829E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96012022E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90585858E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86324649E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01626687E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94988008E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02651855E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93142453E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20569148E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65589622E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.58931987E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49054629E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56080525E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66109797E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22027825E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57941794E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22027825E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57941794E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13487285E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60412083E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60412083E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093018E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19558835E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19558835E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19558835E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.05913418E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.05913418E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.05913418E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.05913418E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01971159E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01971159E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01971159E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01971159E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95844235E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95844235E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.58796944E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.44453429E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77776923E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77776923E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.13428625E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.94367141E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.54428621E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38831633E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03626666E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97361983E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80778761E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97931014E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97931014E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49002105E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49589602E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70800250E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00709003E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60647675E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91123294E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.88046892E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81988349E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37814024E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93479190E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93479190E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43540995E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72645541E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78947269E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284152E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85758877E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21711253E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01627751E-01    2           5        -5   # BR(h -> b       bb     )
     6.22589753E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20369630E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66259280E-04    2           3        -3   # BR(h -> s       sb     )
     2.01145208E-02    2           4        -4   # BR(h -> c       cb     )
     6.64290687E-02    2          21        21   # BR(h -> g       g      )
     2.32215742E-03    2          22        22   # BR(h -> gam     gam    )
     1.62690512E-03    2          22        23   # BR(h -> Z       gam    )
     2.16844745E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80892483E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01436947E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38906330E-03    2           5        -5   # BR(H -> b       bb     )
     2.32118228E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20624353E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05068704E-06    2           3        -3   # BR(H -> s       sb     )
     9.48255767E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259394E-01    2           6        -6   # BR(H -> t       tb     )
     7.51403554E-04    2          21        21   # BR(H -> g       g      )
     2.63470458E-06    2          22        22   # BR(H -> gam     gam    )
     1.09159947E-06    2          23        22   # BR(H -> Z       gam    )
     3.18051260E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58591809E-04    2          23        23   # BR(H -> Z       Z      )
     8.52432105E-04    2          25        25   # BR(H -> h       h      )
     8.55011714E-24    2          36        36   # BR(H -> A       A      )
     3.39195362E-11    2          23        36   # BR(H -> Z       A      )
     2.55910824E-12    2          24       -37   # BR(H -> W+      H-     )
     2.55910824E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49631956E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87545932E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75212787E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52474683E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45530828E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45383650E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11328935E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891566E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39199704E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771130E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12323950E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093185E-06    2           3        -3   # BR(A -> s       sb     )
     9.38477803E-06    2           4        -4   # BR(A -> c       cb     )
     9.39249846E-01    2           6        -6   # BR(A -> t       tb     )
     8.89017823E-04    2          21        21   # BR(A -> g       g      )
     2.67561563E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307547E-06    2          23        22   # BR(A -> Z       gam    )
     3.09936599E-04    2          23        25   # BR(A -> Z       h      )
     5.99371254E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30135944E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12612301E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74033561E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19948089E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48767634E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69805549E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853740E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23341483E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658071E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601046E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41998152E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13752966E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367215E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40914889E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17642874E-04    2          24        25   # BR(H+ -> W+      h      )
     1.31564083E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28638790E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26680173E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55288859E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
