#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13000469E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.19900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.10485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04200254E+01   # W+
        25     1.25718955E+02   # h
        35     4.00001147E+03   # H
        36     3.99999724E+03   # A
        37     4.00102183E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02589180E+03   # ~d_L
   2000001     4.02258142E+03   # ~d_R
   1000002     4.02524373E+03   # ~u_L
   2000002     4.02315840E+03   # ~u_R
   1000003     4.02589180E+03   # ~s_L
   2000003     4.02258142E+03   # ~s_R
   1000004     4.02524373E+03   # ~c_L
   2000004     4.02315840E+03   # ~c_R
   1000005     2.13422574E+03   # ~b_1
   2000005     4.02564187E+03   # ~b_2
   1000006     8.41639344E+02   # ~t_1
   2000006     2.14689051E+03   # ~t_2
   1000011     4.00444472E+03   # ~e_L
   2000011     4.00357745E+03   # ~e_R
   1000012     4.00333592E+03   # ~nu_eL
   1000013     4.00444472E+03   # ~mu_L
   2000013     4.00357745E+03   # ~mu_R
   1000014     4.00333592E+03   # ~nu_muL
   1000015     4.00428074E+03   # ~tau_1
   2000015     4.00802786E+03   # ~tau_2
   1000016     4.00476684E+03   # ~nu_tauL
   1000021     1.99192357E+03   # ~g
   1000022     3.90597694E+02   # ~chi_10
   1000023    -4.31700911E+02   # ~chi_20
   1000025     4.53065291E+02   # ~chi_30
   1000035     2.06504772E+03   # ~chi_40
   1000024     4.29384499E+02   # ~chi_1+
   1000037     2.06521812E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.46599507E-01   # N_11
  1  2     2.02318447E-02   # N_12
  1  3     4.92134101E-01   # N_13
  1  4     4.47195568E-01   # N_14
  2  1    -3.93422409E-02   # N_21
  2  2     2.37319086E-02   # N_22
  2  3    -7.04345500E-01   # N_23
  2  4     7.08368831E-01   # N_24
  3  1     6.64108549E-01   # N_31
  3  2     2.57990378E-02   # N_32
  3  3     5.11527051E-01   # N_33
  3  4     5.44641461E-01   # N_34
  4  1    -1.09550749E-03   # N_41
  4  2     9.99180604E-01   # N_42
  4  3    -6.44350318E-03   # N_43
  4  4    -3.99424900E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.11832594E-03   # U_11
  1  2     9.99958427E-01   # U_12
  2  1    -9.99958427E-01   # U_21
  2  2     9.11832594E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.64968100E-02   # V_11
  1  2    -9.98402780E-01   # V_12
  2  1    -9.98402780E-01   # V_21
  2  2    -5.64968100E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.03146107E-01   # cos(theta_t)
  1  2     9.94666216E-01   # sin(theta_t)
  2  1    -9.94666216E-01   # -sin(theta_t)
  2  2    -1.03146107E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998395E-01   # cos(theta_b)
  1  2    -1.79164657E-03   # sin(theta_b)
  2  1     1.79164657E-03   # -sin(theta_b)
  2  2     9.99998395E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06163054E-01   # cos(theta_tau)
  1  2     7.08049251E-01   # sin(theta_tau)
  2  1    -7.08049251E-01   # -sin(theta_tau)
  2  2    -7.06163054E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00252548E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30004686E+03  # DRbar Higgs Parameters
         1    -4.19900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43677062E+02   # higgs               
         4     1.61604674E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30004686E+03  # The gauge couplings
     1     3.61985701E-01   # gprime(Q) DRbar
     2     6.35377763E-01   # g(Q) DRbar
     3     1.02945191E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30004686E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.35508196E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30004686E+03  # The trilinear couplings
  1  1     4.99898025E-07   # A_d(Q) DRbar
  2  2     4.99944335E-07   # A_s(Q) DRbar
  3  3     8.93724383E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30004686E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10924399E-07   # A_mu(Q) DRbar
  3  3     1.12061659E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30004686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67073989E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30004686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86801037E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30004686E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03141588E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30004686E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57144669E+07   # M^2_Hd              
        22    -1.59594665E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.10485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.09989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40046326E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.88416637E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.35899349E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.05127732E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.05163730E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.95742842E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.93965696E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.00655638E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.05642500E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.34679423E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.27685046E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.65334759E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.60204848E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.16568034E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.57289474E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.93476998E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.86196406E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.93408568E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.67242198E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.43862535E-04    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     5.65639015E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.73274661E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.11604939E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64615899E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64094789E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83428988E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55440023E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.73132269E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68242727E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.43952464E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12211296E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.40878122E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.88864377E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.91849038E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.24219511E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76256354E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.28196222E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.28483074E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98975232E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81451527E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.51777696E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61692384E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51839268E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58758357E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.09667423E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.56376566E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.43460259E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70454008E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44601557E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76275660E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.27767567E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.41409123E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.67719457E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81988129E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.43738548E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64962851E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52060992E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52112353E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.07870807E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.23414141E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.35146159E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.66262508E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85547479E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76256354E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.28196222E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.28483074E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98975232E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81451527E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.51777696E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61692384E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51839268E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58758357E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.09667423E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.56376566E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.43460259E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70454008E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44601557E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76275660E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.27767567E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.41409123E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.67719457E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81988129E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.43738548E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64962851E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52060992E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52112353E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.07870807E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.23414141E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.35146159E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.66262508E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85547479E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11185652E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.32047617E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.76865968E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.22599555E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77652221E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.39658260E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56798220E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04178276E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.59049053E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.54575822E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.39404528E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60914071E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11185652E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.32047617E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.76865968E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.22599555E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77652221E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.39658260E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56798220E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04178276E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.59049053E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.54575822E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.39404528E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60914071E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06477850E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.05393504E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49641246E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.74074844E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39747265E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.55726283E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80247633E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05740860E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.03265179E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02399222E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.57803846E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42814901E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.94824356E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86393716E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11294962E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00591711E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.07428656E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.25563559E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78060384E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22137883E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54495883E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11294962E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00591711E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.07428656E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.25563559E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78060384E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22137883E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54495883E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43666927E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.11502188E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.73457505E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.66851171E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52088851E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.63956219E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02706733E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.64326586E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33714076E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33714076E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11239365E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11239365E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10093119E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.64194650E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.83610697E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.45116996E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.84720512E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38872523E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.49916408E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39260952E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.98057929E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.81442825E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18741860E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54062233E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18741860E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54062233E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36379122E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53488513E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53488513E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49074073E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06744507E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06744507E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06744495E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.21120125E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.21120125E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.21120125E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.21120125E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.73706849E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.73706849E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.73706849E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.73706849E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.19527325E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.19527325E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.30345310E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.78895357E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.63692903E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.50097460E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.19378652E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.50097460E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.19378652E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.06857594E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     6.93694248E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     6.93694248E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.97819240E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.48516138E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.48516138E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.48514943E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.13321464E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.76782348E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.13321464E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.76782348E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.74296028E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.35104214E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.35104214E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.10494950E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.26964411E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.26964411E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.26964413E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.22439660E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.22439660E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.22439660E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.22439660E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.08126342E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.08126342E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.08126342E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.08126342E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.96932484E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.96932484E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.63996512E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.33091717E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.61480936E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.06093932E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38982124E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38982124E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.59727506E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.21522384E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.00680406E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89154281E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89154281E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.05552476E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90699322E-01    2           5        -5   # BR(h -> b       bb     )
     6.43337006E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27740066E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82838923E-04    2           3        -3   # BR(h -> s       sb     )
     2.09715447E-02    2           4        -4   # BR(h -> c       cb     )
     6.85558311E-02    2          21        21   # BR(h -> g       g      )
     2.38005531E-03    2          22        22   # BR(h -> gam     gam    )
     1.64340409E-03    2          22        23   # BR(h -> Z       gam    )
     2.22488478E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82170856E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46906384E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59711523E-01    2           5        -5   # BR(H -> b       bb     )
     6.06247158E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14354311E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53544156E-04    2           3        -3   # BR(H -> s       sb     )
     7.11099570E-08    2           4        -4   # BR(H -> c       cb     )
     7.12669830E-03    2           6        -6   # BR(H -> t       tb     )
     8.31995547E-07    2          21        21   # BR(H -> g       g      )
     1.84943443E-09    2          22        22   # BR(H -> gam     gam    )
     1.83487523E-09    2          23        22   # BR(H -> Z       gam    )
     1.71248644E-06    2          24       -24   # BR(H -> W+      W-     )
     8.55650151E-07    2          23        23   # BR(H -> Z       Z      )
     6.70571865E-06    2          25        25   # BR(H -> h       h      )
    -5.65093510E-25    2          36        36   # BR(H -> A       A      )
     1.55489197E-20    2          23        36   # BR(H -> Z       A      )
     1.87149686E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56915637E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56915637E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.71924510E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.16823111E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.80013928E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.00800769E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.24768288E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.61023168E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.80223941E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.23889275E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.89734056E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.86580184E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.73353812E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.73353812E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.46833523E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59785072E-01    2           5        -5   # BR(A -> b       bb     )
     6.06330313E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14383544E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53620127E-04    2           3        -3   # BR(A -> s       sb     )
     7.15832681E-08    2           4        -4   # BR(A -> c       cb     )
     7.14171557E-03    2           6        -6   # BR(A -> t       tb     )
     1.46744164E-05    2          21        21   # BR(A -> g       g      )
     6.44321638E-08    2          22        22   # BR(A -> gam     gam    )
     1.61390730E-08    2          23        22   # BR(A -> Z       gam    )
     1.70909692E-06    2          23        25   # BR(A -> Z       h      )
     1.89992106E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56920647E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56920647E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.35702298E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.63150862E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.53326003E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.42694589E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.09705434E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.00742495E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.02013128E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.37880211E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.35075719E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.04906337E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.04906337E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47278615E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.76340808E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05992380E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14264059E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.68857803E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21384621E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49727199E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.66973134E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.70961743E-06    2          24        25   # BR(H+ -> W+      h      )
     2.71509877E-14    2          24        36   # BR(H+ -> W+      A      )
     4.40232745E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.27185295E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.12283665E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57012310E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.62656937E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56080457E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.25968376E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.41201015E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
