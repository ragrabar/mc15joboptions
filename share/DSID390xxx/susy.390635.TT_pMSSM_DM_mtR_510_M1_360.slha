#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10048042E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.69900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04210682E+01   # W+
        25     1.24956254E+02   # h
        35     4.00000787E+03   # H
        36     3.99999630E+03   # A
        37     4.00098516E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01665853E+03   # ~d_L
   2000001     4.01530977E+03   # ~d_R
   1000002     4.01600641E+03   # ~u_L
   2000002     4.01587882E+03   # ~u_R
   1000003     4.01665853E+03   # ~s_L
   2000003     4.01530977E+03   # ~s_R
   1000004     4.01600641E+03   # ~c_L
   2000004     4.01587882E+03   # ~c_R
   1000005     2.03060348E+03   # ~b_1
   2000005     4.02083060E+03   # ~b_2
   1000006     5.36936937E+02   # ~t_1
   2000006     2.04209504E+03   # ~t_2
   1000011     4.00246173E+03   # ~e_L
   2000011     4.00355667E+03   # ~e_R
   1000012     4.00135071E+03   # ~nu_eL
   1000013     4.00246173E+03   # ~mu_L
   2000013     4.00355667E+03   # ~mu_R
   1000014     4.00135071E+03   # ~nu_muL
   1000015     4.00402116E+03   # ~tau_1
   2000015     4.00872020E+03   # ~tau_2
   1000016     4.00359933E+03   # ~nu_tauL
   1000021     1.97450523E+03   # ~g
   1000022     3.40372274E+02   # ~chi_10
   1000023    -3.80535074E+02   # ~chi_20
   1000025     4.03152472E+02   # ~chi_30
   1000035     2.06640496E+03   # ~chi_40
   1000024     3.78087572E+02   # ~chi_1+
   1000037     2.06658081E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.45677916E-01   # N_11
  1  2     1.95428161E-02   # N_12
  1  3     4.95848990E-01   # N_13
  1  4     4.44653014E-01   # N_14
  2  1    -4.47387379E-02   # N_21
  2  2     2.42925741E-02   # N_22
  2  3    -7.03720491E-01   # N_23
  2  4     7.08650680E-01   # N_24
  3  1     6.64802056E-01   # N_31
  3  2     2.51436920E-02   # N_32
  3  3     5.08804647E-01   # N_33
  3  4     5.46373364E-01   # N_34
  4  1    -1.05696082E-03   # N_41
  4  2     9.99197550E-01   # N_42
  4  3    -5.39265813E-03   # N_43
  4  4    -3.96744016E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.63205913E-03   # U_11
  1  2     9.99970875E-01   # U_12
  2  1    -9.99970875E-01   # U_21
  2  2     7.63205913E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.61185323E-02   # V_11
  1  2    -9.98424113E-01   # V_12
  2  1    -9.98424113E-01   # V_21
  2  2    -5.61185323E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -9.00971959E-02   # cos(theta_t)
  1  2     9.95932977E-01   # sin(theta_t)
  2  1    -9.95932977E-01   # -sin(theta_t)
  2  2    -9.00971959E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998822E-01   # cos(theta_b)
  1  2    -1.53492626E-03   # sin(theta_b)
  2  1     1.53492626E-03   # -sin(theta_b)
  2  2     9.99998822E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05896323E-01   # cos(theta_tau)
  1  2     7.08315171E-01   # sin(theta_tau)
  2  1    -7.08315171E-01   # -sin(theta_tau)
  2  2    -7.05896323E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00240506E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00480415E+03  # DRbar Higgs Parameters
         1    -3.69900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44349319E+02   # higgs               
         4     1.62757508E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00480415E+03  # The gauge couplings
     1     3.61182050E-01   # gprime(Q) DRbar
     2     6.35055633E-01   # g(Q) DRbar
     3     1.03520162E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00480415E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.46393013E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00480415E+03  # The trilinear couplings
  1  1     2.75656008E-07   # A_d(Q) DRbar
  2  2     2.75682337E-07   # A_s(Q) DRbar
  3  3     4.91752070E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00480415E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.18866485E-08   # A_mu(Q) DRbar
  3  3     6.25162722E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00480415E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71930075E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00480415E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87990407E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00480415E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03005083E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00480415E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57362942E+07   # M^2_Hd              
        22    -1.47845914E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39899814E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.54566254E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.12885055E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.30903546E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.56909645E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.91189083E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.02819173E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.33363371E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.28783811E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.46765619E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.89989127E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.90367956E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.94489170E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.00326827E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.68097799E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.65160142E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.13927646E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.69472840E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.42334068E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64054306E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65093417E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82518016E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54324600E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.21614018E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66944318E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.41269476E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12562200E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.13682723E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.55242665E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.27577990E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.71934917E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75257813E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.29596650E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.17541429E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98456308E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80444740E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.48231002E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59670623E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52137949E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58111603E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.09487558E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.11018803E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.44615076E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.42782751E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44478684E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75276490E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.26110839E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.34791504E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.83923795E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80969366E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.01405708E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62924869E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52361925E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51479207E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.07475818E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.89655944E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.38217718E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.94169050E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85514090E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75257813E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.29596650E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.17541429E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98456308E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80444740E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.48231002E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59670623E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52137949E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58111603E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.09487558E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.11018803E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.44615076E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.42782751E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44478684E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75276490E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.26110839E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.34791504E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.83923795E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80969366E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.01405708E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62924869E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52361925E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51479207E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.07475818E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.89655944E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.38217718E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.94169050E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85514090E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10354816E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.34530803E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.74434166E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.24271297E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77533246E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91998062E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56526669E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04281111E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.57483291E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.99946992E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.40516628E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.11594039E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10354816E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.34530803E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.74434166E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.24271297E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77533246E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91998062E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56526669E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04281111E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.57483291E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.99946992E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.40516628E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.11594039E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06306081E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.06068263E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46372309E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.74501675E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39567404E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53552076E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79870220E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05949471E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.02791423E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.10836012E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.57850488E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42479990E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00878838E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85706613E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10465988E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00292926E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.25599959E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.31096643E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77912890E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19866668E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54229854E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10465988E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00292926E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.25599959E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.31096643E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77912890E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19866668E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54229854E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43093486E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.08077584E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.13721858E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.71414714E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51830840E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68659886E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02216723E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.95196169E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33736282E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33736282E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11246767E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11246767E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10033901E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66199578E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.01866281E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.16840336E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44588983E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.73087267E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.38669805E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.50424604E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37652849E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.32875221E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.59924709E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18924232E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54257696E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18924232E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54257696E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35779530E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53695431E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53695431E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49107950E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07088407E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07088407E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07088388E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.86452273E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.86452273E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.86452273E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.86452273E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.62150992E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.62150992E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.62150992E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.62150992E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.31128390E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.31128390E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.66434112E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.15092674E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.60933203E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.55184823E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.26476150E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.55184823E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.26476150E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.12899025E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     7.13918184E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     7.13918184E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.16223266E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.51427036E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.51427036E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.51425376E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.18919812E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.83971398E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.18919812E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.83971398E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.88688407E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.51162833E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.51162833E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.28474772E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.30159891E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.30159891E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.30159894E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.25868041E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.25868041E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25868041E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25868041E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.19553944E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.19553944E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.19553944E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.19553944E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.09257754E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.09257754E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66286590E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.50154175E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.55178977E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.41849662E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38249581E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38249581E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.27446338E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.71335083E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.70162286E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.98529605E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.98529605E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.26059250E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.26059250E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95854844E-03   # h decays
#          BR         NDA      ID1       ID2
     6.03291883E-01    2           5        -5   # BR(h -> b       bb     )
     6.55057755E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31892583E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.92225840E-04    2           3        -3   # BR(h -> s       sb     )
     2.13803313E-02    2           4        -4   # BR(h -> c       cb     )
     6.90426287E-02    2          21        21   # BR(h -> g       g      )
     2.37608705E-03    2          22        22   # BR(h -> gam     gam    )
     1.57354862E-03    2          22        23   # BR(h -> Z       gam    )
     2.09810002E-01    2          24       -24   # BR(h -> W+      W-     )
     2.62956247E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49768540E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58549209E-01    2           5        -5   # BR(H -> b       bb     )
     6.03090498E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13238193E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52224010E-04    2           3        -3   # BR(H -> s       sb     )
     7.07362923E-08    2           4        -4   # BR(H -> c       cb     )
     7.08924925E-03    2           6        -6   # BR(H -> t       tb     )
     6.19915658E-07    2          21        21   # BR(H -> g       g      )
     1.68534430E-09    2          22        22   # BR(H -> gam     gam    )
     1.82665500E-09    2          23        22   # BR(H -> Z       gam    )
     1.64275303E-06    2          24       -24   # BR(H -> W+      W-     )
     8.20807651E-07    2          23        23   # BR(H -> Z       Z      )
     6.36353748E-06    2          25        25   # BR(H -> h       h      )
    -3.40502474E-25    2          36        36   # BR(H -> A       A      )
     6.07834025E-20    2          23        36   # BR(H -> Z       A      )
     1.86229129E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57808821E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57808821E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.76639345E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.04919361E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.81440298E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96099590E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.23731416E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.75454833E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.83526887E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.12052336E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.95467347E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.94413986E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.05322317E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.05322317E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.49739295E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58593979E-01    2           5        -5   # BR(A -> b       bb     )
     6.03125275E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13250321E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52279508E-04    2           3        -3   # BR(A -> s       sb     )
     7.12048839E-08    2           4        -4   # BR(A -> c       cb     )
     7.10396495E-03    2           6        -6   # BR(A -> t       tb     )
     1.45968528E-05    2          21        21   # BR(A -> g       g      )
     6.10458523E-08    2          22        22   # BR(A -> gam     gam    )
     1.60566808E-08    2          23        22   # BR(A -> Z       gam    )
     1.63943262E-06    2          23        25   # BR(A -> Z       h      )
     1.87963972E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57809219E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57809219E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.38073363E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.67958949E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.52529265E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.40149857E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.10793612E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.02606076E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.07508621E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.55608581E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.24412775E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.27096457E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.27096457E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49796666E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.73834096E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03211426E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13280782E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.67253509E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20827715E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48581464E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.65375356E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64105128E-06    2          24        25   # BR(H+ -> W+      h      )
     2.26311861E-14    2          24        36   # BR(H+ -> W+      A      )
     4.41014289E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.09785002E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.16875648E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58002298E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.79331914E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56792736E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.21392445E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.25729675E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
