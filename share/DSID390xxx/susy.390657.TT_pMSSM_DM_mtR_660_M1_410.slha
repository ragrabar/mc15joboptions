#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12072271E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.16900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.22485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04202123E+01   # W+
        25     1.25333634E+02   # h
        35     4.00001125E+03   # H
        36     3.99999717E+03   # A
        37     4.00099804E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02326020E+03   # ~d_L
   2000001     4.02052125E+03   # ~d_R
   1000002     4.02261091E+03   # ~u_L
   2000002     4.02104683E+03   # ~u_R
   1000003     4.02326020E+03   # ~s_L
   2000003     4.02052125E+03   # ~s_R
   1000004     4.02261091E+03   # ~c_L
   2000004     4.02104683E+03   # ~c_R
   1000005     2.25162473E+03   # ~b_1
   2000005     4.02439216E+03   # ~b_2
   1000006     7.09366114E+02   # ~t_1
   2000006     2.26202983E+03   # ~t_2
   1000011     4.00384821E+03   # ~e_L
   2000011     4.00361040E+03   # ~e_R
   1000012     4.00273873E+03   # ~nu_eL
   1000013     4.00384821E+03   # ~mu_L
   2000013     4.00361040E+03   # ~mu_R
   1000014     4.00273873E+03   # ~nu_muL
   1000015     4.00424769E+03   # ~tau_1
   2000015     4.00819940E+03   # ~tau_2
   1000016     4.00440593E+03   # ~nu_tauL
   1000021     1.98928043E+03   # ~g
   1000022     3.89267575E+02   # ~chi_10
   1000023    -4.28435451E+02   # ~chi_20
   1000025     4.51536668E+02   # ~chi_30
   1000035     2.06710469E+03   # ~chi_40
   1000024     4.26090668E+02   # ~chi_1+
   1000037     2.06727187E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.29576515E-01   # N_11
  1  2     2.08442113E-02   # N_12
  1  3     5.04995695E-01   # N_13
  1  4     4.60720062E-01   # N_14
  2  1    -3.94960974E-02   # N_21
  2  2     2.37768761E-02   # N_22
  2  3    -7.04319212E-01   # N_23
  2  4     7.08384899E-01   # N_24
  3  1     6.82756887E-01   # N_31
  3  2     2.52529248E-02   # N_32
  3  3     4.98871780E-01   # N_33
  3  4     5.33228160E-01   # N_34
  4  1    -1.09596516E-03   # N_41
  4  2     9.99180899E-01   # N_42
  4  3    -6.38292608E-03   # N_43
  4  4    -3.99448158E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.03265052E-03   # U_11
  1  2     9.99959205E-01   # U_12
  2  1    -9.99959205E-01   # U_21
  2  2     9.03265052E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.65001185E-02   # V_11
  1  2    -9.98402592E-01   # V_12
  2  1    -9.98402592E-01   # V_21
  2  2    -5.65001185E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.35968627E-02   # cos(theta_t)
  1  2     9.96499656E-01   # sin(theta_t)
  2  1    -9.96499656E-01   # -sin(theta_t)
  2  2    -8.35968627E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998258E-01   # cos(theta_b)
  1  2    -1.86654680E-03   # sin(theta_b)
  2  1     1.86654680E-03   # -sin(theta_b)
  2  2     9.99998258E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06130491E-01   # cos(theta_tau)
  1  2     7.08081725E-01   # sin(theta_tau)
  2  1    -7.08081725E-01   # -sin(theta_tau)
  2  2    -7.06130491E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00241722E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20722708E+03  # DRbar Higgs Parameters
         1    -4.16900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43899557E+02   # higgs               
         4     1.62336324E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20722708E+03  # The gauge couplings
     1     3.61758217E-01   # gprime(Q) DRbar
     2     6.35203097E-01   # g(Q) DRbar
     3     1.03104962E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20722708E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14761610E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20722708E+03  # The trilinear couplings
  1  1     4.23700376E-07   # A_d(Q) DRbar
  2  2     4.23739987E-07   # A_s(Q) DRbar
  3  3     7.56932051E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20722708E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.43078191E-08   # A_mu(Q) DRbar
  3  3     9.52706658E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20722708E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67922638E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20722708E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87159559E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20722708E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03042097E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20722708E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57117465E+07   # M^2_Hd              
        22    -1.87230804E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.22485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39968524E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.21984893E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.90258674E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     9.36606715E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.84619926E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.81863548E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.39855854E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.06218158E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.01146674E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.16836750E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.15026000E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.84170474E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.98779301E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.91194474E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.14861156E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.55357211E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.38140630E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.73103871E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.49490054E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.28205042E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.25307173E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.35829266E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.43920027E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.72682515E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.24572443E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.75122185E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64666720E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64836533E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82503798E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53504054E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.74305042E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66405456E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.46332837E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12648025E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.32506659E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.75840324E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.63716492E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.06800205E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76171986E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.20026457E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.28882096E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06633293E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79930660E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.51557697E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58655312E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52300359E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58795557E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.95112941E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.61535664E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56817441E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.69182555E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44720771E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76191261E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20163813E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.44834734E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.38396377E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80466615E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.40977429E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61919357E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52522787E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52166655E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.69816519E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.24735557E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.69919721E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.62857064E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85580154E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76171986E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.20026457E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.28882096E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06633293E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79930660E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.51557697E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58655312E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52300359E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58795557E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.95112941E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.61535664E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56817441E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.69182555E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44720771E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76191261E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20163813E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.44834734E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.38396377E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80466615E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.40977429E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61919357E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52522787E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52166655E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.69816519E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.24735557E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.69919721E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.62857064E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85580154E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10500530E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.90933766E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.32803035E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.65008072E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77609063E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.25482545E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56713373E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03920318E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.33924293E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.55844859E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64516598E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60528843E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10500530E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.90933766E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.32803035E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.65008072E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77609063E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.25482545E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56713373E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03920318E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.33924293E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.55844859E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64516598E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60528843E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06013568E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.96613293E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49821785E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.83034943E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39672545E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.55983831E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80098657E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05318833E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.93856569E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.03098041E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67281261E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42747907E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95441173E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86260341E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10610737E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.66231950E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.08209785E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66439880E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78015695E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22772258E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54407302E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10610737E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.66231950E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.08209785E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66439880E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78015695E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22772258E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54407302E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43000211E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.75369132E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.80342530E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03772360E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52020105E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65153377E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02570066E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.74092121E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33756258E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33756258E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11253390E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11253390E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09980704E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.63999536E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.38861597E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46453701E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.48019226E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40222412E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.44870025E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39765780E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.09598493E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.59116686E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18963405E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54341379E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18963405E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54341379E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35007626E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54079700E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54079700E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49312047E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07909811E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07909811E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07909798E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.72581708E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.72581708E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.72581708E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.72581708E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.24194085E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.24194085E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.24194085E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.24194085E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.59551355E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.59551355E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.73783090E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.19870158E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.26624959E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.07913341E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.37104022E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.07913341E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.37104022E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.32134747E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.92627900E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.92627900E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.95330490E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.40269651E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.40269651E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.40261717E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.25448434E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.92501256E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.25448434E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.92501256E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.97955448E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.71079421E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.71079421E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.48620929E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.34153068E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.34153068E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.34153071E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.25798268E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.25798268E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25798268E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25798268E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.19321200E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.19321200E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.19321200E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.19321200E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.09333255E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.09333255E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.63836057E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.57132357E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.61988361E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.89686666E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40277089E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40277089E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.97187803E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.27178556E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.68459710E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.67464764E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.67464764E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.00601612E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96917657E-01    2           5        -5   # BR(h -> b       bb     )
     6.49258700E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29838026E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87578331E-04    2           3        -3   # BR(h -> s       sb     )
     2.11783309E-02    2           4        -4   # BR(h -> c       cb     )
     6.90048027E-02    2          21        21   # BR(h -> g       g      )
     2.37632467E-03    2          22        22   # BR(h -> gam     gam    )
     1.60805727E-03    2          22        23   # BR(h -> Z       gam    )
     2.16035010E-01    2          24       -24   # BR(h -> W+      W-     )
     2.72365310E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46210363E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59248365E-01    2           5        -5   # BR(H -> b       bb     )
     6.07019715E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14627469E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53867255E-04    2           3        -3   # BR(H -> s       sb     )
     7.11974880E-08    2           4        -4   # BR(H -> c       cb     )
     7.13547072E-03    2           6        -6   # BR(H -> t       tb     )
     7.77588701E-07    2          21        21   # BR(H -> g       g      )
     1.68647784E-09    2          22        22   # BR(H -> gam     gam    )
     1.83809248E-09    2          23        22   # BR(H -> Z       gam    )
     1.65958914E-06    2          24       -24   # BR(H -> W+      W-     )
     8.29219834E-07    2          23        23   # BR(H -> Z       Z      )
     6.49169955E-06    2          25        25   # BR(H -> h       h      )
    -6.55478525E-25    2          36        36   # BR(H -> A       A      )
     4.74650969E-20    2          23        36   # BR(H -> Z       A      )
     1.87590464E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57000662E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57000662E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.75403057E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.20958473E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.82729253E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.91290808E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.86660598E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.80665462E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.90229907E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.23513442E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.71185812E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.19275027E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.81498395E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     6.81498395E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.46124596E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59330394E-01    2           5        -5   # BR(A -> b       bb     )
     6.07117383E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14661833E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53949348E-04    2           3        -3   # BR(A -> s       sb     )
     7.16761896E-08    2           4        -4   # BR(A -> c       cb     )
     7.15098616E-03    2           6        -6   # BR(A -> t       tb     )
     1.46934654E-05    2          21        21   # BR(A -> g       g      )
     6.43410230E-08    2          22        22   # BR(A -> gam     gam    )
     1.61605490E-08    2          23        22   # BR(A -> Z       gam    )
     1.65637408E-06    2          23        25   # BR(A -> Z       h      )
     1.90361671E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57009830E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57009830E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.38485184E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.67987524E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.55457151E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.31726259E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.77761336E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.21926250E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.13720546E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.38988600E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.13689103E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.02671664E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.02671664E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46439515E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.75372230E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06919319E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14591802E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.68237914E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21570384E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50109374E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.66378949E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.65724189E-06    2          24        25   # BR(H+ -> W+      h      )
     2.41864509E-14    2          24        36   # BR(H+ -> W+      A      )
     4.19406148E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.19779921E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.34721703E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57135414E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     8.03716579E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56191530E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.86157724E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.41024113E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
