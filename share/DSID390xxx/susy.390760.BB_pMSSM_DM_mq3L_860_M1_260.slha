#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13059404E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.03900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04172396E+01   # W+
        25     1.25843956E+02   # h
        35     4.00000789E+03   # H
        36     3.99999687E+03   # A
        37     4.00104757E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02605070E+03   # ~d_L
   2000001     4.02265578E+03   # ~d_R
   1000002     4.02539851E+03   # ~u_L
   2000002     4.02351061E+03   # ~u_R
   1000003     4.02605070E+03   # ~s_L
   2000003     4.02265578E+03   # ~s_R
   1000004     4.02539851E+03   # ~c_L
   2000004     4.02351061E+03   # ~c_R
   1000005     9.23402219E+02   # ~b_1
   2000005     4.02547274E+03   # ~b_2
   1000006     9.05679540E+02   # ~t_1
   2000006     2.01891667E+03   # ~t_2
   1000011     4.00463438E+03   # ~e_L
   2000011     4.00336528E+03   # ~e_R
   1000012     4.00352069E+03   # ~nu_eL
   1000013     4.00463438E+03   # ~mu_L
   2000013     4.00336528E+03   # ~mu_R
   1000014     4.00352069E+03   # ~nu_muL
   1000015     4.00479048E+03   # ~tau_1
   2000015     4.00748035E+03   # ~tau_2
   1000016     4.00494522E+03   # ~nu_tauL
   1000021     1.97937695E+03   # ~g
   1000022     2.50639936E+02   # ~chi_10
   1000023    -3.14725206E+02   # ~chi_20
   1000025     3.24843618E+02   # ~chi_30
   1000035     2.05216397E+03   # ~chi_40
   1000024     3.11995564E+02   # ~chi_1+
   1000037     2.05232882E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.83418941E-01   # N_11
  1  2    -1.20373118E-02   # N_12
  1  3    -3.67044361E-01   # N_13
  1  4    -2.91040403E-01   # N_14
  2  1    -5.77811035E-02   # N_21
  2  2     2.49714396E-02   # N_22
  2  3    -7.02215890E-01   # N_23
  2  4     7.09176011E-01   # N_24
  3  1     4.65006824E-01   # N_31
  3  2     2.80911147E-02   # N_32
  3  3     6.10045161E-01   # N_33
  3  4     6.40955884E-01   # N_34
  4  1    -9.86461982E-04   # N_41
  4  2     9.99220906E-01   # N_42
  4  3    -4.02286848E-03   # N_43
  4  4    -3.92482334E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.69460700E-03   # U_11
  1  2     9.99983786E-01   # U_12
  2  1    -9.99983786E-01   # U_21
  2  2     5.69460700E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55172124E-02   # V_11
  1  2    -9.98457730E-01   # V_12
  2  1    -9.98457730E-01   # V_21
  2  2    -5.55172124E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92937625E-01   # cos(theta_t)
  1  2    -1.18637569E-01   # sin(theta_t)
  2  1     1.18637569E-01   # -sin(theta_t)
  2  2     9.92937625E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999528E-01   # cos(theta_b)
  1  2    -9.71596509E-04   # sin(theta_b)
  2  1     9.71596509E-04   # -sin(theta_b)
  2  2     9.99999528E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05655664E-01   # cos(theta_tau)
  1  2     7.08554926E-01   # sin(theta_tau)
  2  1    -7.08554926E-01   # -sin(theta_tau)
  2  2    -7.05655664E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00286275E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30594043E+03  # DRbar Higgs Parameters
         1    -3.03900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43610924E+02   # higgs               
         4     1.61101261E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30594043E+03  # The gauge couplings
     1     3.61970525E-01   # gprime(Q) DRbar
     2     6.36311479E-01   # g(Q) DRbar
     3     1.03004387E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30594043E+03  # The trilinear couplings
  1  1     1.38016203E-06   # A_u(Q) DRbar
  2  2     1.38017475E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30594043E+03  # The trilinear couplings
  1  1     5.06812632E-07   # A_d(Q) DRbar
  2  2     5.06859756E-07   # A_s(Q) DRbar
  3  3     9.07687874E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30594043E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10557933E-07   # A_mu(Q) DRbar
  3  3     1.11690253E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30594043E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67567764E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30594043E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83391212E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30594043E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03564618E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30594043E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57983889E+07   # M^2_Hd              
        22    -6.46079381E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40470425E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.22304591E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42153993E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42153993E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57846007E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57846007E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.07014664E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.06340332E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.31155916E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.82285139E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05924913E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20858125E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.00495078E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16100242E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.71823263E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.28356633E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.60726430E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.15705912E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.31878950E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.11441805E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.43962890E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.98687813E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.75912637E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.88143666E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66547367E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53511890E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79780196E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64144530E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.00399641E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62644770E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18483540E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13542613E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.82856781E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.29874049E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.17980355E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.32272115E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78574731E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.09872665E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.69811975E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.16774146E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80922921E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33031922E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60629181E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52028309E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60893581E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.32432745E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.84206480E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.19211476E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.99055479E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44651342E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78594801E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.79915553E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11600641E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.11871305E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81416407E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.60854117E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63834708E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52246653E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54140624E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.12819665E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.80585156E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.11016457E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.79994365E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85559803E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78574731E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.09872665E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.69811975E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.16774146E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80922921E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33031922E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60629181E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52028309E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60893581E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.32432745E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.84206480E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.19211476E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.99055479E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44651342E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78594801E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.79915553E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11600641E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.11871305E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81416407E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.60854117E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63834708E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52246653E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54140624E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.12819665E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.80585156E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.11016457E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.79994365E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85559803E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14993678E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.22228756E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.15448468E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.32669099E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77676711E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.28072647E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56763271E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06829132E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.81343854E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.32741440E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.15328198E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.33689017E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14993678E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.22228756E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.15448468E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.32669099E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77676711E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.28072647E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56763271E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06829132E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.81343854E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.32741440E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.15328198E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.33689017E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10189392E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.81766699E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35276794E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.95877940E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39910279E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46759264E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80531621E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09924887E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.92442939E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24972735E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.66414431E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42448028E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05795694E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85617759E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15099517E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.34438397E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.69089224E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.82297640E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78020114E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11622039E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54504612E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15099517E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.34438397E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.69089224E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.82297640E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78020114E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11622039E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54504612E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47961819E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.21785730E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53175892E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.55730183E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51980545E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65594129E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02569535E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.71938844E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33475850E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33475850E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11159839E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11159839E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10728621E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.30632445E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.59059806E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.13302794E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.43516364E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.55537696E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08462013E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.37764061E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.25446396E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.47016800E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.42331287E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.70739784E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17599561E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52542029E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17599561E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52542029E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44756908E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49774288E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49774288E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47415664E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99304785E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99304785E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99304764E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.43298398E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.43298398E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.43298398E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.43298398E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.10995560E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.10995560E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.10995560E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.10995560E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.52556161E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.52556161E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.83523627E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.10562543E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.38570143E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.10253132E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42692156E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.10253132E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42692156E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.38003882E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.24615808E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.24615808E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.23673969E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.55310753E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.55310753E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.55310045E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.07980549E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.58944875E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.07980549E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.58944875E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.97056501E-06    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.51111173E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.51111173E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.25125093E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.02061837E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.02061837E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.02061840E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.66189285E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.66189285E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.66189285E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.66189285E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.88728119E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.88728119E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.88728119E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.88728119E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.71925505E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.71925505E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.30517136E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.94555196E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.56148067E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.49897248E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.36515774E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.36515774E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.10302759E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.96592866E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.87403110E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.75409515E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.75409515E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.75912553E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.75912553E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07234450E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88519251E-01    2           5        -5   # BR(h -> b       bb     )
     6.41404827E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27055540E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81293736E-04    2           3        -3   # BR(h -> s       sb     )
     2.09016355E-02    2           4        -4   # BR(h -> c       cb     )
     6.84850129E-02    2          21        21   # BR(h -> g       g      )
     2.38051124E-03    2          22        22   # BR(h -> gam     gam    )
     1.65451000E-03    2          22        23   # BR(h -> Z       gam    )
     2.24674173E-01    2          24       -24   # BR(h -> W+      W-     )
     2.85360748E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49035253E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52445688E-01    2           5        -5   # BR(H -> b       bb     )
     6.03895707E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13522895E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52560762E-04    2           3        -3   # BR(H -> s       sb     )
     7.08437184E-08    2           4        -4   # BR(H -> c       cb     )
     7.10001557E-03    2           6        -6   # BR(H -> t       tb     )
     8.58795498E-07    2          21        21   # BR(H -> g       g      )
     5.14308425E-10    2          22        22   # BR(H -> gam     gam    )
     1.82403733E-09    2          23        22   # BR(H -> Z       gam    )
     1.88228494E-06    2          24       -24   # BR(H -> W+      W-     )
     9.40490437E-07    2          23        23   # BR(H -> Z       Z      )
     7.30171719E-06    2          25        25   # BR(H -> h       h      )
    -1.51285003E-24    2          36        36   # BR(H -> A       A      )
     4.59757189E-20    2          23        36   # BR(H -> Z       A      )
     1.85120876E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61793588E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61793588E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.05775766E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.24070793E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.18811709E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.77598173E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53795144E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.15819908E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.01829877E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.08142478E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.98990290E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.52367881E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.44814779E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.44814779E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.32466742E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48992715E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52498668E-01    2           5        -5   # BR(A -> b       bb     )
     6.03945556E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13540353E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52622617E-04    2           3        -3   # BR(A -> s       sb     )
     7.13017249E-08    2           4        -4   # BR(A -> c       cb     )
     7.11362658E-03    2           6        -6   # BR(A -> t       tb     )
     1.46167023E-05    2          21        21   # BR(A -> g       g      )
     5.66088106E-08    2          22        22   # BR(A -> gam     gam    )
     1.60678033E-08    2          23        22   # BR(A -> Z       gam    )
     1.87844772E-06    2          23        25   # BR(A -> Z       h      )
     1.85903724E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61805483E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61805483E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.78381036E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03454482E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.88781709E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.40990536E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.24746994E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.21682718E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.12761129E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.91964276E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.08839020E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.59070571E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.59070571E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49528138E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.64920603E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03515600E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13388330E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.61548877E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20888406E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48706326E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.59811964E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.87875400E-06    2          24        25   # BR(H+ -> W+      h      )
     3.06637433E-14    2          24        36   # BR(H+ -> W+      A      )
     6.49205595E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.87835277E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.22055643E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61861269E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.37739015E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59943103E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.20873089E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.83767252E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.02774578E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
