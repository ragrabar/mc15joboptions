#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871801E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04027716E+01   # W+
        25     1.24821543E+02   # h
        35     3.00008379E+03   # H
        36     2.99999996E+03   # A
        37     3.00089611E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04197025E+03   # ~d_L
   2000001     3.03688126E+03   # ~d_R
   1000002     3.04106646E+03   # ~u_L
   2000002     3.03780800E+03   # ~u_R
   1000003     3.04197025E+03   # ~s_L
   2000003     3.03688126E+03   # ~s_R
   1000004     3.04106646E+03   # ~c_L
   2000004     3.03780800E+03   # ~c_R
   1000005     1.05282746E+03   # ~b_1
   2000005     3.03578689E+03   # ~b_2
   1000006     1.04909325E+03   # ~t_1
   2000006     3.01861454E+03   # ~t_2
   1000011     3.00622498E+03   # ~e_L
   2000011     3.00223363E+03   # ~e_R
   1000012     3.00483976E+03   # ~nu_eL
   1000013     3.00622498E+03   # ~mu_L
   2000013     3.00223363E+03   # ~mu_R
   1000014     3.00483976E+03   # ~nu_muL
   1000015     2.98553659E+03   # ~tau_1
   2000015     3.02194428E+03   # ~tau_2
   1000016     3.00454280E+03   # ~nu_tauL
   1000021     2.35219593E+03   # ~g
   1000022     3.01128021E+02   # ~chi_10
   1000023     6.31534230E+02   # ~chi_20
   1000025    -2.99466653E+03   # ~chi_30
   1000035     2.99552350E+03   # ~chi_40
   1000024     6.31697512E+02   # ~chi_1+
   1000037     2.99604686E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885253E-01   # N_11
  1  2    -7.71187630E-04   # N_12
  1  3     1.49633932E-02   # N_13
  1  4    -2.23220118E-03   # N_14
  2  1     1.19308500E-03   # N_21
  2  2     9.99606586E-01   # N_22
  2  3    -2.72029199E-02   # N_23
  2  4     6.72693831E-03   # N_24
  3  1    -8.98857157E-03   # N_31
  3  2     1.44885062E-02   # N_32
  3  3     7.06870811E-01   # N_33
  3  4     7.07137147E-01   # N_34
  4  1    -1.21351851E-02   # N_41
  4  2     2.40033843E-02   # N_42
  4  3     7.06660990E-01   # N_43
  4  4    -7.07040890E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99259226E-01   # U_11
  1  2    -3.84837549E-02   # U_12
  2  1     3.84837549E-02   # U_21
  2  2     9.99259226E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954705E-01   # V_11
  1  2    -9.51780320E-03   # V_12
  2  1     9.51780320E-03   # V_21
  2  2     9.99954705E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447803E-01   # cos(theta_t)
  1  2    -5.56954638E-02   # sin(theta_t)
  2  1     5.56954638E-02   # -sin(theta_t)
  2  2     9.98447803E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99905462E-01   # cos(theta_b)
  1  2     1.37501659E-02   # sin(theta_b)
  2  1    -1.37501659E-02   # -sin(theta_b)
  2  2     9.99905462E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06959176E-01   # cos(theta_tau)
  1  2     7.07254356E-01   # sin(theta_tau)
  2  1    -7.07254356E-01   # -sin(theta_tau)
  2  2     7.06959176E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01761174E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718008E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43933561E+02   # higgs               
         4     7.70823782E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718008E+03  # The gauge couplings
     1     3.62569106E-01   # gprime(Q) DRbar
     2     6.37532266E-01   # g(Q) DRbar
     3     1.02371163E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718008E+03  # The trilinear couplings
  1  1     2.71433195E-06   # A_u(Q) DRbar
  2  2     2.71436925E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718008E+03  # The trilinear couplings
  1  1     7.29609696E-07   # A_d(Q) DRbar
  2  2     7.29736950E-07   # A_s(Q) DRbar
  3  3     1.60140818E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718008E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.50292600E-07   # A_mu(Q) DRbar
  3  3     1.52073923E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718008E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50424438E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718008E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15433301E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718008E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07167465E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718008E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.46462043E+04   # M^2_Hd              
        22    -9.03112032E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41073498E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.13215864E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47654476E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47654476E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52345524E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52345524E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.18370611E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.36719193E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.96861531E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.79466550E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10469000E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.89282805E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.12360808E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.28832311E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.30509338E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.00807760E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68020819E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59365214E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11543682E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.04909836E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.64120129E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.54095558E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.19492429E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.18074239E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.94074133E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.70841549E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.56743304E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.49146256E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.43923247E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30981936E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.85387474E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.14919229E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.95290769E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.78255150E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.03784953E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.58518755E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.96020171E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.22774976E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.17100398E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.09463136E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.18342855E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14934783E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59023169E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.11873244E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.19531772E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.09547167E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40976587E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.78770052E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.13420301E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.58311832E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.92464283E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.25425670E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.16532461E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.25274226E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.19020804E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64697370E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52174832E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.02426084E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.26055221E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.71099648E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54782448E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.78255150E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.03784953E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.58518755E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.96020171E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.22774976E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.17100398E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.09463136E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.18342855E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14934783E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59023169E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.11873244E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.19531772E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.09547167E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40976587E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.78770052E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.13420301E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.58311832E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.92464283E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.25425670E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.16532461E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.25274226E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.19020804E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64697370E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52174832E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.02426084E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.26055221E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.71099648E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54782448E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71288402E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03455658E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99266626E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.69532937E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.83578178E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97277666E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.43077807E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53852499E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998668E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.32713111E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.09024727E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.99665089E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71288402E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03455658E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99266626E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.69532937E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.83578178E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97277666E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.43077807E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53852499E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998668E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.32713111E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.09024727E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.99665089E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.65224969E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.62149276E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12926226E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.24924498E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59664265E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70729737E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10064498E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.58807634E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.75704796E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19154242E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.80711688E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71304602E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03963996E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98279239E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.74364400E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.19784040E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97756745E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.02445791E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71304602E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03963996E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98279239E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.74364400E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.19784040E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97756745E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.02445791E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71295574E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03955833E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98251563E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.41442108E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.12294312E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97790408E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.17971473E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.35442080E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85918751E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.76756157E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.00458178E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.09859128E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.53302051E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.35845260E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.33697132E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.95814521E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17633312E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.33171745E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.46682825E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.88185178E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.57033818E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.49546888E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.30392087E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.30392087E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.94096277E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.04310841E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54431873E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54431873E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25136369E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25136369E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.23149461E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.23149461E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.79461256E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.71412994E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.02866141E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.39002210E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.39002210E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.65477308E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.94976719E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.50757368E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.50757368E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28189555E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28189555E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.81852157E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.81852157E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.75710237E-03   # h decays
#          BR         NDA      ID1       ID2
     6.71956708E-01    2           5        -5   # BR(h -> b       bb     )
     5.47819716E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93930474E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.11699719E-04    2           3        -3   # BR(h -> s       sb     )
     1.77756007E-02    2           4        -4   # BR(h -> c       cb     )
     5.76112015E-02    2          21        21   # BR(h -> g       g      )
     1.96706828E-03    2          22        22   # BR(h -> gam     gam    )
     1.29282996E-03    2          22        23   # BR(h -> Z       gam    )
     1.72494072E-01    2          24       -24   # BR(h -> W+      W-     )
     2.15149180E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40942115E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40001507E-01    2           5        -5   # BR(H -> b       bb     )
     1.76435958E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23835387E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66071623E-04    2           3        -3   # BR(H -> s       sb     )
     2.16119063E-07    2           4        -4   # BR(H -> c       cb     )
     2.15592608E-02    2           6        -6   # BR(H -> t       tb     )
     2.60882912E-05    2          21        21   # BR(H -> g       g      )
     1.77835099E-08    2          22        22   # BR(H -> gam     gam    )
     8.32454754E-09    2          23        22   # BR(H -> Z       gam    )
     2.96742704E-05    2          24       -24   # BR(H -> W+      W-     )
     1.48188439E-05    2          23        23   # BR(H -> Z       Z      )
     7.87740221E-05    2          25        25   # BR(H -> h       h      )
     5.88714676E-23    2          36        36   # BR(H -> A       A      )
     3.38105021E-19    2          23        36   # BR(H -> Z       A      )
     1.94171921E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.07228999E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.68437835E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.56759945E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.67842343E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.38827421E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33068143E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83871328E-01    2           5        -5   # BR(A -> b       bb     )
     1.86875230E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60745234E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.11532352E-04    2           3        -3   # BR(A -> s       sb     )
     2.29002927E-07    2           4        -4   # BR(A -> c       cb     )
     2.28319388E-02    2           6        -6   # BR(A -> t       tb     )
     6.72378743E-05    2          21        21   # BR(A -> g       g      )
     4.47417016E-08    2          22        22   # BR(A -> gam     gam    )
     6.62542910E-08    2          23        22   # BR(A -> Z       gam    )
     3.13122232E-05    2          23        25   # BR(A -> Z       h      )
     2.62690306E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21934332E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31006581E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.91431873E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04093772E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10501307E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38963076E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44915151E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07207265E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96539209E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02154082E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18285356E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.00888688E-05    2          24        25   # BR(H+ -> W+      h      )
     7.30607817E-14    2          24        36   # BR(H+ -> W+      A      )
     1.89743565E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.62116573E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.77858458E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
