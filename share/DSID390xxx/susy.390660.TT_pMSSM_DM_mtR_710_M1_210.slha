#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12013915E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.47900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194450E+01   # W+
        25     1.25635990E+02   # h
        35     4.00000589E+03   # H
        36     3.99999648E+03   # A
        37     4.00103009E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02308980E+03   # ~d_L
   2000001     4.02038129E+03   # ~d_R
   1000002     4.02244014E+03   # ~u_L
   2000002     4.02097426E+03   # ~u_R
   1000003     4.02308980E+03   # ~s_L
   2000003     4.02038129E+03   # ~s_R
   1000004     4.02244014E+03   # ~c_L
   2000004     4.02097426E+03   # ~c_R
   1000005     2.08359021E+03   # ~b_1
   2000005     4.02411268E+03   # ~b_2
   1000006     7.35771608E+02   # ~t_1
   2000006     2.09616103E+03   # ~t_2
   1000011     4.00385617E+03   # ~e_L
   2000011     4.00363727E+03   # ~e_R
   1000012     4.00274566E+03   # ~nu_eL
   1000013     4.00385617E+03   # ~mu_L
   2000013     4.00363727E+03   # ~mu_R
   1000014     4.00274566E+03   # ~nu_muL
   1000015     4.00495392E+03   # ~tau_1
   2000015     4.00760862E+03   # ~tau_2
   1000016     4.00443924E+03   # ~nu_tauL
   1000021     1.98622749E+03   # ~g
   1000022     1.98072141E+02   # ~chi_10
   1000023    -2.58457329E+02   # ~chi_20
   1000025     2.70658042E+02   # ~chi_30
   1000035     2.06531672E+03   # ~chi_40
   1000024     2.55222434E+02   # ~chi_1+
   1000037     2.06548933E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.64061127E-01   # N_11
  1  2    -1.22964947E-02   # N_12
  1  3    -3.99037754E-01   # N_13
  1  4    -3.06620346E-01   # N_14
  2  1    -7.10772765E-02   # N_21
  2  2     2.56093391E-02   # N_22
  2  3    -7.00207491E-01   # N_23
  2  4     7.09930737E-01   # N_24
  3  1     4.98342733E-01   # N_31
  3  2     2.68870202E-02   # N_32
  3  3     5.92005036E-01   # N_33
  3  4     6.32820390E-01   # N_34
  4  1    -9.54516468E-04   # N_41
  4  2     9.99234730E-01   # N_42
  4  3    -2.89438088E-03   # N_43
  4  4    -3.89957093E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.09834579E-03   # U_11
  1  2     9.99991602E-01   # U_12
  2  1    -9.99991602E-01   # U_21
  2  2     4.09834579E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51605484E-02   # V_11
  1  2    -9.98477498E-01   # V_12
  2  1    -9.98477498E-01   # V_21
  2  2    -5.51605484E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.00784644E-01   # cos(theta_t)
  1  2     9.94908265E-01   # sin(theta_t)
  2  1    -9.94908265E-01   # -sin(theta_t)
  2  2    -1.00784644E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999471E-01   # cos(theta_b)
  1  2    -1.02859113E-03   # sin(theta_b)
  2  1     1.02859113E-03   # -sin(theta_b)
  2  2     9.99999471E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05377448E-01   # cos(theta_tau)
  1  2     7.08831895E-01   # sin(theta_tau)
  2  1    -7.08831895E-01   # -sin(theta_tau)
  2  2    -7.05377448E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00289392E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20139150E+03  # DRbar Higgs Parameters
         1    -2.47900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43794551E+02   # higgs               
         4     1.61370254E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20139150E+03  # The gauge couplings
     1     3.61838343E-01   # gprime(Q) DRbar
     2     6.35823538E-01   # g(Q) DRbar
     3     1.03123226E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20139150E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13742122E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20139150E+03  # The trilinear couplings
  1  1     4.17456564E-07   # A_d(Q) DRbar
  2  2     4.17495987E-07   # A_s(Q) DRbar
  3  3     7.47577437E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20139150E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.16288565E-08   # A_mu(Q) DRbar
  3  3     9.25711909E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20139150E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69244413E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20139150E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83868138E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20139150E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03684727E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20139150E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58226181E+07   # M^2_Hd              
        22    -5.24153282E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40245441E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.15013949E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.71668881E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.27300253E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.10912960E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.18591331E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.87765683E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.33956736E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.40824614E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.32849444E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.75132121E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.44894652E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.36098156E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.97789175E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.85421236E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.14292736E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.72686751E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.47570200E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.53883379E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.70267118E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.93942228E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.39769113E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     1.94599801E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64322733E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.59658615E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79977684E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61798589E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.05032110E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64652502E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.96448226E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13165255E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.85368829E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     6.34071408E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.72317402E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.10137113E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76037918E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.02502193E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.29132851E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.29370147E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81856692E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34503940E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62487776E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51699413E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58723055E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.19844958E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.83189339E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.39105127E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.80622211E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43821774E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76056792E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73542383E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.34211424E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.01573759E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82343587E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.95078074E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65684395E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51923042E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51984518E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09598282E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.39249937E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.63126077E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.32397723E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85334979E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76037918E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.02502193E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.29132851E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.29370147E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81856692E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34503940E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62487776E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51699413E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58723055E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.19844958E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.83189339E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.39105127E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.80622211E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43821774E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76056792E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73542383E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.34211424E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.01573759E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82343587E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.95078074E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65684395E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51923042E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51984518E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09598282E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.39249937E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.63126077E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.32397723E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85334979E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12173529E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.17971056E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.12637255E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.92679142E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77084933E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.71856671E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55546274E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07323722E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47404010E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.03998505E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.47555511E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.93653120E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12173529E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.17971056E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.12637255E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.92679142E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77084933E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.71856671E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55546274E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07323722E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47404010E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.03998505E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.47555511E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.93653120E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09225932E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75287531E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29047300E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.09760859E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38905889E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46388643E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78502126E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09227936E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.79177568E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.45841845E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.80384484E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41236799E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.14423088E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83174656E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12282881E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.30302307E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.23054350E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.36587592E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77402212E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11122777E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53294950E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12282881E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.30302307E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.23054350E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.36587592E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77402212E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11122777E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53294950E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45357891E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17873564E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.01779145E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.04483654E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51095481E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77404157E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00824383E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.95257808E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33501237E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33501237E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11168223E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11168223E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10661081E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.67684837E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.92145830E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.44933923E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.94719391E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39416651E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.97956125E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39006779E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.86268200E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.12522612E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17735158E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52726654E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17735158E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52726654E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43859416E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50246681E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50246681E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47664243E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00282216E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00282216E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00282190E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.32376023E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.32376023E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.32376023E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.32376023E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.44125665E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.44125665E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.44125665E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.44125665E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.38281590E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.38281590E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.90996671E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.79803861E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.05260778E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.04415145E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.35154746E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.04415145E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.35154746E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.30405206E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.07608492E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.07608492E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.06319308E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.20788774E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.20788774E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.20787994E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.16618658E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.51285976E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.16618658E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.51285976E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.15082052E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.46992230E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.46992230E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.06298882E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.93622865E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.93622865E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.93622874E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.27703224E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.27703224E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.27703224E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.27703224E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.25673184E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.25673184E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.25673184E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.25673184E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.98908490E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.98908490E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.67549970E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.91243655E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.41495089E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.43983624E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38842454E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38842454E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.08276762E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01727018E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.16019280E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.93616511E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.93616511E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.06137289E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93827349E-01    2           5        -5   # BR(h -> b       bb     )
     6.42080170E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27295508E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81957375E-04    2           3        -3   # BR(h -> s       sb     )
     2.09302137E-02    2           4        -4   # BR(h -> c       cb     )
     6.82343977E-02    2          21        21   # BR(h -> g       g      )
     2.36996126E-03    2          22        22   # BR(h -> gam     gam    )
     1.62906723E-03    2          22        23   # BR(h -> Z       gam    )
     2.20201984E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78897572E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44016416E+01   # H decays
#          BR         NDA      ID1       ID2
     3.48326986E-01    2           5        -5   # BR(H -> b       bb     )
     6.09466639E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15492642E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54890650E-04    2           3        -3   # BR(H -> s       sb     )
     7.14981479E-08    2           4        -4   # BR(H -> c       cb     )
     7.16560300E-03    2           6        -6   # BR(H -> t       tb     )
     8.80918097E-07    2          21        21   # BR(H -> g       g      )
     9.46242862E-11    2          22        22   # BR(H -> gam     gam    )
     1.84168754E-09    2          23        22   # BR(H -> Z       gam    )
     1.91654620E-06    2          24       -24   # BR(H -> W+      W-     )
     9.57609327E-07    2          23        23   # BR(H -> Z       Z      )
     7.17162330E-06    2          25        25   # BR(H -> h       h      )
    -2.22360234E-24    2          36        36   # BR(H -> A       A      )
     5.73626638E-20    2          23        36   # BR(H -> Z       A      )
     1.86211611E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63319895E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63319895E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.36848724E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.08491908E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.35112331E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.57863266E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.17701199E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.61929877E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.22009816E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.95262654E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.80774965E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.02930271E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.26966281E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.26966281E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43981863E+01   # A decays
#          BR         NDA      ID1       ID2
     3.48374984E-01    2           5        -5   # BR(A -> b       bb     )
     6.09508701E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15507344E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54949612E-04    2           3        -3   # BR(A -> s       sb     )
     7.19585098E-08    2           4        -4   # BR(A -> c       cb     )
     7.17915266E-03    2           6        -6   # BR(A -> t       tb     )
     1.47513437E-05    2          21        21   # BR(A -> g       g      )
     5.26462208E-08    2          22        22   # BR(A -> gam     gam    )
     1.62220382E-08    2          23        22   # BR(A -> Z       gam    )
     1.91263380E-06    2          23        25   # BR(A -> Z       h      )
     1.86517079E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63332799E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63332799E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.05097641E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.35190795E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.11110152E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.21285742E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.33568649E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.60659057E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.36054426E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.10427406E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.66634725E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.33122664E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.33122664E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43135076E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.55775404E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10616713E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15899110E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55695953E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22310876E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51632804E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54182603E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.91777185E-06    2          24        25   # BR(H+ -> W+      h      )
     2.85831947E-14    2          24        36   # BR(H+ -> W+      A      )
     6.29879804E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.49217970E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.57745101E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63799693E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.24108778E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60932019E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.15092959E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.69175204E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
