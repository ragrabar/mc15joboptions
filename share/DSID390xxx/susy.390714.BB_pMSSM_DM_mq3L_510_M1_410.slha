#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10033344E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.39900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04189555E+01   # W+
        25     1.24911274E+02   # h
        35     4.00000841E+03   # H
        36     3.99999659E+03   # A
        37     4.00098299E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01652114E+03   # ~d_L
   2000001     4.01506230E+03   # ~d_R
   1000002     4.01586222E+03   # ~u_L
   2000002     4.01630533E+03   # ~u_R
   1000003     4.01652114E+03   # ~s_L
   2000003     4.01506230E+03   # ~s_R
   1000004     4.01586222E+03   # ~c_L
   2000004     4.01630533E+03   # ~c_R
   1000005     5.56860091E+02   # ~b_1
   2000005     4.02030388E+03   # ~b_2
   1000006     5.42013705E+02   # ~t_1
   2000006     2.02401141E+03   # ~t_2
   1000011     4.00279091E+03   # ~e_L
   2000011     4.00284051E+03   # ~e_R
   1000012     4.00167302E+03   # ~nu_eL
   1000013     4.00279091E+03   # ~mu_L
   2000013     4.00284051E+03   # ~mu_R
   1000014     4.00167302E+03   # ~nu_muL
   1000015     4.00391705E+03   # ~tau_1
   2000015     4.00843544E+03   # ~tau_2
   1000016     4.00391987E+03   # ~nu_tauL
   1000021     1.96405209E+03   # ~g
   1000022     4.00032297E+02   # ~chi_10
   1000023    -4.50160218E+02   # ~chi_20
   1000025     4.64638709E+02   # ~chi_30
   1000035     2.05618128E+03   # ~chi_40
   1000024     4.48077794E+02   # ~chi_1+
   1000037     2.05634530E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40555946E-01   # N_11
  1  2    -1.62919669E-02   # N_12
  1  3    -4.06282852E-01   # N_13
  1  4    -3.57958821E-01   # N_14
  2  1    -3.84275397E-02   # N_21
  2  2     2.36139004E-02   # N_22
  2  3    -7.04487148E-01   # N_23
  2  4     7.08282123E-01   # N_24
  3  1     5.40358962E-01   # N_31
  3  2     2.90622775E-02   # N_32
  3  3     5.81880136E-01   # N_33
  3  4     6.07110437E-01   # N_34
  4  1    -1.10324866E-03   # N_41
  4  2     9.99165822E-01   # N_42
  4  3    -6.89997523E-03   # N_43
  4  4    -4.02347317E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.76411872E-03   # U_11
  1  2     9.99952330E-01   # U_12
  2  1    -9.99952330E-01   # U_21
  2  2     9.76411872E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.69105797E-02   # V_11
  1  2    -9.98379280E-01   # V_12
  2  1    -9.98379280E-01   # V_21
  2  2    -5.69105797E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95926195E-01   # cos(theta_t)
  1  2    -9.01721360E-02   # sin(theta_t)
  2  1     9.01721360E-02   # -sin(theta_t)
  2  2     9.95926195E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999038E-01   # cos(theta_b)
  1  2    -1.38708294E-03   # sin(theta_b)
  2  1     1.38708294E-03   # -sin(theta_b)
  2  2     9.99999038E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05996727E-01   # cos(theta_tau)
  1  2     7.08215095E-01   # sin(theta_tau)
  2  1    -7.08215095E-01   # -sin(theta_tau)
  2  2    -7.05996727E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00230167E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00333437E+03  # DRbar Higgs Parameters
         1    -4.39900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44401108E+02   # higgs               
         4     1.63219837E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00333437E+03  # The gauge couplings
     1     3.61015834E-01   # gprime(Q) DRbar
     2     6.35609075E-01   # g(Q) DRbar
     3     1.03613623E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00333437E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.51142962E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00333437E+03  # The trilinear couplings
  1  1     2.77691646E-07   # A_d(Q) DRbar
  2  2     2.77718057E-07   # A_s(Q) DRbar
  3  3     4.94924116E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00333437E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.20177658E-08   # A_mu(Q) DRbar
  3  3     6.26424866E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00333437E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71812985E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00333437E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87548764E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00333437E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02750238E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00333437E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56819817E+07   # M^2_Hd              
        22    -2.04880640E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40152984E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.98557889E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45153948E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45153948E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54846052E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54846052E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.87789543E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13525445E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.69945234E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16971859E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.26569811E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.32182313E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.52155807E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.12837435E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.36201083E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.36630767E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.88049609E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.99090886E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.12859506E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66715714E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52357613E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79236567E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60194139E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59741936E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59662280E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.17016740E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13827683E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.17965178E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.99704096E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.03258297E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.56166104E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78140159E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74681417E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.27335329E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46728825E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76754983E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.51770174E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.52300329E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53305863E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60775462E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.84387346E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.99229041E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.57778670E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.69620692E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45703439E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78160342E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.63475867E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.12617864E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.56139388E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77289106E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.62429578E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.55574505E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53525236E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54121357E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00212975E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.08364979E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.11340359E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.63225637E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85844453E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78140159E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74681417E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.27335329E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46728825E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76754983E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.51770174E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.52300329E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53305863E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60775462E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.84387346E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.99229041E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.57778670E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.69620692E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45703439E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78160342E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.63475867E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.12617864E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.56139388E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77289106E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.62429578E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.55574505E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53525236E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54121357E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00212975E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.08364979E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.11340359E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.63225637E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85844453E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12434503E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07303282E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.60399562E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65615940E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78175266E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.57254471E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57862528E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03032780E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07993771E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47178566E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.90533769E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.74331322E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12434503E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07303282E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.60399562E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65615940E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78175266E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.57254471E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57862528E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03032780E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07993771E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47178566E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.90533769E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.74331322E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06369927E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.53811497E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48035561E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23285270E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40609236E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.55064008E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81984040E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.05706472E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60429847E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97986977E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98100677E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43976479E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.89637725E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88730527E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12541474E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.22925532E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03569426E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.86593628E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78589029E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.24990890E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55540473E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12541474E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.22925532E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03569426E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.86593628E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78589029E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.24990890E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55540473E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44856736E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11471708E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.39197183E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.50574829E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52828642E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.55291676E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04173802E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.65923023E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33578093E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33578093E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11194261E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11194261E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10455293E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.44745156E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.84151406E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.92168214E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.72863845E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.18452254E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.38271216E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.03592594E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.63797737E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.05733106E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.36957239E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.59759635E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18008962E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53038353E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18008962E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53038353E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41832193E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50717466E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50717466E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47456951E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01077869E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01077869E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01077845E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.63069734E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.63069734E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.63069734E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.63069734E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.43566215E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.43566215E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.43566215E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.43566215E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.59318528E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.59318528E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.92484788E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.92631214E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.21293259E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.47061256E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.76311555E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.47061256E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.76311555E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.76003281E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.29350230E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.29350230E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.29501914E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.65307314E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.65307314E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.65306089E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.03017683E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.33600739E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.03017683E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.33600739E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.22201183E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.06192015E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.06192015E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.80812206E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.11999730E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.11999730E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.11999739E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     6.85992807E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     6.85992807E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     6.85992807E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     6.85992807E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.28661961E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.28661961E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.28661961E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.28661961E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.16103978E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.16103978E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.44635620E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.17754923E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.12724856E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.53900996E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.03298537E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.03298537E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.80831914E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.02110093E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.14130730E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88701457E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.88701457E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.89832422E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.89832422E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.92291579E-03   # h decays
#          BR         NDA      ID1       ID2
     6.00717640E-01    2           5        -5   # BR(h -> b       bb     )
     6.60741969E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33905017E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.96532612E-04    2           3        -3   # BR(h -> s       sb     )
     2.15682853E-02    2           4        -4   # BR(h -> c       cb     )
     6.98058729E-02    2          21        21   # BR(h -> g       g      )
     2.39516592E-03    2          22        22   # BR(h -> gam     gam    )
     1.58133279E-03    2          22        23   # BR(h -> Z       gam    )
     2.10741728E-01    2          24       -24   # BR(h -> W+      W-     )
     2.63853403E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.58390522E+01   # H decays
#          BR         NDA      ID1       ID2
     3.65979263E-01    2           5        -5   # BR(H -> b       bb     )
     5.93778456E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09945680E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48329532E-04    2           3        -3   # BR(H -> s       sb     )
     6.96412010E-08    2           4        -4   # BR(H -> c       cb     )
     6.97949831E-03    2           6        -6   # BR(H -> t       tb     )
     5.50885360E-07    2          21        21   # BR(H -> g       g      )
     3.29036831E-09    2          22        22   # BR(H -> gam     gam    )
     1.79816054E-09    2          23        22   # BR(H -> Z       gam    )
     1.56684996E-06    2          24       -24   # BR(H -> W+      W-     )
     7.82882299E-07    2          23        23   # BR(H -> Z       Z      )
     6.38457393E-06    2          25        25   # BR(H -> h       h      )
    -2.86838047E-26    2          36        36   # BR(H -> A       A      )
    -5.08898006E-22    2          23        36   # BR(H -> Z       A      )
     1.84976387E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.53983140E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.53983140E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.22575205E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.89639251E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.41207220E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.53966413E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.63170661E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.39387329E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.19088810E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.13346126E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.91715730E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.68810506E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.73036396E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     9.73036396E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.45703082E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.58401746E+01   # A decays
#          BR         NDA      ID1       ID2
     3.65997436E-01    2           5        -5   # BR(A -> b       bb     )
     5.93769074E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09942198E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48365929E-04    2           3        -3   # BR(A -> s       sb     )
     7.01002915E-08    2           4        -4   # BR(A -> c       cb     )
     6.99376204E-03    2           6        -6   # BR(A -> t       tb     )
     1.43704123E-05    2          21        21   # BR(A -> g       g      )
     6.45411609E-08    2          22        22   # BR(A -> gam     gam    )
     1.58017819E-08    2          23        22   # BR(A -> Z       gam    )
     1.56357117E-06    2          23        25   # BR(A -> Z       h      )
     1.88184972E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.53961336E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.53961336E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.94346334E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.30823102E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.21110674E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.03203189E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.19394341E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.69022617E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.31230050E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.19916049E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.55401297E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.00525969E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.00525969E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.60863946E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.89963633E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.91308201E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09072093E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.77576407E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18443419E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43676201E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.75311148E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.55840388E-06    2          24        25   # BR(H+ -> W+      h      )
     2.19099822E-14    2          24        36   # BR(H+ -> W+      A      )
     5.55194301E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.85880000E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.74865134E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.53501686E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.07165844E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.52659738E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.04483119E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.44213972E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.97382678E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
