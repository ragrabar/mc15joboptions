#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.93814603E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     2.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03998878E+01   # W+
        25     1.27153746E+02   # h
        35     3.00022578E+03   # H
        36     3.00000047E+03   # A
        37     3.00111728E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00801688E+03   # ~d_L
   2000001     3.00229274E+03   # ~d_R
   1000002     3.00709464E+03   # ~u_L
   2000002     3.00736040E+03   # ~u_R
   1000003     3.00801688E+03   # ~s_L
   2000003     3.00229274E+03   # ~s_R
   1000004     3.00709464E+03   # ~c_L
   2000004     3.00736040E+03   # ~c_R
   1000005     3.11529899E+02   # ~b_1
   2000005     3.00726426E+03   # ~b_2
   1000006     3.02938280E+02   # ~t_1
   2000006     3.01592107E+03   # ~t_2
   1000011     3.00816888E+03   # ~e_L
   2000011     2.99804889E+03   # ~e_R
   1000012     3.00677779E+03   # ~nu_eL
   1000013     3.00816888E+03   # ~mu_L
   2000013     2.99804889E+03   # ~mu_R
   1000014     3.00677779E+03   # ~nu_muL
   1000015     2.98647350E+03   # ~tau_1
   2000015     3.02299937E+03   # ~tau_2
   1000016     3.00787524E+03   # ~nu_tauL
   1000021     2.30906040E+03   # ~g
   1000022     1.52551651E+02   # ~chi_10
   1000023     3.20854426E+02   # ~chi_20
   1000025    -3.01567529E+03   # ~chi_30
   1000035     3.01570029E+03   # ~chi_40
   1000024     3.20999469E+02   # ~chi_1+
   1000037     3.01661634E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891913E-01   # N_11
  1  2     3.82825384E-04   # N_12
  1  3    -1.46974586E-02   # N_13
  1  4     3.31786766E-07   # N_14
  2  1     1.67349138E-06   # N_21
  2  2     9.99657115E-01   # N_22
  2  3     2.61519951E-02   # N_23
  2  4     1.31328839E-03   # N_24
  3  1     1.03959345E-02   # N_31
  3  2    -1.94185599E-02   # N_32
  3  3     7.06762426E-01   # N_33
  3  4     7.07107995E-01   # N_34
  4  1    -1.03964604E-02   # N_41
  4  2     1.75620201E-02   # N_42
  4  3    -7.06814637E-01   # N_43
  4  4     7.07104347E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99316309E-01   # U_11
  1  2     3.69718056E-02   # U_12
  2  1    -3.69718056E-02   # U_21
  2  2     9.99316309E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998279E-01   # V_11
  1  2    -1.85537181E-03   # V_12
  2  1    -1.85537181E-03   # V_21
  2  2    -9.99998279E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98665537E-01   # cos(theta_t)
  1  2    -5.16444112E-02   # sin(theta_t)
  2  1     5.16444112E-02   # -sin(theta_t)
  2  2     9.98665537E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99712025E-01   # cos(theta_b)
  1  2    -2.39972305E-02   # sin(theta_b)
  2  1     2.39972305E-02   # -sin(theta_b)
  2  2     9.99712025E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06891874E-01   # cos(theta_tau)
  1  2     7.07321623E-01   # sin(theta_tau)
  2  1    -7.07321623E-01   # -sin(theta_tau)
  2  2    -7.06891874E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00287178E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.38146028E+02  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45057141E+02   # higgs               
         4     1.27792369E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.38146028E+02  # The gauge couplings
     1     3.60587036E-01   # gprime(Q) DRbar
     2     6.38108566E-01   # g(Q) DRbar
     3     1.03679681E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.38146028E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.42490139E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.38146028E+02  # The trilinear couplings
  1  1     2.66378496E-07   # A_d(Q) DRbar
  2  2     2.66410558E-07   # A_s(Q) DRbar
  3  3     5.55635082E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.38146028E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.29717257E-07   # A_mu(Q) DRbar
  3  3     1.31533459E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.38146028E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.63123154E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.38146028E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.12052935E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.38146028E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02544593E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.38146028E+02  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -1.00429865E+05   # M^2_Hd              
        22    -9.24762462E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     2.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41341356E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.74721697E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48045600E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48045600E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51954400E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51954400E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.12052187E-05   # stop1 decays
#          BR         NDA      ID1       ID2
     7.46468831E-02    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
     5.75504821E-04    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3
     9.24777612E-01    3     1000022         5        24   # BR(~t_1 -> ~chi_10  b  W+)
#
#         PDG            Width
DECAY   2000006     1.27857744E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.09708313E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.41971370E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.86655999E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.84543804E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.73285148E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65190331E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25281258E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.84615596E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
#
#         PDG            Width
DECAY   2000005     5.91224622E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.76183414E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.70092900E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -4.61559600E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.07511822E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.01581568E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.15536735E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.69174953E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.03165674E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.83783508E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61126139E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.22465704E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.10570322E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.24648884E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54054950E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.24510050E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45945050E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03679354E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.78818058E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61061318E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.21896888E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.11253613E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.71478505E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.39491831E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.21098313E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.56050817E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.03165674E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.83783508E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61126139E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.22465704E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.10570322E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.24648884E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54054950E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.24510050E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45945050E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03679354E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.78818058E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61061318E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.21896888E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.11253613E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.71478505E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.39491831E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.21098313E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.56050817E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.95590747E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79567108E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00822366E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01220923E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54267116E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.75160990E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.95590747E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79567108E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00822366E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01220923E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54267116E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.75160990E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.77708337E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.47669187E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17565673E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34765139E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72045539E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.55729619E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14877140E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.38273841E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.31871154E-07    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.29390022E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.00419391E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.95619317E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.76388997E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00653999E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.01707101E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.95619317E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.76388997E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00653999E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.01707101E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.95803205E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.76295257E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00629055E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.01741420E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.57669399E-02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99979259E-01    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     2.07412570E-05    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.03580810E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     5.67894117E-11    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.84839718E-08    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     5.67894117E-11    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.84839718E-08    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.52547983E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.60936472E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.72209230E-11    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.72209230E-11    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.90113342E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     5.04343592E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     5.04343592E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.83267559E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.63083507E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.78223854E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.49396319E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.74429070E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.69653846E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.40061230E-05    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.02959446E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     4.99962849E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     4.99962849E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
#
#         PDG            Width
DECAY   1000025     1.03265658E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.13551778E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.53998967E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.51259519E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.51259519E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.60204406E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.09149650E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     2.37200713E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     2.37200713E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     4.49394219E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     4.49394219E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.85841307E-09    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.85841307E-09    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.79759788E-10    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.79759788E-10    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     2.37200713E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     2.37200713E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     4.49394219E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     4.49394219E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.85841307E-09    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.85841307E-09    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.79759788E-10    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.79759788E-10    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.37697469E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37697469E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.56453610E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.56453610E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.83041525E-06    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     2.83041525E-06    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.68196020E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.68196020E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     1.10912894E-09    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.10912894E-09    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.68196020E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.68196020E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.10912894E-09    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.10912894E-09    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.15902910E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.15902910E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.31352709E-09    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.31352709E-09    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.31352709E-09    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.31352709E-09    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.00984816E-09    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.00984816E-09    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.04467529E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.03619515E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.07432375E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.44862323E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.44862323E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12447522E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.87536211E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.88304805E-09    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.88304805E-09    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.46832294E-10    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.46832294E-10    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.37084511E-09    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.37084511E-09    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.77557335E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.77557335E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.88304805E-09    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.88304805E-09    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.46832294E-10    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.46832294E-10    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.37084511E-09    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.37084511E-09    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.77557335E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.77557335E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25830309E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25830309E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.47896271E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.47896271E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.01118050E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.01118050E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.98715408E-10    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.98715408E-10    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.09957396E-09    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.09957396E-09    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.98715408E-10    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.98715408E-10    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.09957396E-09    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.09957396E-09    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.90337824E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.90337824E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.12108457E-09    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.12108457E-09    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.12108457E-09    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.12108457E-09    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     8.62576792E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     8.62576792E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.92442747E-03   # h decays
#          BR         NDA      ID1       ID2
     5.29752486E-01    2           5        -5   # BR(h -> b       bb     )
     6.72526571E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.38066731E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.03619973E-04    2           3        -3   # BR(h -> s       sb     )
     2.18710438E-02    2           4        -4   # BR(h -> c       cb     )
     7.22598012E-02    2          21        21   # BR(h -> g       g      )
     2.58186263E-03    2          22        22   # BR(h -> gam     gam    )
     1.92122375E-03    2          22        23   # BR(h -> Z       gam    )
     2.68899925E-01    2          24       -24   # BR(h -> W+      W-     )
     3.47193141E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.63722161E+01   # H decays
#          BR         NDA      ID1       ID2
     9.18442072E-01    2           5        -5   # BR(H -> b       bb     )
     5.36286678E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.89618154E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.32850215E-04    2           3        -3   # BR(H -> s       sb     )
     6.53041274E-08    2           4        -4   # BR(H -> c       cb     )
     6.51451178E-03    2           6        -6   # BR(H -> t       tb     )
     1.66764428E-06    2          21        21   # BR(H -> g       g      )
     2.56320810E-08    2          22        22   # BR(H -> gam     gam    )
     2.65802599E-09    2          23        22   # BR(H -> Z       gam    )
     9.41033337E-07    2          24       -24   # BR(H -> W+      W-     )
     4.69936522E-07    2          23        23   # BR(H -> Z       Z      )
     4.80544509E-06    2          25        25   # BR(H -> h       h      )
    -3.62045338E-30    2          36        36   # BR(H -> A       A      )
     1.35683753E-17    2          23        36   # BR(H -> Z       A      )
     7.01218283E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.32922227E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.50635277E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.16969403E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.96770610E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.12638802E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.54627623E+01   # A decays
#          BR         NDA      ID1       ID2
     9.36795141E-01    2           5        -5   # BR(A -> b       bb     )
     5.46978291E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.93398182E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.37533122E-04    2           3        -3   # BR(A -> s       sb     )
     6.70284801E-08    2           4        -4   # BR(A -> c       cb     )
     6.68284104E-03    2           6        -6   # BR(A -> t       tb     )
     1.96803231E-05    2          21        21   # BR(A -> g       g      )
     5.00237792E-08    2          22        22   # BR(A -> gam     gam    )
     1.93827987E-08    2          23        22   # BR(A -> Z       gam    )
     9.55931988E-07    2          23        25   # BR(A -> Z       h      )
     7.42028732E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.43108557E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.71020865E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.25124843E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.91065329E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.49230066E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.06580283E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.79114432E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.55071263E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.05260853E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.16555422E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.37403751E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.86544514E-07    2          24        25   # BR(H+ -> W+      h      )
     4.65501869E-14    2          24        36   # BR(H+ -> W+      A      )
     4.13741350E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.71048795E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.61554585E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
