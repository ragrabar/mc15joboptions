#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.84163507E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.61900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04176112E+01   # W+
        25     1.24926719E+02   # h
        35     4.00000533E+03   # H
        36     3.99999561E+03   # A
        37     4.00097907E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.00998057E+03   # ~d_L
   2000001     4.00974138E+03   # ~d_R
   1000002     4.00931539E+03   # ~u_L
   2000002     4.01178599E+03   # ~u_R
   1000003     4.00998057E+03   # ~s_L
   2000003     4.00974138E+03   # ~s_R
   1000004     4.00931539E+03   # ~c_L
   2000004     4.01178599E+03   # ~c_R
   1000005     3.39743489E+02   # ~b_1
   2000005     4.01642423E+03   # ~b_2
   1000006     3.28186309E+02   # ~t_1
   2000006     2.32371499E+03   # ~t_2
   1000011     4.00185039E+03   # ~e_L
   2000011     4.00213657E+03   # ~e_R
   1000012     4.00072697E+03   # ~nu_eL
   1000013     4.00185039E+03   # ~mu_L
   2000013     4.00213657E+03   # ~mu_R
   1000014     4.00072697E+03   # ~nu_muL
   1000015     4.00426787E+03   # ~tau_1
   2000015     4.00817131E+03   # ~tau_2
   1000016     4.00355078E+03   # ~nu_tauL
   1000021     1.95675629E+03   # ~g
   1000022     2.03405975E+02   # ~chi_10
   1000023    -2.71277533E+02   # ~chi_20
   1000025     2.79783222E+02   # ~chi_30
   1000035     2.05922619E+03   # ~chi_40
   1000024     2.68392189E+02   # ~chi_1+
   1000037     2.05939017E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.01318364E-01   # N_11
  1  2    -1.03144876E-02   # N_12
  1  3    -3.48924648E-01   # N_13
  1  4    -2.56457419E-01   # N_14
  2  1    -6.90109459E-02   # N_21
  2  2     2.55576880E-02   # N_22
  2  3    -7.00646983E-01   # N_23
  2  4     7.09702825E-01   # N_24
  3  1     4.27623401E-01   # N_31
  3  2     2.81086465E-02   # N_32
  3  3     6.22362620E-01   # N_33
  3  4     6.54990764E-01   # N_34
  4  1    -9.60262105E-04   # N_41
  4  2     9.99224860E-01   # N_42
  4  3    -3.18830425E-03   # N_43
  4  4    -3.92248989E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.51420734E-03   # U_11
  1  2     9.99989811E-01   # U_12
  2  1    -9.99989811E-01   # U_21
  2  2     4.51420734E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54851639E-02   # V_11
  1  2    -9.98459512E-01   # V_12
  2  1    -9.98459512E-01   # V_21
  2  2    -5.54851639E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97857731E-01   # cos(theta_t)
  1  2    -6.54213167E-02   # sin(theta_t)
  2  1     6.54213167E-02   # -sin(theta_t)
  2  2     9.97857731E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999672E-01   # cos(theta_b)
  1  2    -8.09938203E-04   # sin(theta_b)
  2  1     8.09938203E-04   # -sin(theta_b)
  2  2     9.99999672E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05017840E-01   # cos(theta_tau)
  1  2     7.09189569E-01   # sin(theta_tau)
  2  1    -7.09189569E-01   # -sin(theta_tau)
  2  2    -7.05017840E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00267090E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.41635066E+02  # DRbar Higgs Parameters
         1    -2.61900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44811392E+02   # higgs               
         4     1.63560837E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.41635066E+02  # The gauge couplings
     1     3.60520109E-01   # gprime(Q) DRbar
     2     6.35992385E-01   # g(Q) DRbar
     3     1.04015034E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.41635066E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     4.82631236E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.41635066E+02  # The trilinear couplings
  1  1     1.77100489E-07   # A_d(Q) DRbar
  2  2     1.77117842E-07   # A_s(Q) DRbar
  3  3     3.17151344E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.41635066E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     3.89776279E-08   # A_mu(Q) DRbar
  3  3     3.93635296E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.41635066E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74359094E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.41635066E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84570454E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.41635066E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03164532E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.41635066E+02  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57931719E+07   # M^2_Hd              
        22    -1.67416225E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40327952E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.62200891E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46862901E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46862901E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53137099E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53137099E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.78356727E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.39020791E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.57607769E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18964232E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.05206386E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.05622663E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.35413444E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.15582676E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.02931180E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.13042195E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.90005223E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.89679803E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.06104655E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.52981396E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.39015983E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.08002622E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66326247E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51144873E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.74485904E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61367445E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.74880206E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.53122549E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.35628859E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15641100E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.68228444E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.99997571E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.68786472E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.03668874E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77753330E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22566437E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.35730413E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.02687032E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.75459462E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33440096E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.49700090E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53684497E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60783086E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.46372771E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.60702554E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.00044177E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.77833324E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45097575E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77773382E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83676801E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29053001E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.40383975E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.75943117E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.53118572E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.52890161E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53906936E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53987236E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16456994E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.80160662E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.61010391E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.24339480E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85676173E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77753330E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22566437E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.35730413E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.02687032E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.75459462E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33440096E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.49700090E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53684497E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60783086E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.46372771E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.60702554E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.00044177E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.77833324E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45097575E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77773382E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83676801E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29053001E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.40383975E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.75943117E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.53118572E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.52890161E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53906936E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53987236E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16456994E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.80160662E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.61010391E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.24339480E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85676173E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12701703E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.28376300E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.38388168E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.73123345E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77601291E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.07975353E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56595438E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05726056E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.13076864E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.74742062E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.82175214E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.01510216E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12701703E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.28376300E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.38388168E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.73123345E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77601291E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.07975353E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56595438E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05726056E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.13076864E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.74742062E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.82175214E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.01510216E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08506106E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.93147565E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28876008E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     8.96620112E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39658203E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46270228E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80017597E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08700036E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.05473796E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40851468E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.51543150E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42382297E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10373126E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85478298E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12810421E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.39086664E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13286748E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.34006079E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77919617E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13998451E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54320259E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12810421E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.39086664E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13286748E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.34006079E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77919617E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13998451E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54320259E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45877076E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25879360E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93034703E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.11786864E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51782999E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.70388417E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02189766E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.23219961E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33456328E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33456328E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11153004E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11153004E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10781336E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.92008248E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.88279890E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.79357517E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.91898951E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.45847854E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.79478873E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.10725933E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.76937388E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.68159652E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.32677385E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17369297E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52158103E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17369297E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52158103E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.47175418E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48415080E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48415080E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46219500E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96435262E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96435262E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96435213E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.88352312E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.88352312E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.88352312E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.88352312E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.61175621E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.61175621E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.61175621E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.61175621E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.34247434E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.34247434E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.55058865E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.99060158E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.09069600E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.15896543E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50027062E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15896543E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50027062E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.48095926E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.41749650E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.41749650E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.40515879E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.87461759E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.87461759E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.87460791E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.14258926E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.48133399E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.14258926E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.48133399E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.39247808E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.39247808E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.55843527E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.77959999E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.77959999E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.77960011E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.65978987E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.65978987E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.65978987E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.65978987E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.53258857E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.53258857E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.53258857E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.53258857E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.91573059E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.91573059E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.91907278E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.13961693E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.45616832E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.14782620E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.77962991E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.77962991E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.38773653E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.41510129E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.00113721E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.91119235E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.91119235E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.92719624E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.92719624E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95885410E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01813851E-01    2           5        -5   # BR(h -> b       bb     )
     6.54921527E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31844491E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.92145735E-04    2           3        -3   # BR(h -> s       sb     )
     2.13746081E-02    2           4        -4   # BR(h -> c       cb     )
     7.12475974E-02    2          21        21   # BR(h -> g       g      )
     2.35950759E-03    2          22        22   # BR(h -> gam     gam    )
     1.56907330E-03    2          22        23   # BR(h -> Z       gam    )
     2.09222739E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61964798E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49774192E+01   # H decays
#          BR         NDA      ID1       ID2
     3.52900655E-01    2           5        -5   # BR(H -> b       bb     )
     6.03083755E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13235808E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52221210E-04    2           3        -3   # BR(H -> s       sb     )
     7.07430380E-08    2           4        -4   # BR(H -> c       cb     )
     7.08992525E-03    2           6        -6   # BR(H -> t       tb     )
     8.43789342E-07    2          21        21   # BR(H -> g       g      )
     2.68633826E-10    2          22        22   # BR(H -> gam     gam    )
     1.82308934E-09    2          23        22   # BR(H -> Z       gam    )
     1.77845683E-06    2          24       -24   # BR(H -> W+      W-     )
     8.88612371E-07    2          23        23   # BR(H -> Z       Z      )
     6.77521152E-06    2          25        25   # BR(H -> h       h      )
     2.32789286E-24    2          36        36   # BR(H -> A       A      )
     3.18600165E-20    2          23        36   # BR(H -> Z       A      )
     1.86068226E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61969027E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61969027E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.92261416E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03368631E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.03189105E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83914164E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.88138806E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     1.97371083E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.26507298E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.93133905E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.33811526E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.46607741E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.89948915E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.89948915E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.45724170E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49759438E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52935823E-01    2           5        -5   # BR(A -> b       bb     )
     6.03103072E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13242471E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52270228E-04    2           3        -3   # BR(A -> s       sb     )
     7.12022642E-08    2           4        -4   # BR(A -> c       cb     )
     7.10370359E-03    2           6        -6   # BR(A -> t       tb     )
     1.45963189E-05    2          21        21   # BR(A -> g       g      )
     5.33846635E-08    2          22        22   # BR(A -> gam     gam    )
     1.60464327E-08    2          23        22   # BR(A -> Z       gam    )
     1.77481990E-06    2          23        25   # BR(A -> Z       h      )
     1.86430637E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61975340E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61975340E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.67690911E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.28999242E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.52460101E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.51208667E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.51694469E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.98670149E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.01787263E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.01406661E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.24264315E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.93315928E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.93315928E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50247487E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.65639353E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02716294E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13105715E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62008877E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20728559E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48377469E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60251603E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.77517469E-06    2          24        25   # BR(H+ -> W+      h      )
     2.20013362E-14    2          24        36   # BR(H+ -> W+      A      )
     6.82461890E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.77479776E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.93118458E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62038160E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     3.96218070E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59411577E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.25887480E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     7.79375049E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.81943267E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
