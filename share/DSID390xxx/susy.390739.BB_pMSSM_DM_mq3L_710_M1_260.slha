#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12001919E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.05900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04175664E+01   # W+
        25     1.25657788E+02   # h
        35     4.00000770E+03   # H
        36     3.99999666E+03   # A
        37     4.00102753E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02301689E+03   # ~d_L
   2000001     4.02023835E+03   # ~d_R
   1000002     4.02236276E+03   # ~u_L
   2000002     4.02125397E+03   # ~u_R
   1000003     4.02301689E+03   # ~s_L
   2000003     4.02023835E+03   # ~s_R
   1000004     4.02236276E+03   # ~c_L
   2000004     4.02125397E+03   # ~c_R
   1000005     7.72051997E+02   # ~b_1
   2000005     4.02380878E+03   # ~b_2
   1000006     7.56319320E+02   # ~t_1
   2000006     2.07088766E+03   # ~t_2
   1000011     4.00406437E+03   # ~e_L
   2000011     4.00319135E+03   # ~e_R
   1000012     4.00294938E+03   # ~nu_eL
   1000013     4.00406437E+03   # ~mu_L
   2000013     4.00319135E+03   # ~mu_R
   1000014     4.00294938E+03   # ~nu_muL
   1000015     4.00474866E+03   # ~tau_1
   2000015     4.00757609E+03   # ~tau_2
   1000016     4.00464170E+03   # ~nu_tauL
   1000021     1.97470182E+03   # ~g
   1000022     2.51383537E+02   # ~chi_10
   1000023    -3.16475351E+02   # ~chi_20
   1000025     3.26277358E+02   # ~chi_30
   1000035     2.05342136E+03   # ~chi_40
   1000024     3.13788343E+02   # ~chi_1+
   1000037     2.05358600E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.88642983E-01   # N_11
  1  2    -1.17496640E-02   # N_12
  1  3    -3.59971962E-01   # N_13
  1  4    -2.83893962E-01   # N_14
  2  1    -5.75849626E-02   # N_21
  2  2     2.49693107E-02   # N_22
  2  3    -7.02252016E-01   # N_23
  2  4     7.09156267E-01   # N_24
  3  1     4.54968841E-01   # N_31
  3  2     2.82787042E-02   # N_32
  3  3     6.14203348E-01   # N_33
  3  4     6.44172271E-01   # N_34
  4  1    -9.87587219E-04   # N_41
  4  2     9.99219092E-01   # N_42
  4  3    -4.06685146E-03   # N_43
  4  4    -3.92898407E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.75684060E-03   # U_11
  1  2     9.99983429E-01   # U_12
  2  1    -9.99983429E-01   # U_21
  2  2     5.75684060E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55761384E-02   # V_11
  1  2    -9.98454452E-01   # V_12
  2  1    -9.98454452E-01   # V_21
  2  2    -5.55761384E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94825139E-01   # cos(theta_t)
  1  2    -1.01601884E-01   # sin(theta_t)
  2  1     1.01601884E-01   # -sin(theta_t)
  2  2     9.94825139E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999534E-01   # cos(theta_b)
  1  2    -9.65401358E-04   # sin(theta_b)
  2  1     9.65401358E-04   # -sin(theta_b)
  2  2     9.99999534E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05599235E-01   # cos(theta_tau)
  1  2     7.08611120E-01   # sin(theta_tau)
  2  1    -7.08611120E-01   # -sin(theta_tau)
  2  2    -7.05599235E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00280847E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20019186E+03  # DRbar Higgs Parameters
         1    -3.05900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43836755E+02   # higgs               
         4     1.61601450E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20019186E+03  # The gauge couplings
     1     3.61683906E-01   # gprime(Q) DRbar
     2     6.36232887E-01   # g(Q) DRbar
     3     1.03202462E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20019186E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14496550E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20019186E+03  # The trilinear couplings
  1  1     4.20539776E-07   # A_d(Q) DRbar
  2  2     4.20579345E-07   # A_s(Q) DRbar
  3  3     7.52872762E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20019186E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.20779001E-08   # A_mu(Q) DRbar
  3  3     9.30165090E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20019186E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69213499E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20019186E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83907003E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20019186E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03457745E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20019186E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57917874E+07   # M^2_Hd              
        22    -8.67733741E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40434298E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.05180171E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43892800E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43892800E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56107200E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56107200E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.47210278E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     8.75004504E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.25715394E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.76151825E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10632331E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21602480E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.96593620E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18041011E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.00396158E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.33003881E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.98305354E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.58272104E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.10772459E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.29855006E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.92882227E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.63851198E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.23205588E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.95351583E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.81759163E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66777313E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52280839E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78922368E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63996584E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.03266915E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60874272E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.19052796E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13929435E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.01405753E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.33934058E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.25186799E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.37899161E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78661259E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.12679088E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.70541602E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.13043164E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79238002E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33229954E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57260775E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52542616E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61106011E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.35978032E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.82278783E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.13698772E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98179188E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44850011E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78681364E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81293443E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11116802E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.89463378E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79730955E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.72177700E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60462736E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52761394E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54354368E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13732356E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.75504463E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.96602008E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77581399E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85613186E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78661259E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.12679088E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.70541602E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.13043164E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79238002E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33229954E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57260775E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52542616E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61106011E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.35978032E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.82278783E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.13698772E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98179188E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44850011E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78681364E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81293443E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11116802E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.89463378E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79730955E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.72177700E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60462736E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52761394E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54354368E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13732356E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.75504463E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.96602008E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77581399E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85613186E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14518933E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.23821523E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.05341770E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.16404966E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77687358E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.35609456E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56786528E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06490081E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.90584707E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.30444318E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.06110315E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.34413669E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14518933E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.23821523E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.05341770E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.16404966E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77687358E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.35609456E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56786528E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06490081E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.90584707E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.30444318E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.06110315E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.34413669E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09770705E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.84465051E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35489543E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.68283363E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39910537E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.47139981E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80533123E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09526797E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.96305347E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24835298E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.62409670E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42507398E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05562970E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85737759E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14625374E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.35811978E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68461307E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.68253648E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78029924E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12587509E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54522245E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14625374E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.35811978E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68461307E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.68253648E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78029924E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12587509E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54522245E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47492398E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23019224E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52593764E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.42986726E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51991340E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65754798E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02589346E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.89575320E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33470108E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33470108E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11157898E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11157898E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10743986E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.84739195E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.71381073E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.58084161E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.87583402E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.34904265E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.71609362E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.74617535E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.78046935E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.53572517E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.56379669E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17585768E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52508215E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17585768E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52508215E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45022705E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49605232E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49605232E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47296298E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98937055E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98937055E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98937030E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.14861963E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.14861963E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.14861963E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.14861963E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.16207334E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.16207334E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.16207334E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.16207334E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.87887762E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.87887762E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.04317962E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.64220408E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.21859673E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11662627E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.44519821E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11662627E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.44519821E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.39848369E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.28834199E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.28834199E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.27911207E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.63428487E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.63428487E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.63427686E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.94071249E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.11130120E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.94071249E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.11130120E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     6.49137702E-07    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.17183199E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.17183199E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.56698110E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.34231837E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.34231837E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.34231839E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.46802019E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.46802019E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.46802019E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.46802019E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.48932749E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.48932749E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.48932749E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.48932749E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.34948212E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.34948212E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.84625182E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.26422142E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.15770284E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.29113958E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.70476887E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.70476887E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.24390112E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.68690031E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55148379E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.81980055E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81980055E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.82782062E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.82782062E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04464292E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91312845E-01    2           5        -5   # BR(h -> b       bb     )
     6.44826147E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28267485E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84002233E-04    2           3        -3   # BR(h -> s       sb     )
     2.10197250E-02    2           4        -4   # BR(h -> c       cb     )
     6.86759880E-02    2          21        21   # BR(h -> g       g      )
     2.38215708E-03    2          22        22   # BR(h -> gam     gam    )
     1.63883881E-03    2          22        23   # BR(h -> Z       gam    )
     2.21695046E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80805153E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50003710E+01   # H decays
#          BR         NDA      ID1       ID2
     3.53657425E-01    2           5        -5   # BR(H -> b       bb     )
     6.02832360E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13146921E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52116051E-04    2           3        -3   # BR(H -> s       sb     )
     7.07174392E-08    2           4        -4   # BR(H -> c       cb     )
     7.08735977E-03    2           6        -6   # BR(H -> t       tb     )
     8.19967293E-07    2          21        21   # BR(H -> g       g      )
     4.64644431E-10    2          22        22   # BR(H -> gam     gam    )
     1.82134919E-09    2          23        22   # BR(H -> Z       gam    )
     1.85003853E-06    2          24       -24   # BR(H -> W+      W-     )
     9.24378422E-07    2          23        23   # BR(H -> Z       Z      )
     7.19272735E-06    2          25        25   # BR(H -> h       h      )
     6.68262856E-25    2          36        36   # BR(H -> A       A      )
     4.88614719E-20    2          23        36   # BR(H -> Z       A      )
     1.85127520E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61309456E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61309456E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.99506657E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.19206032E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.14179375E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.81025836E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.63698579E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.06745865E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.77566994E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.06425999E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.04839140E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.09279685E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.73961566E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.73961566E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.37521256E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49964256E+01   # A decays
#          BR         NDA      ID1       ID2
     3.53708456E-01    2           5        -5   # BR(A -> b       bb     )
     6.02878623E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13163111E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52176336E-04    2           3        -3   # BR(A -> s       sb     )
     7.11757634E-08    2           4        -4   # BR(A -> c       cb     )
     7.10105966E-03    2           6        -6   # BR(A -> t       tb     )
     1.45908814E-05    2          21        21   # BR(A -> g       g      )
     5.66768885E-08    2          22        22   # BR(A -> gam     gam    )
     1.60403313E-08    2          23        22   # BR(A -> Z       gam    )
     1.84627270E-06    2          23        25   # BR(A -> Z       h      )
     1.85925238E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61320200E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61320200E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.73110306E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02870519E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.50811541E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.44891470E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.33021171E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.12748147E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.08025498E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.89077124E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.15852655E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.85579555E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.85579555E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50646164E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.67148847E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02287211E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12954002E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62974952E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20642427E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48200269E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.61189382E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.84605812E-06    2          24        25   # BR(H+ -> W+      h      )
     2.78209349E-14    2          24        36   # BR(H+ -> W+      A      )
     6.56305159E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.84590188E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.13032392E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61331874E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.19605986E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59437644E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22127477E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.61534738E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.58032345E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
