#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.93807152E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     2.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04009835E+01   # W+
        25     1.27320649E+02   # h
        35     3.00021410E+03   # H
        36     3.00000054E+03   # A
        37     3.00112333E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00819611E+03   # ~d_L
   2000001     3.00230637E+03   # ~d_R
   1000002     3.00726452E+03   # ~u_L
   2000002     3.00732889E+03   # ~u_R
   1000003     3.00819611E+03   # ~s_L
   2000003     3.00230637E+03   # ~s_R
   1000004     3.00726452E+03   # ~c_L
   2000004     3.00732889E+03   # ~c_R
   1000005     3.07759236E+02   # ~b_1
   2000005     3.00732080E+03   # ~b_2
   1000006     2.99344647E+02   # ~t_1
   2000006     3.01567466E+03   # ~t_2
   1000011     3.00832384E+03   # ~e_L
   2000011     2.99811319E+03   # ~e_R
   1000012     3.00692093E+03   # ~nu_eL
   1000013     3.00832384E+03   # ~mu_L
   2000013     2.99811319E+03   # ~mu_R
   1000014     3.00692093E+03   # ~nu_muL
   1000015     2.98647822E+03   # ~tau_1
   2000015     3.02319922E+03   # ~tau_2
   1000016     3.00801331E+03   # ~nu_tauL
   1000021     2.30905660E+03   # ~g
   1000022     5.09938550E+01   # ~chi_10
   1000023     1.10119803E+02   # ~chi_20
   1000025    -3.01561298E+03   # ~chi_30
   1000035     3.01580568E+03   # ~chi_40
   1000024     1.10269759E+02   # ~chi_1+
   1000037     3.01664957E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890733E-01   # N_11
  1  2     1.91809034E-03   # N_12
  1  3    -1.46493603E-02   # N_13
  1  4     4.88944180E-04   # N_14
  2  1    -1.53439435E-03   # N_21
  2  2     9.99656915E-01   # N_22
  2  3     2.61439634E-02   # N_23
  2  4    -4.36139320E-04   # N_24
  3  1     1.00442275E-02   # N_31
  3  2    -1.81607372E-02   # N_32
  3  3     7.06791085E-01   # N_33
  3  4     7.07117857E-01   # N_34
  4  1    -1.07369000E-02   # N_41
  4  2     1.87765801E-02   # N_42
  4  3    -7.06787275E-01   # N_43
  4  4     7.07095402E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99317904E-01   # U_11
  1  2     3.69286770E-02   # U_12
  2  1    -3.69286770E-02   # U_21
  2  2     9.99317904E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999809E-01   # V_11
  1  2     6.17316199E-04   # V_12
  2  1     6.17316199E-04   # V_21
  2  2    -9.99999809E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98665186E-01   # cos(theta_t)
  1  2    -5.16511982E-02   # sin(theta_t)
  2  1     5.16511982E-02   # -sin(theta_t)
  2  2     9.98665186E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99701804E-01   # cos(theta_b)
  1  2    -2.44193177E-02   # sin(theta_b)
  2  1     2.44193177E-02   # -sin(theta_b)
  2  2     9.99701804E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06847892E-01   # cos(theta_tau)
  1  2     7.07365576E-01   # sin(theta_tau)
  2  1    -7.07365576E-01   # -sin(theta_tau)
  2  2    -7.06847892E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00311624E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.38071520E+02  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.45088140E+02   # higgs               
         4     1.26437191E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.38071520E+02  # The gauge couplings
     1     3.60547006E-01   # gprime(Q) DRbar
     2     6.40852739E-01   # g(Q) DRbar
     3     1.03680436E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.38071520E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.41964173E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.38071520E+02  # The trilinear couplings
  1  1     2.66010460E-07   # A_d(Q) DRbar
  2  2     2.66042921E-07   # A_s(Q) DRbar
  3  3     5.60869910E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.38071520E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.34589853E-07   # A_mu(Q) DRbar
  3  3     1.36518213E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.38071520E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.63130980E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.38071520E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.19258971E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.38071520E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03499053E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.38071520E+02  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -1.01137694E+05   # M^2_Hd              
        22    -9.24934049E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     2.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42554776E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.75481963E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48053985E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48053985E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51946015E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51946015E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.23059668E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.79217715E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.05332030E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.86875793E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.28401328E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.09166488E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.13083811E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.29220353E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.83208342E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72608523E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65691953E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.26932229E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.68850967E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.67469824E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.13988637E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.59264381E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     6.00472919E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.72864922E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -5.51822587E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.13311043E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.98265762E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.17182854E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.18647260E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.74082369E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12705307E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.87503108E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63253341E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.26916922E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.03954706E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.24869477E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54509261E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.63272917E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45490376E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.13217763E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.63723932E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63376340E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.26341225E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.04645196E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.71565259E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40926738E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.03667435E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55907223E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12705307E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.87503108E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63253341E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.26916922E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.03954706E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.24869477E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54509261E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.63272917E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45490376E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.13217763E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.63723932E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63376340E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.26341225E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.04645196E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.71565259E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40926738E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.03667435E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55907223E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06206516E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.63415611E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01010234E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.02648204E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54948275E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997650E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.34988500E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06206516E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.63415611E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01010234E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.02648204E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54948275E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997650E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.34988500E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.83470823E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42517120E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19040608E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38442271E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.77580532E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50514108E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16352737E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.45413438E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.51965809E-07    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.33129804E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.04543960E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06243200E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.49835543E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01882909E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03133537E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06243200E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.49835543E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01882909E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03133537E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06425301E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.49755271E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01857859E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03166614E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     6.97257073E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39966642E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.76604548E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39966642E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.76604548E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06843934E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.88681859E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06843934E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.88681859E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06378801E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.04713025E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     6.09777510E-12    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.77937492E-08    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     6.09777510E-12    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.77937492E-08    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.56090346E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54119453E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.85981159E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.85981159E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.78844474E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.87667433E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.87667433E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.83446813E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.75187888E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.80017889E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.61399264E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.81274383E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.03286532E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.19498833E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.29316006E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.60471008E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.29316006E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.60471008E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.92519116E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.39556560E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.39556560E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.39227407E-04    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.39067527E-05    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.39067527E-05    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.40014280E-05    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     1.05251178E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.63796096E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.96158931E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.59594675E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.59594675E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.97133375E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.43576122E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.93539538E-09    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     1.93539538E-09    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     4.08585839E-10    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     4.08585839E-10    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     2.33870386E-09    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     2.33870386E-09    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     2.53396528E-10    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     2.53396528E-10    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     1.93539538E-09    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     1.93539538E-09    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     4.08585839E-10    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     4.08585839E-10    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     2.33870386E-09    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     2.33870386E-09    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     2.53396528E-10    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     2.53396528E-10    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.31372966E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31372966E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.68712328E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.68712328E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.78051870E-06    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     2.78051870E-06    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.13524277E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.13524277E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     1.00112016E-09    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.00112016E-09    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.13524277E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.13524277E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.00112016E-09    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.00112016E-09    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     2.13946571E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.13946571E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.09955140E-09    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.09955140E-09    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.09955140E-09    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.09955140E-09    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     8.40849182E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     8.40849182E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.04605396E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.48767452E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.78904241E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.63067788E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.63067788E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.04839026E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.46576011E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.15766793E-09    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.15766793E-09    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.90928178E-10    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.90928178E-10    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.65910287E-09    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.65910287E-09    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     2.99454059E-10    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     2.99454059E-10    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.15766793E-09    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.15766793E-09    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.90928178E-10    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.90928178E-10    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.65910287E-09    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.65910287E-09    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     2.99454059E-10    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     2.99454059E-10    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25572331E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25572331E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.73559939E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.73559939E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.15579351E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.15579351E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.34579641E-10    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.34579641E-10    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.17636107E-09    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.17636107E-09    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.34579641E-10    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.34579641E-10    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.17636107E-09    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.17636107E-09    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.92358275E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.92358275E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.25546695E-09    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.25546695E-09    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.25546695E-09    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.25546695E-09    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.66076503E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.66076503E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.97103511E-03   # h decays
#          BR         NDA      ID1       ID2
     5.29504803E-01    2           5        -5   # BR(h -> b       bb     )
     6.65572626E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.35604392E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.98283473E-04    2           3        -3   # BR(h -> s       sb     )
     2.16371895E-02    2           4        -4   # BR(h -> c       cb     )
     7.15969339E-02    2          21        21   # BR(h -> g       g      )
     2.56334190E-03    2          22        22   # BR(h -> gam     gam    )
     1.92583892E-03    2          22        23   # BR(h -> Z       gam    )
     2.70450699E-01    2          24       -24   # BR(h -> W+      W-     )
     3.50053188E-02    2          23        23   # BR(h -> Z       Z      )
     2.47249677E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.66549779E+01   # H decays
#          BR         NDA      ID1       ID2
     9.18856801E-01    2           5        -5   # BR(H -> b       bb     )
     5.33034200E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.88468156E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.31438142E-04    2           3        -3   # BR(H -> s       sb     )
     6.49144575E-08    2           4        -4   # BR(H -> c       cb     )
     6.47563912E-03    2           6        -6   # BR(H -> t       tb     )
     1.63739602E-06    2          21        21   # BR(H -> g       g      )
     2.51302722E-08    2          22        22   # BR(H -> gam     gam    )
     2.64048059E-09    2          23        22   # BR(H -> Z       gam    )
     1.00147806E-06    2          24       -24   # BR(H -> W+      W-     )
     5.00121675E-07    2          23        23   # BR(H -> Z       Z      )
     4.77703554E-06    2          25        25   # BR(H -> h       h      )
    -4.19186046E-30    2          36        36   # BR(H -> A       A      )
     1.03383547E-17    2          23        36   # BR(H -> Z       A      )
     7.35037140E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.30278527E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.68765634E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.20943894E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.95732091E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.24111298E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.57434496E+01   # A decays
#          BR         NDA      ID1       ID2
     9.37150364E-01    2           5        -5   # BR(A -> b       bb     )
     5.43621979E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.92211472E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.36075595E-04    2           3        -3   # BR(A -> s       sb     )
     6.66171867E-08    2           4        -4   # BR(A -> c       cb     )
     6.64183446E-03    2           6        -6   # BR(A -> t       tb     )
     1.95595621E-05    2          21        21   # BR(A -> g       g      )
     4.01577687E-08    2          22        22   # BR(A -> gam     gam    )
     1.92675011E-08    2          23        22   # BR(A -> Z       gam    )
     1.01725413E-06    2          23        25   # BR(A -> Z       h      )
     7.56232812E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.39488396E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.79392095E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.27040102E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.91707979E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.49240608E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.05919215E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.78880694E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.55138731E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.05123464E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.16272769E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.37461618E-01    2           6        -5   # BR(H+ -> t       bb     )
     9.48005190E-07    2          24        25   # BR(H+ -> W+      h      )
     4.77472612E-14    2          24        36   # BR(H+ -> W+      A      )
     4.22954742E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.95235491E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.61493344E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
