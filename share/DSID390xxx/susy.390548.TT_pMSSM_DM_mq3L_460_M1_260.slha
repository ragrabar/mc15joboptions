#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90934473E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.85485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04199858E+01   # W+
        25     1.25522083E+02   # h
        35     4.00000566E+03   # H
        36     3.99999563E+03   # A
        37     4.00099137E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01294866E+03   # ~d_L
   2000001     4.01225516E+03   # ~d_R
   1000002     4.01229292E+03   # ~u_L
   2000002     4.01348005E+03   # ~u_R
   1000003     4.01294866E+03   # ~s_L
   2000003     4.01225516E+03   # ~s_R
   1000004     4.01229292E+03   # ~c_L
   2000004     4.01348005E+03   # ~c_R
   1000005     4.77961086E+02   # ~b_1
   2000005     4.01830667E+03   # ~b_2
   1000006     4.54033019E+02   # ~t_1
   2000006     1.87510336E+03   # ~t_2
   1000011     4.00202980E+03   # ~e_L
   2000011     4.00294184E+03   # ~e_R
   1000012     4.00091554E+03   # ~nu_eL
   1000013     4.00202980E+03   # ~mu_L
   2000013     4.00294184E+03   # ~mu_R
   1000014     4.00091554E+03   # ~nu_muL
   1000015     4.00410913E+03   # ~tau_1
   2000015     4.00854977E+03   # ~tau_2
   1000016     4.00348521E+03   # ~nu_tauL
   1000021     1.95666080E+03   # ~g
   1000022     2.52547003E+02   # ~chi_10
   1000023    -3.16558324E+02   # ~chi_20
   1000025     3.26671607E+02   # ~chi_30
   1000035     2.05751547E+03   # ~chi_40
   1000024     3.14089464E+02   # ~chi_1+
   1000037     2.05767943E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91119432E-01   # N_11
  1  2    -1.16304394E-02   # N_12
  1  3    -3.56553100E-01   # N_13
  1  4    -2.80429632E-01   # N_14
  2  1    -5.74963531E-02   # N_21
  2  2     2.50120948E-02   # N_22
  2  3    -7.02262912E-01   # N_23
  2  4     7.09151160E-01   # N_24
  3  1     4.50110373E-01   # N_31
  3  2     2.84185071E-02   # N_32
  3  3     6.16181721E-01   # N_33
  3  4     6.45688104E-01   # N_34
  4  1    -9.90026789E-04   # N_41
  4  2     9.99215450E-01   # N_42
  4  3    -4.09598079E-03   # N_43
  4  4    -3.93792629E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.79809090E-03   # U_11
  1  2     9.99983191E-01   # U_12
  2  1    -9.99983191E-01   # U_21
  2  2     5.79809090E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57028049E-02   # V_11
  1  2    -9.98447393E-01   # V_12
  2  1    -9.98447393E-01   # V_21
  2  2    -5.57028049E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94498426E-01   # cos(theta_t)
  1  2    -1.04751519E-01   # sin(theta_t)
  2  1     1.04751519E-01   # -sin(theta_t)
  2  2     9.94498426E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999538E-01   # cos(theta_b)
  1  2    -9.61249076E-04   # sin(theta_b)
  2  1     9.61249076E-04   # -sin(theta_b)
  2  2     9.99999538E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05415451E-01   # cos(theta_tau)
  1  2     7.08794076E-01   # sin(theta_tau)
  2  1    -7.08794076E-01   # -sin(theta_tau)
  2  2    -7.05415451E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00283344E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.09344735E+02  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44510183E+02   # higgs               
         4     1.62696934E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.09344735E+02  # The gauge couplings
     1     3.60769590E-01   # gprime(Q) DRbar
     2     6.35847718E-01   # g(Q) DRbar
     3     1.03838969E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.09344735E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.90476371E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.09344735E+02  # The trilinear couplings
  1  1     2.16795293E-07   # A_d(Q) DRbar
  2  2     2.16816232E-07   # A_s(Q) DRbar
  3  3     3.88033840E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.09344735E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.82505100E-08   # A_mu(Q) DRbar
  3  3     4.87363129E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.09344735E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75885355E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.09344735E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86425067E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.09344735E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03183032E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.09344735E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57734821E+07   # M^2_Hd              
        22    -1.01607118E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.85485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40253745E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.23860740E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44176018E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44176018E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55823982E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55823982E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.54132824E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.30370031E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.69629969E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21059421E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.49052372E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.07466157E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.13928504E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.12123503E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.59781665E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.27228585E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.57102003E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.89977437E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.96235989E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.14511727E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.89252284E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66195709E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51464929E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78288255E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63705121E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.01699211E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59530967E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18715746E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14213217E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.06090028E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.41388458E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.37396756E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.19216453E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77552034E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.13836731E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.72864782E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11416095E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77241142E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36784033E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53265141E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53142773E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60397500E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.37091184E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81179245E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.10950299E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.97130232E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45014643E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77571205E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81955293E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11382823E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.77934417E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77736663E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.81624335E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.56475668E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53364080E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53678211E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.14014557E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.72602006E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.89411236E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.74730771E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85657164E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77552034E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.13836731E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.72864782E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11416095E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77241142E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36784033E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53265141E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53142773E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60397500E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.37091184E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81179245E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.10950299E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.97130232E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45014643E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77571205E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81955293E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11382823E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.77934417E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77736663E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.81624335E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.56475668E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53364080E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53678211E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.14014557E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.72602006E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.89411236E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.74730771E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85657164E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12842158E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24539630E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.94255263E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.08875480E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77697757E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.41660550E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56811473E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05425935E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.94971572E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29439365E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01733498E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.35502073E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12842158E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24539630E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.94255263E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.08875480E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77697757E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.41660550E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56811473E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05425935E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.94971572E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29439365E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01733498E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.35502073E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08351515E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85677290E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.36238303E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.55765989E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39852884E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48497774E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80419619E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08342374E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.97765556E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25595536E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60459432E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42635622E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05828252E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85997011E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12951175E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36458516E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68655643E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61338715E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78037159E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.15140108E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54532495E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12951175E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36458516E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68655643E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61338715E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78037159E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.15140108E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54532495E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45870577E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23551186E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52703753E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36620869E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51967975E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.67527172E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02538998E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.60196872E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33474653E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33474653E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11159437E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11159437E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10731820E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.66647303E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.86326270E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.09221225E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.74678767E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.08321097E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.05259138E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.94777344E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.10974899E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.95241151E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.25136188E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.91350935E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17595702E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52474958E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17595702E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52474958E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45369418E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49270348E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49270348E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46913443E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98172168E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98172168E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98172129E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.58220275E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.58220275E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.58220275E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.58220275E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.27401498E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.27401498E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.27401498E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.27401498E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.83172351E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.83172351E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.92249772E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.27611237E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.40411545E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11353358E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.44076110E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11353358E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.44076110E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.39510584E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.27587316E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.27587316E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.26629305E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.60808181E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.60808181E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.60806972E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.90387159E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.35864521E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.90387159E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.35864521E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.75452282E-06    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.45672374E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.45672374E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.20589549E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.91137269E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.91137269E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.91137273E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.94200777E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.94200777E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.94200777E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.94200777E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.64732219E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.64732219E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.64732219E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.64732219E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.49478131E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.49478131E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.66552603E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.67068873E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.68159467E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.03364034E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.93689122E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.93689122E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.48060463E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.36025797E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.13003529E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89925638E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89925638E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.69390480E-06    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.69390480E-06    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.90600468E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90600468E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01913287E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93762130E-01    2           5        -5   # BR(h -> b       bb     )
     6.48222933E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29470536E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86655348E-04    2           3        -3   # BR(h -> s       sb     )
     2.11347489E-02    2           4        -4   # BR(h -> c       cb     )
     6.79740464E-02    2          21        21   # BR(h -> g       g      )
     2.39779185E-03    2          22        22   # BR(h -> gam     gam    )
     1.62976114E-03    2          22        23   # BR(h -> Z       gam    )
     2.19773964E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77891383E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.55646184E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59747132E-01    2           5        -5   # BR(H -> b       bb     )
     5.96710401E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10982345E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49555747E-04    2           3        -3   # BR(H -> s       sb     )
     6.99999850E-08    2           4        -4   # BR(H -> c       cb     )
     7.01545588E-03    2           6        -6   # BR(H -> t       tb     )
     5.13290450E-07    2          21        21   # BR(H -> g       g      )
     1.33617977E-09    2          22        22   # BR(H -> gam     gam    )
     1.80369186E-09    2          23        22   # BR(H -> Z       gam    )
     1.84439516E-06    2          24       -24   # BR(H -> W+      W-     )
     9.21558826E-07    2          23        23   # BR(H -> Z       Z      )
     7.07355178E-06    2          25        25   # BR(H -> h       h      )
     2.35243580E-24    2          36        36   # BR(H -> A       A      )
    -2.27156768E-21    2          23        36   # BR(H -> Z       A      )
     1.84090584E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59197189E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59197189E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.94583777E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.10691418E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.10822691E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.79957769E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.66949220E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     1.99924663E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.49041884E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.96263573E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.00351793E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.80347615E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.79656245E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     3.63260444E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.63260444E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43240963E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.55633117E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59781399E-01    2           5        -5   # BR(A -> b       bb     )
     5.96727585E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10988255E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49603443E-04    2           3        -3   # BR(A -> s       sb     )
     7.04495750E-08    2           4        -4   # BR(A -> c       cb     )
     7.02860933E-03    2           6        -6   # BR(A -> t       tb     )
     1.44420191E-05    2          21        21   # BR(A -> g       g      )
     5.62183848E-08    2          22        22   # BR(A -> gam     gam    )
     1.58833683E-08    2          23        22   # BR(A -> Z       gam    )
     1.84056552E-06    2          23        25   # BR(A -> Z       h      )
     1.84865295E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59200113E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59200113E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.68928675E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.01797389E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.23043659E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.43421124E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.35777468E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.06133935E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.04762582E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.78441151E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.12052229E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.79560542E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.79560542E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.56519395E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.77273794E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95925584E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10704687E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69454915E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19368283E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45578945E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67439212E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.83965028E-06    2          24        25   # BR(H+ -> W+      h      )
     2.31465904E-14    2          24        36   # BR(H+ -> W+      A      )
     6.53168284E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.81343376E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.06778803E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59152845E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.06008542E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57296421E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.21251291E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.98086394E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.42018137E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
