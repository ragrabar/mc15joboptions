#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13067978E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.35900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04168265E+01   # W+
        25     1.25482531E+02   # h
        35     4.00001224E+03   # H
        36     3.99999747E+03   # A
        37     4.00101685E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02603515E+03   # ~d_L
   2000001     4.02259514E+03   # ~d_R
   1000002     4.02538050E+03   # ~u_L
   2000002     4.02366115E+03   # ~u_R
   1000003     4.02603515E+03   # ~s_L
   2000003     4.02259514E+03   # ~s_R
   1000004     4.02538050E+03   # ~c_L
   2000004     4.02366115E+03   # ~c_R
   1000005     8.36990606E+02   # ~b_1
   2000005     4.02546254E+03   # ~b_2
   1000006     8.25880845E+02   # ~t_1
   2000006     2.27859954E+03   # ~t_2
   1000011     4.00473882E+03   # ~e_L
   2000011     4.00308481E+03   # ~e_R
   1000012     4.00362307E+03   # ~nu_eL
   1000013     4.00473882E+03   # ~mu_L
   2000013     4.00308481E+03   # ~mu_R
   1000014     4.00362307E+03   # ~nu_muL
   1000015     4.00411005E+03   # ~tau_1
   2000015     4.00795897E+03   # ~tau_2
   1000016     4.00503807E+03   # ~nu_tauL
   1000021     1.98156834E+03   # ~g
   1000022     3.97772267E+02   # ~chi_10
   1000023    -4.47452511E+02   # ~chi_20
   1000025     4.62195766E+02   # ~chi_30
   1000035     2.05235689E+03   # ~chi_40
   1000024     4.45190925E+02   # ~chi_1+
   1000037     2.05252139E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.24719812E-01   # N_11
  1  2    -1.70294732E-02   # N_12
  1  3    -4.22899645E-01   # N_13
  1  4    -3.75104145E-01   # N_14
  2  1    -3.85960977E-02   # N_21
  2  2     2.36018801E-02   # N_22
  2  3    -7.04464286E-01   # N_23
  2  4     7.08296098E-01   # N_24
  3  1     5.64221910E-01   # N_31
  3  2     2.84538758E-02   # N_32
  3  3     5.69947146E-01   # N_33
  3  4     5.96660929E-01   # N_34
  4  1    -1.09972706E-03   # N_41
  4  2     9.99171319E-01   # N_42
  4  3    -6.79791510E-03   # N_43
  4  4    -4.01155045E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.61969250E-03   # U_11
  1  2     9.99953730E-01   # U_12
  2  1    -9.99953730E-01   # U_21
  2  2     9.61969250E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.67417396E-02   # V_11
  1  2    -9.98388890E-01   # V_12
  2  1    -9.98388890E-01   # V_21
  2  2    -5.67417396E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96255606E-01   # cos(theta_t)
  1  2    -8.64567378E-02   # sin(theta_t)
  2  1     8.64567378E-02   # -sin(theta_t)
  2  2     9.96255606E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999036E-01   # cos(theta_b)
  1  2    -1.38852406E-03   # sin(theta_b)
  2  1     1.38852406E-03   # -sin(theta_b)
  2  2     9.99999036E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06112695E-01   # cos(theta_tau)
  1  2     7.08099472E-01   # sin(theta_tau)
  2  1    -7.08099472E-01   # -sin(theta_tau)
  2  2    -7.06112695E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00249874E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30679778E+03  # DRbar Higgs Parameters
         1    -4.35900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43729808E+02   # higgs               
         4     1.61965473E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30679778E+03  # The gauge couplings
     1     3.61886456E-01   # gprime(Q) DRbar
     2     6.35981445E-01   # g(Q) DRbar
     3     1.03009355E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30679778E+03  # The trilinear couplings
  1  1     1.38101742E-06   # A_u(Q) DRbar
  2  2     1.38103057E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30679778E+03  # The trilinear couplings
  1  1     5.09596616E-07   # A_d(Q) DRbar
  2  2     5.09643587E-07   # A_s(Q) DRbar
  3  3     9.10614563E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30679778E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11924517E-07   # A_mu(Q) DRbar
  3  3     1.13059981E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30679778E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65904261E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30679778E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85299180E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30679778E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03044770E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30679778E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57022184E+07   # M^2_Hd              
        22    -1.94601518E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40326554E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.72108867E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44948890E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44948890E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55051110E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55051110E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.81861358E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.61498683E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.20882139E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.03898234E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13720944E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.33510359E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.78834967E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14726414E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.91075239E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.42541969E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.27723262E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.30703954E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.30314898E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.39857571E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.55532775E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.02117052E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.14861285E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.34749304E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.47616879E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.52589399E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.76504442E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66985413E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54719196E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80329256E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59627781E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.58495588E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61914078E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.14574150E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13374360E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.07054805E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.81094422E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.72815190E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     9.13988778E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78908935E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.66320225E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.25735828E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.56033021E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79384651E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.48489236E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57561047E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52510835E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61164983E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71829096E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.10232964E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.72868004E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70854848E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45449230E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78929670E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.57536996E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.14332226E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.24797437E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79917005E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.57655871E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60829170E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52728016E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54478535E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.69536951E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.11266057E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.50748259E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.66651092E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85776012E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78908935E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.66320225E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.25735828E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.56033021E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79384651E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.48489236E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57561047E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52510835E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61164983E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71829096E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.10232964E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.72868004E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70854848E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45449230E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78929670E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.57536996E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.14332226E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.24797437E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79917005E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.57655871E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60829170E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52728016E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54478535E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.69536951E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.11266057E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.50748259E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.66651092E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85776012E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14021034E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02885866E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.34565399E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.10197793E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78164626E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.26309402E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57835752E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04038550E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.81681364E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.48509273E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.16832872E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.71835696E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14021034E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02885866E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.34565399E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.10197793E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78164626E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.26309402E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57835752E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04038550E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.81681364E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.48509273E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.16832872E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.71835696E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07677144E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45440265E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47470823E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.31786801E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40626362E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53838933E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82015596E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06922131E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.50033043E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97612371E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.08873819E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43856855E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.89871625E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88487883E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14126367E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18891607E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03815217E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.27397933E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78580507E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.22081856E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55529122E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14126367E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18891607E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03815217E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.27397933E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78580507E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.22081856E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55529122E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46411386E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.07850711E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.41747503E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.87709467E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52833801E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54137162E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04189078E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.81033395E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33585082E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33585082E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196539E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196539E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10436757E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.58565985E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.67254585E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.54218535E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.07658993E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.75146775E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.90000739E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.13648020E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.98814265E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.75541215E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.97200976E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18097956E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53197402E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18097956E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53197402E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40925667E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51331393E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51331393E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48021416E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02389095E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02389095E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02389081E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.37071590E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.37071590E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.37071590E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.37071590E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.90239266E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.90239266E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.90239266E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.90239266E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.80915359E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.80915359E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.89077316E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.19586226E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.76231814E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.60572382E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.64556640E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.60572382E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.64556640E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.43560081E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.04033950E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.04033950E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.04177676E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.14198671E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.14198671E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.14197955E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.09603751E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.42182474E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.09603751E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.42182474E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.67096284E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.26091400E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.26091400E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.00032696E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.51852382E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.51852382E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.51852388E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.60779322E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.60779322E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.60779322E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.60779322E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.53590372E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.53590372E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.53590372E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.53590372E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.40354405E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.40354405E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.58411730E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.56551065E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.70941309E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.71189906E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.89799304E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.89799304E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12430659E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.33783388E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.50695807E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.79699665E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79699665E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.81085596E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.81085596E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.00340288E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92063285E-01    2           5        -5   # BR(h -> b       bb     )
     6.50477378E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30268782E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88379053E-04    2           3        -3   # BR(h -> s       sb     )
     2.12124159E-02    2           4        -4   # BR(h -> c       cb     )
     6.93795244E-02    2          21        21   # BR(h -> g       g      )
     2.39024347E-03    2          22        22   # BR(h -> gam     gam    )
     1.63027265E-03    2          22        23   # BR(h -> Z       gam    )
     2.19795730E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77621434E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51076826E+01   # H decays
#          BR         NDA      ID1       ID2
     3.61611703E-01    2           5        -5   # BR(H -> b       bb     )
     6.01659330E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12732166E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51625432E-04    2           3        -3   # BR(H -> s       sb     )
     7.05710687E-08    2           4        -4   # BR(H -> c       cb     )
     7.07269049E-03    2           6        -6   # BR(H -> t       tb     )
     8.54418847E-07    2          21        21   # BR(H -> g       g      )
     1.66131565E-09    2          22        22   # BR(H -> gam     gam    )
     1.81969754E-09    2          23        22   # BR(H -> Z       gam    )
     1.68596079E-06    2          24       -24   # BR(H -> W+      W-     )
     8.42396352E-07    2          23        23   # BR(H -> Z       Z      )
     6.78666470E-06    2          25        25   # BR(H -> h       h      )
    -3.59274296E-25    2          36        36   # BR(H -> A       A      )
    -1.89923329E-20    2          23        36   # BR(H -> Z       A      )
     1.86463932E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56580305E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56580305E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.36654337E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.98364125E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.51979526E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.47033761E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.78909821E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.64533592E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.31078904E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.25749158E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.80802625E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.32391420E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.02331965E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.02331965E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39547224E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50995457E+01   # A decays
#          BR         NDA      ID1       ID2
     3.61690521E-01    2           5        -5   # BR(A -> b       bb     )
     6.01750444E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12764214E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51704426E-04    2           3        -3   # BR(A -> s       sb     )
     7.10425688E-08    2           4        -4   # BR(A -> c       cb     )
     7.08777111E-03    2           6        -6   # BR(A -> t       tb     )
     1.45635731E-05    2          21        21   # BR(A -> g       g      )
     6.51196084E-08    2          22        22   # BR(A -> gam     gam    )
     1.60082489E-08    2          23        22   # BR(A -> Z       gam    )
     1.68266584E-06    2          23        25   # BR(A -> Z       h      )
     1.89678258E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56585090E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56585090E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.06331473E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.41769707E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30201424E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.95578289E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.62569188E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.96071262E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.44978692E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.33291953E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.41071079E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.23947736E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.23947736E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53116676E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.82442189E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99595477E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12002271E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.72762685E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20103294E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47091098E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.70709641E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.67808159E-06    2          24        25   # BR(H+ -> W+      h      )
     2.61872541E-14    2          24        36   # BR(H+ -> W+      A      )
     5.40393696E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.74746338E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.01778239E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.56205587E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.59588873E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55331495E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.02028264E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.74761609E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.42081978E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
