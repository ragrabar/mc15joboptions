#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.89511431E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.50900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194612E+01   # W+
        25     1.25361346E+02   # h
        35     4.00000707E+03   # H
        36     3.99999596E+03   # A
        37     4.00097697E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01233550E+03   # ~d_L
   2000001     4.01172282E+03   # ~d_R
   1000002     4.01167704E+03   # ~u_L
   2000002     4.01316036E+03   # ~u_R
   1000003     4.01233550E+03   # ~s_L
   2000003     4.01172282E+03   # ~s_R
   1000004     4.01167704E+03   # ~c_L
   2000004     4.01316036E+03   # ~c_R
   1000005     4.34635109E+02   # ~b_1
   2000005     4.01795310E+03   # ~b_2
   1000006     4.15372959E+02   # ~t_1
   2000006     2.02369820E+03   # ~t_2
   1000011     4.00201396E+03   # ~e_L
   2000011     4.00269492E+03   # ~e_R
   1000012     4.00089723E+03   # ~nu_eL
   1000013     4.00201396E+03   # ~mu_L
   2000013     4.00269492E+03   # ~mu_R
   1000014     4.00089723E+03   # ~nu_muL
   1000015     4.00399519E+03   # ~tau_1
   2000015     4.00854565E+03   # ~tau_2
   1000016     4.00351545E+03   # ~nu_tauL
   1000021     1.95752462E+03   # ~g
   1000022     3.02096503E+02   # ~chi_10
   1000023    -3.60532150E+02   # ~chi_20
   1000025     3.72042797E+02   # ~chi_30
   1000035     2.05796580E+03   # ~chi_40
   1000024     3.58274559E+02   # ~chi_1+
   1000037     2.05812978E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.75947121E-01   # N_11
  1  2    -1.31528803E-02   # N_12
  1  3    -3.71641482E-01   # N_13
  1  4    -3.07288549E-01   # N_14
  2  1    -4.93692041E-02   # N_21
  2  2     2.45421669E-02   # N_22
  2  3    -7.03275684E-01   # N_23
  2  4     7.08776182E-01   # N_24
  3  1     4.79873184E-01   # N_31
  3  2     2.86679859E-02   # N_32
  3  3     6.06020516E-01   # N_33
  3  4     6.33749957E-01   # N_34
  4  1    -1.02496161E-03   # N_41
  4  2     9.99201096E-01   # N_42
  4  3    -5.00563331E-03   # N_43
  4  4    -3.96366336E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.08476803E-03   # U_11
  1  2     9.99974903E-01   # U_12
  2  1    -9.99974903E-01   # U_21
  2  2     7.08476803E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.60661935E-02   # V_11
  1  2    -9.98427054E-01   # V_12
  2  1    -9.98427054E-01   # V_21
  2  2    -5.60661935E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96094934E-01   # cos(theta_t)
  1  2    -8.82886315E-02   # sin(theta_t)
  2  1     8.82886315E-02   # -sin(theta_t)
  2  2     9.96094934E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999396E-01   # cos(theta_b)
  1  2    -1.09909037E-03   # sin(theta_b)
  2  1     1.09909037E-03   # -sin(theta_b)
  2  2     9.99999396E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05624422E-01   # cos(theta_tau)
  1  2     7.08586039E-01   # sin(theta_tau)
  2  1    -7.08586039E-01   # -sin(theta_tau)
  2  2    -7.05624422E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00260985E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  8.95114312E+02  # DRbar Higgs Parameters
         1    -3.50900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44615879E+02   # higgs               
         4     1.63426602E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  8.95114312E+02  # The gauge couplings
     1     3.60682068E-01   # gprime(Q) DRbar
     2     6.35706941E-01   # g(Q) DRbar
     3     1.03875196E+00   # g3(Q) DRbar
#
BLOCK AU Q=  8.95114312E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.66731291E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  8.95114312E+02  # The trilinear couplings
  1  1     2.08438101E-07   # A_d(Q) DRbar
  2  2     2.08458333E-07   # A_s(Q) DRbar
  3  3     3.72839677E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  8.95114312E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.64394807E-08   # A_mu(Q) DRbar
  3  3     4.69038564E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  8.95114312E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74970796E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  8.95114312E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86893656E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  8.95114312E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02968042E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  8.95114312E+02  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57440312E+07   # M^2_Hd              
        22    -1.55973230E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40193938E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.38444985E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45300355E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45300355E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54699645E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54699645E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.44211265E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18359925E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.10389176E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16704485E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.77761759E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.31255347E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.52141626E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.13140611E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.37942837E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.63412286E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.45659379E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.59197146E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.95143475E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66476556E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51104665E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77972432E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62169118E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.65845642E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58092444E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.71319795E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14418740E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.39002572E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.88322016E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.16605441E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.84805473E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77789436E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00971871E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.98845005E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.22536042E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76035417E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.40808286E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.50857823E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53514904E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60655467E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.20036157E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.32809818E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.25351484E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.17635977E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45328394E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77809047E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.76297393E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.87224584E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.31222462E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.76542102E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.63613357E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.54082927E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53735942E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53944832E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.09545667E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.46367604E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.26916052E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.27985480E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85741628E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77789436E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00971871E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.98845005E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.22536042E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76035417E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.40808286E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.50857823E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53514904E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60655467E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.20036157E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.32809818E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.25351484E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.17635977E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45328394E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77809047E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.76297393E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.87224584E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.31222462E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.76542102E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.63613357E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.54082927E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53735942E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53944832E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.09545667E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.46367604E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.26916052E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.27985480E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85741628E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12429501E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.19171124E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.09467295E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.58294964E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77832308E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.08665825E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57110110E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04551651E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.68338699E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.42882020E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.29231905E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.75866960E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12429501E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.19171124E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.09467295E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.58294964E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77832308E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.08665825E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57110110E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04551651E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.68338699E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.42882020E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.29231905E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.75866960E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07529681E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75848670E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41423130E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.03996877E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40068996E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50754168E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80867728E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07310052E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.86192128E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.14460954E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.72460922E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43029627E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00703116E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86800916E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12538128E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.32398054E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.39823124E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.00105517E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78193676E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18351823E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54815969E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12538128E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.32398054E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.39823124E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.00105517E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78193676E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18351823E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54815969E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45276399E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.19924617E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26650972E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.71834210E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52217943E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.63953774E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03012132E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.76172781E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33507892E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33507892E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11170745E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11170745E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10642725E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.74086761E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87668386E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.72829756E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.76878329E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.99508974E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.66538713E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.85855623E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.86693202E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.85820601E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.60596632E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.92166218E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17632983E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52523652E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17632983E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52523652E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44927586E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49385417E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49385417E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46739881E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98383828E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98383828E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98383792E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.42113120E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.42113120E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.42113120E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.42113120E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.73710863E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.73710863E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.73710863E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.73710863E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.06195292E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.06195292E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.16145708E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.08193169E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.76327917E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.88311400E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.27746337E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.88311400E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.27746337E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.27909253E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.89451826E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.89451826E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.88826722E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86387793E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86387793E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86386175E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.90124289E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.46525214E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.90124289E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.46525214E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.33038258E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.64766793E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.64766793E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.90206234E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.12873073E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.12873073E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.12873075E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.57740664E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.57740664E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.57740664E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.57740664E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.25797538E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.25797538E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.25797538E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.25797538E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.84717137E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.84717137E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.73988124E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.12953744E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.75288381E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.82856995E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.85106982E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.85106982E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.64176898E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.20003376E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.08027390E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90645597E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90645597E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.91649245E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.91649245E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.99303112E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95043051E-01    2           5        -5   # BR(h -> b       bb     )
     6.51564539E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30654173E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89288155E-04    2           3        -3   # BR(h -> s       sb     )
     2.12509782E-02    2           4        -4   # BR(h -> c       cb     )
     6.89869561E-02    2          21        21   # BR(h -> g       g      )
     2.39361785E-03    2          22        22   # BR(h -> gam     gam    )
     1.61721315E-03    2          22        23   # BR(h -> Z       gam    )
     2.17412500E-01    2          24       -24   # BR(h -> W+      W-     )
     2.74192874E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56422839E+01   # H decays
#          BR         NDA      ID1       ID2
     3.62284550E-01    2           5        -5   # BR(H -> b       bb     )
     5.95877856E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10687977E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49207551E-04    2           3        -3   # BR(H -> s       sb     )
     6.98960577E-08    2           4        -4   # BR(H -> c       cb     )
     7.00504022E-03    2           6        -6   # BR(H -> t       tb     )
     6.03719485E-07    2          21        21   # BR(H -> g       g      )
     1.41172133E-09    2          22        22   # BR(H -> gam     gam    )
     1.80252866E-09    2          23        22   # BR(H -> Z       gam    )
     1.72594112E-06    2          24       -24   # BR(H -> W+      W-     )
     8.62372803E-07    2          23        23   # BR(H -> Z       Z      )
     6.73958373E-06    2          25        25   # BR(H -> h       h      )
     3.31705423E-24    2          36        36   # BR(H -> A       A      )
    -1.35421406E-20    2          23        36   # BR(H -> Z       A      )
     1.84519437E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57549504E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57549504E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.04095718E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.70133758E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.21909248E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.73204584E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.39545814E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.11768458E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.01995798E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.03124073E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.65164725E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.01912231E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.27774386E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.27774386E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.45172845E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56419786E+01   # A decays
#          BR         NDA      ID1       ID2
     3.62312210E-01    2           5        -5   # BR(A -> b       bb     )
     5.95883978E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10689976E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49250570E-04    2           3        -3   # BR(A -> s       sb     )
     7.03499781E-08    2           4        -4   # BR(A -> c       cb     )
     7.01867276E-03    2           6        -6   # BR(A -> t       tb     )
     1.44216004E-05    2          21        21   # BR(A -> g       g      )
     5.92172753E-08    2          22        22   # BR(A -> gam     gam    )
     1.58594639E-08    2          23        22   # BR(A -> Z       gam    )
     1.72233799E-06    2          23        25   # BR(A -> Z       h      )
     1.85885208E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57545127E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57545127E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.76992603E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.47593677E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.02414251E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.32389026E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.14372957E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.24314422E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.12866442E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.59958299E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.95863721E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.44530851E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.44530851E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57901642E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.82391113E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94446990E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10181892E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.72729997E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19072164E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44969731E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.70618101E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.71962864E-06    2          24        25   # BR(H+ -> W+      h      )
     2.14316765E-14    2          24        36   # BR(H+ -> W+      A      )
     6.22409977E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.02755851E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.28741301E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57333527E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.35623544E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55944659E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.16007464E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.08715204E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.07001802E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
