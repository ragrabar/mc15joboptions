include("MC15JobOptions/MadGraphControl_monoHiggs_scalar.py")

evgenConfig.description = "Simplified Model of scalar mediator for\
MonoHiggs(h->bb) with mDM="+str(mDM)+"GeV and mScalar="+str(mS)+"GeV"
evgenConfig.process = "pp -> Scalar -> Scalar + Higgs -> bbXX"
evgenConfig.keywords = ["BSM", "BSMHiggs", "Higgs", "bbbar", "scalar", "simplifiedModel"]
evgenConfig.contact = ['Laser Kaplan <laser.seymour.kaplan@cern.ch>']

genSeq.Pythia8.Commands += [
    '25:oneChannel = on 1.0 100 5 -5 '
]
