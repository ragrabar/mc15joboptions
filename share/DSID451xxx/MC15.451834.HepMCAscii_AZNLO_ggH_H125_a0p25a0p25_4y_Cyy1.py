from TruthIO.TruthIOConf import HepMCReadFromFile 

genSeq += HepMCReadFromFile()
genSeq.HepMCReadFromFile.InputFile="events.hepmc"
evgenConfig.generators += ["HepMCAscii"]
evgenConfig.tune = "AZNLO CTEQ6L1"

evgenConfig.minevents = 10000
evgenConfig.inputfilecheck = "TXT"

evgenConfig.process = "ggH, H->aa->4y"
evgenConfig.description = "HepMC to EVNT transform: H->aa->4y mh=125 GeV"
evgenConfig.keywords += ["BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact  = ["olivera.vujinovic@cern.ch"]
