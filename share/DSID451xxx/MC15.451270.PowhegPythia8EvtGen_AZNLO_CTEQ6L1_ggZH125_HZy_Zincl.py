#--------------------------------------------------------------
# POWHEG+Pythia8 gg->ZH, H->Zy->(vv,l+l-)y, Z->all production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

PowhegConfig.runningscales = 1 #
PowhegConfig.decay_mode_Z = 'z > all'


PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']

#--------------------------------------------------------------
# Higgs->Zy->vvy at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 23',
                             '23:onMode = off',
                             '23:onIfAny = 11 12 13 14 15 16' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description   = "POWHEG+Pythia8 gg->H+Z->vvy+l+l- production"
evgenConfig.keywords	  = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs"]
evgenConfig.contact	  = [ 'hassnae.el.jarrari@cern.ch' ]
evgenConfig.inputconfcheck= 'ggZH125_MINLO_Zy_VpT_13TeV'
evgenConfig.process	  = "gg->ZH, H->Zy->(vv,l+l-)y, Z->all"
evgenConfig.minevents = 10000
