model  = 'dmA'
mR     = 200
mDM    = 10000
gSM    = 0.30
gDM    = 1.00
widthR = 7.156958
phminpt= 100.000000
filteff = 0.048500

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")
