evgenConfig.description = 'MadGraph5+Pythia8 for Ypp331  (Costantini-Corcella-Coriano-Frampton) M(Ypp)=750'
evgenConfig.contact = ['antonio.sidoti@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic']


#mass of the Y boson 
mYpp=750


# load configuration
#temporary
#include('./MadGraphControl_Ypp331.py')
# final 
include('MC15JobOptions/MadGraphControl_MGPy8EG_Ypp331.py')
