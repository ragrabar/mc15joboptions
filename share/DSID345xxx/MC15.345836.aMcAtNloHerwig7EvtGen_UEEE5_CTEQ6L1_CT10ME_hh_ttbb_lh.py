#--------------------------------------------------------------
# Herwig 7 showering setup                                    
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
""")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 345836):
    evgenConfig.description = "SM diHiggs production, decay to bbtautau (lephad), with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "bbbar", "tau"]

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 
evgenConfig.minevents   = 4000

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hTauTauFilter", PDGParent = [25], PDGChild = [15])
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2  
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had)                            
#--------------------------------------------------------------------------------------------------- 
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepTauPtFilter")
filtSeq.LepTauPtFilter.IncludeHadTaus = True
filtSeq.LepTauPtFilter.NLeptons = 2
filtSeq.LepTauPtFilter.MinPt = 13000.
filtSeq.LepTauPtFilter.MinVisPtHadTau = 15000.
filtSeq.LepTauPtFilter.MaxEta = 3.

filtSeq.Expression = "hbbFilter and hTauTauFilter and XtoVVDecayFilterExtended and LepTauPtFilter"

# run Herwig7
Herwig7Config.run()
