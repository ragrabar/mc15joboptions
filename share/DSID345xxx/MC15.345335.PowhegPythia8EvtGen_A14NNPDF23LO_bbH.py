evgenConfig.description = "Powheg+Pythia8 for bbH"
evgenConfig.keywords    = [ 'Higgs', 'bbHiggs','quark' ]
evgenConfig.contact = ["lserkin@cern.ch"]
evgenConfig.inputfilecheck = 'powheg_V2.345335.bbH125_4FS_incl_NNPDF30_13TeV'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')
