###########################################
# MadGraph5, ZZjj, ZZ->4l                 #
###########################################

#### Shower 
evgenConfig.description = 'qq->h->ZZjj + qq->ZZjj, Z->ll sample, off-shell Higgs included, m4l>130 GeV'
evgenConfig.keywords+=['VBF','ZZ', 'mH125']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['Monica Trovatelli <Monica.Trovatelli@cern.ch>', 'Lailin.Xu <Lailin.Xu@cern.ch>']
# comment out inputfilecheck, to allow running on multiple inputs
#evgenConfig.inputfilecheck = 'VBFH125_sbi_4l_m4l130'
evgenConfig.minevents = 5000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include('MC15JobOptions/Pythia8_LHEF.py')


# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]
