model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 220.
mZp = 190.
mHD = 125.
widthZp = 7.559856e-01
widthN2 = 5.805173e-03
filteff = 5.440696e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
