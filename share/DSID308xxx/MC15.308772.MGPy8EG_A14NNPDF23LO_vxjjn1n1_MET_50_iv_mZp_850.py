model="InelasticVectorEFT"
mDM1 = 425.
mDM2 = 1700.
mZp = 850.
mHD = 125.
widthZp = 4.051237e+00
widthN2 = 2.237936e+00
filteff = 1.0

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
