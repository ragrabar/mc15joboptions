model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 450.
mHD = 450.
widthZp = 2.088811e+00
widthhd = 1.790051e-01
filteff = 9.689922e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
