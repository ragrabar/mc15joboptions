model="LightVector"
mDM1 = 65.
mDM2 = 260.
mZp = 130.
mHD = 125.
widthZp = 5.172525e-01
widthN2 = 8.540477e+00
filteff = 8.237232e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
