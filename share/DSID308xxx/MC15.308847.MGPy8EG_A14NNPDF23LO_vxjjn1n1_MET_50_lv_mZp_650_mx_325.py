model="LightVector"
mDM1 = 325.
mDM2 = 1300.
mZp = 650.
mHD = 125.
widthZp = 3.086608e+00
widthN2 = 4.270239e+01
filteff = 9.861933e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
