model="LightVector"
mDM1 = 300.
mDM2 = 1200.
mZp = 600.
mHD = 125.
widthZp = 2.842817e+00
widthN2 = 3.941759e+01
filteff = 9.900990e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
