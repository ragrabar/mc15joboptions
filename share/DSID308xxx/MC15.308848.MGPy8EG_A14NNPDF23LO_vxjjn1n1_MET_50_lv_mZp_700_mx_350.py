model="LightVector"
mDM1 = 350.
mDM2 = 1400.
mZp = 700.
mHD = 125.
widthZp = 3.328931e+00
widthN2 = 4.598718e+01
filteff = 9.940358e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
