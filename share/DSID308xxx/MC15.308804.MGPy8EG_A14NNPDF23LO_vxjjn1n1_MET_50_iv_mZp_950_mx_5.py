model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 980.
mZp = 950.
mHD = 125.
widthZp = 4.530812e+00
widthN2 = 5.200187e-01
filteff = 8.680556e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
