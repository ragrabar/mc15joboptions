model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 130.
mZp = 100.
mHD = 125.
widthZp = 3.978850e-01
widthN2 = 1.152279e-03
filteff = 3.736921e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
