from MadGraphControl.MadGraphUtils import *
import os
# General settings
nevents=int(1.1*runArgs.maxEvents)
mode=0 #0 single machine, 1 cluster, 2 multicore
gridpack_dir='madevent/'
gridpack_mode=True
runName='run_01'

# MG Particle cuts
maxjetflavor=5
dyn_scale = '3'
# Shower/merging settings
parton_shower='PYTHIA8'

### DSID lists (extensions can include systematics samples)
clfv_id = 410775
DSID = runArgs.runNumber


if DSID == clfv_id:
    mgproc="generate p p > t t~ > l+ l- lj l v bj NP=1\n"
    name='ttbarLFV_lept_DF'
    keyword=["ttbar", "BSM"]

else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model clfvLO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define lj = u c u~ c~
define bj = b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define l = e+ mu+ ta+ e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define v = ve vm vt ve~ vm~ vt~
"""+mgproc+"""
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)
lhaid=315200  #NNPDF31_lo_as_0130
pdflabel='lhapdf'

extras = {'lhaid'         : lhaid,
          'pdlabel'       : "'"+pdflabel+"'",
          'maxjetflavor'  : maxjetflavor,
          'parton_shower' : parton_shower,
          'mll_sf'        : 0.0,
          'mll'           : 0.0,
          'drjj'          : 0.0,
          'drll'          : 0.0,
          'draa'          : 0.0,
          'drjl'          : 0.0,
          'dral'          : 0.0,
          'ptl'           : 0.1,
          'pta'           : 0.0,
          'ptj'           : 0.1, # minimum jet pT
          'ptb'           : 0.1, # minimum jet pT
          'etaj'          : -1.0,
          'etab'          : -1.0,
          'etal'          : -1.0,
          'etaa'          : -1.0,
          'jetalgo'   : '-1',  # use anti-kT jet algorithm
          'jetradius' : '0.4', # set jet cone size of 0.4
          'req_acc'   : '0.001',
          'dynamical_scale_choice': dyn_scale}  

doSyst=True

if doSyst:
    lhe_version=3
    extras.update({'use_syst'      : 'T',
                   'sys_scalefact' :'1 0.5 2',                   
                   'sys_matchscale': 'None',
                   'sys_alpsfact'  : '1 0.5 2',
                   'sys_pdf' : 'NNPDF23_lo_as_0130_qed'})

# make a folder called Cards in the current pathand link there the me5_configuration.txt
# to make SetNCores (line 619 of MadGraphUtils) happy
if(os.getcwd().split('/')[-1]!=gridpack_dir.strip('/')):
    os.makedirs(os.getcwd()+'/Cards')
    command = 'cp '+gridpack_dir+'Cards/me5_configuration.txt ./Cards/me5_configuration.txt'
    os.system(command)

build_run_card(
  run_card_old=get_default_runcard(proc_dir=process_dir),
  run_card_new='run_card.dat',
  nevts=nevents,
  rand_seed=runArgs.randomSeed,
  beamEnergy=beamEnergy,
  xqcut=0.,
  extras=extras)

print_cards()

generate(
  proc_dir=process_dir,
  run_card_loc='run_card.dat',
  param_card_loc=None,
  mode=mode,
  njobs=1,
  run_name=runName,
  grid_pack=gridpack_mode,
  gridpack_dir=gridpack_dir,
  nevents=nevents,
  random_seed=runArgs.randomSeed)

outDS = arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.tar.gz',lhe_version=3,saveProcDir=True)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

##shower
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=keyword
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["carlo.gottardo@cern.ch"]
runArgs.inputGeneratorFile = outDS

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

