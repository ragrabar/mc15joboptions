#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG ttbar production'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'dominic@hirschbuehl.de' ]

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 3.
  PowhegConfig.PDF     = 10800
  PowhegConfig.generate()
#--------------------------------------------------------------
# Herwig7 (H7-UE-MMHT) showering
#--------------------------------------------------------------
  include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_EvtGen_Common.py')

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
