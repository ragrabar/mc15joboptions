#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419682E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399774E+03   # H
        36     2.00000000E+03   # A
        37     2.00151730E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.95285059E+03   # ~d_L
   2000001     4.95274061E+03   # ~d_R
   1000002     4.95260467E+03   # ~u_L
   2000002     4.95266273E+03   # ~u_R
   1000003     4.95285059E+03   # ~s_L
   2000003     4.95274061E+03   # ~s_R
   1000004     4.95260467E+03   # ~c_L
   2000004     4.95266273E+03   # ~c_R
   1000005     4.95203189E+03   # ~b_1
   2000005     4.95356067E+03   # ~b_2
   1000006     5.06417497E+03   # ~t_1
   2000006     5.35386138E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.97443182E+03   # ~g
   1000022     1.90930108E+03   # ~chi_10
   1000023    -1.96179841E+03   # ~chi_20
   1000025     1.99666219E+03   # ~chi_30
   1000035     3.12547617E+03   # ~chi_40
   1000024     1.95661319E+03   # ~chi_1+
   1000037     3.12546913E+03   # ~chi_2+
   1000039     2.90033333E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.35544075E-01   # N_11
  1  2    -4.32639451E-02   # N_12
  1  3     4.79363947E-01   # N_13
  1  4    -4.76773899E-01   # N_14
  2  1     2.23208825E-03   # N_21
  2  2    -3.05250403E-03   # N_22
  2  3    -7.07041518E-01   # N_23
  2  4    -7.07161928E-01   # N_24
  3  1     6.77468612E-01   # N_31
  3  2     5.06533477E-02   # N_32
  3  3    -5.17962279E-01   # N_33
  3  4     5.19793801E-01   # N_34
  4  1     2.49224871E-03   # N_41
  4  2    -9.97774099E-01   # N_42
  4  3    -4.49174336E-02   # N_43
  4  4     4.92245997E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.34342634E-02   # U_11
  1  2     9.97986019E-01   # U_12
  2  1     9.97986019E-01   # U_21
  2  2     6.34342634E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.95232125E-02   # V_11
  1  2     9.97580334E-01   # V_12
  2  1     9.97580334E-01   # V_21
  2  2     6.95232125E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99828010E-01   # cos(theta_t)
  1  2     1.85459003E-02   # sin(theta_t)
  2  1    -1.85459003E-02   # -sin(theta_t)
  2  2     9.99828010E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81197793E-01   # cos(theta_b)
  1  2     7.32099424E-01   # sin(theta_b)
  2  1    -7.32099424E-01   # -sin(theta_b)
  2  2     6.81197793E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954967E-01   # cos(theta_tau)
  1  2     7.09252067E-01   # sin(theta_tau)
  2  1    -7.09252067E-01   # -sin(theta_tau)
  2  2     7.04954967E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200216E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305790E+02   # vev(Q)              
         4     3.12009024E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717807E-01   # gprime(Q) DRbar
     2     6.26763094E-01   # g(Q) DRbar
     3     1.08126735E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02489664E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71450333E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738347E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.86734552E+05   # M^2_Hd              
        22    -8.58958978E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36864711E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.36355113E-09   # gluino decays
#          BR         NDA      ID1       ID2
     8.65405857E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.45761848E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
     9.41155137E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.99113905E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.36769247E-11    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.60598726E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.57360052E-11    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.99113905E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.36769247E-11    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.60598726E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.57360052E-11    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.70663230E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.52460857E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.89302263E-07    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.89302263E-07    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.89302263E-07    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.89302263E-07    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.68240400E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.53597100E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.76392444E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.38762395E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.78082523E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.17260737E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.47495026E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.11660813E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.26635715E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.52262062E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.31022661E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.39965716E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.73770921E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.88108338E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.43366496E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.74910167E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.88651947E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.24095560E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.10055104E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     5.57168953E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.50918558E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.72222686E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.18134872E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.27188618E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.25565175E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.80547017E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.38302523E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01994924E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.81060214E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.49918890E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.25947741E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.48414443E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.86720577E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.42855306E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.66338229E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.24392403E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.38103420E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20162062E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.06298732E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.89053139E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.46709154E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.00737462E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.27732487E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.23735694E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.55828732E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.98423268E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.92861333E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.13955725E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.93227928E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75644197E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.23265101E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.61039691E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.06306721E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.15705260E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.75970655E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.71218528E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.28393671E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.19289451E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.56454931E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98466803E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.87231049E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.50988881E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.97610038E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.52327775E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.17447699E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89966752E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.06298732E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.89053139E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.46709154E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.00737462E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.27732487E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.23735694E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.55828732E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.98423268E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.92861333E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.13955725E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.93227928E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75644197E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.23265101E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.61039691E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.06306721E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.15705260E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.75970655E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.71218528E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.28393671E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.19289451E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.56454931E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98466803E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.87231049E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.50988881E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.97610038E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.52327775E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.17447699E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89966752E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.62939215E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.44927081E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71641575E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.79317002E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73835193E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.28885851E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49449823E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77963463E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.49015013E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.96002952E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.50976820E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.20671206E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.62939215E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.44927081E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71641575E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.79317002E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73835193E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.28885851E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49449823E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77963463E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.49015013E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.96002952E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.50976820E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.20671206E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20915408E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.72537150E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45420385E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.36643791E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61319910E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.36165885E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23692068E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.93523949E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.20677342E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59158991E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02320419E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.44030488E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64789282E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.82183794E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30636735E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.62964761E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13310276E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.87259597E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.73680270E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75305258E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.15080842E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.48855759E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.62964761E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13310276E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.87259597E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.73680270E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75305258E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.15080842E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.48855759E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63194688E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13211288E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.86397125E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.73179101E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75064750E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01700166E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48379186E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.47484500E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.32755252E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33589923E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33589923E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196759E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196759E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10425308E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.65783557E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53814374E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05676827E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46116657E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41815955E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52576187E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.11071204E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.88711178E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     2.66422121E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00708759E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88577618E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.07136230E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02520735E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.09108642E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.94428678E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.74984741E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     9.29473074E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18401879E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53321881E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18401879E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53321881E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41631182E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50205351E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50205351E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47089092E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99407975E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99407975E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99407975E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.79957132E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.79957132E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.79957132E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.79957132E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.26652393E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.26652393E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.26652393E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.26652393E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.29265425E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.29265425E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.68615616E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.98349243E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.24179247E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     9.75004860E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.73782212E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.61373270E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.78537424E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.12606365E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.78537424E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.12606365E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.91873694E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.72235898E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.72235898E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.10102534E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.03679287E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.03679287E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.03679287E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.92633769E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.49447070E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.92633769E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.49447070E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.09710942E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.69767209E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.69767209E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.60404435E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.13789792E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.13789792E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.13789792E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.39946623E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.39946623E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.39946623E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.39946623E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.66488081E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.66488081E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.66488081E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.66488081E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.61951135E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.61951135E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     8.02326008E-09    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.94875796E-09    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     8.02326008E-09    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.94875796E-09    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     1.01964008E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.65776167E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.40565052E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52475207E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.91170934E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46909895E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46909895E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07854979E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.16282665E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44802005E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.64860940E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.45698366E-11    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.40140974E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.81023173E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.87895440E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22160542E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02212290E-01    2           5        -5   # BR(h -> b       bb     )
     6.21905707E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20127507E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65747298E-04    2           3        -3   # BR(h -> s       sb     )
     2.00934246E-02    2           4        -4   # BR(h -> c       cb     )
     6.63584058E-02    2          21        21   # BR(h -> g       g      )
     2.30463176E-03    2          22        22   # BR(h -> gam     gam    )
     1.62543580E-03    2          22        23   # BR(h -> Z       gam    )
     2.16470011E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80593554E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094758E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46681398E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430896E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71224965E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548728E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668801E-05    2           4        -4   # BR(H -> c       cb     )
     9.96070887E-01    2           6        -6   # BR(H -> t       tb     )
     7.97753580E-04    2          21        21   # BR(H -> g       g      )
     2.71750512E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030463E-06    2          23        22   # BR(H -> Z       gam    )
     3.34077246E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66583049E-04    2          23        23   # BR(H -> Z       Z      )
     9.01522969E-04    2          25        25   # BR(H -> h       h      )
     7.29635181E-24    2          36        36   # BR(H -> A       A      )
     2.91420837E-11    2          23        36   # BR(H -> Z       A      )
     6.52918395E-12    2          24       -37   # BR(H -> W+      H-     )
     6.52918395E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380473E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46973335E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898867E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270601E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677930E-06    2           3        -3   # BR(A -> s       sb     )
     9.96181165E-06    2           4        -4   # BR(A -> c       cb     )
     9.97000678E-01    2           6        -6   # BR(A -> t       tb     )
     9.43680083E-04    2          21        21   # BR(A -> g       g      )
     3.13388706E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290107E-06    2          23        22   # BR(A -> Z       gam    )
     3.25562194E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471689E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35484288E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238915E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81149600E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49711078E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696914E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732179E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402915E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33667277E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82417595E-13    2          24        36   # BR(H+ -> W+      A      )
