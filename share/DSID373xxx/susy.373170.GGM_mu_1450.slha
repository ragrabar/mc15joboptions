#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413637E+01   # W+
        25     1.26000000E+02   # h
        35     2.00397184E+03   # H
        36     2.00000000E+03   # A
        37     2.00152432E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.38578420E+03   # ~d_L
   2000001     4.38566160E+03   # ~d_R
   1000002     4.38551007E+03   # ~u_L
   2000002     4.38557483E+03   # ~u_R
   1000003     4.38578420E+03   # ~s_L
   2000003     4.38566160E+03   # ~s_R
   1000004     4.38551007E+03   # ~c_L
   2000004     4.38557483E+03   # ~c_R
   1000005     4.38505650E+03   # ~b_1
   2000005     4.38639078E+03   # ~b_2
   1000006     4.51855980E+03   # ~t_1
   2000006     4.84583284E+03   # ~t_2
   1000011     5.00008217E+03   # ~e_L
   2000011     5.00007610E+03   # ~e_R
   1000012     4.99984172E+03   # ~nu_eL
   1000013     5.00008217E+03   # ~mu_L
   2000013     5.00007610E+03   # ~mu_R
   1000014     4.99984172E+03   # ~nu_muL
   1000015     4.99969261E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984172E+03   # ~nu_tauL
   1000021     4.51224778E+03   # ~g
   1000022     1.49232635E+03   # ~chi_10
   1000023    -1.54162174E+03   # ~chi_20
   1000025     1.57953188E+03   # ~chi_30
   1000035     3.12742967E+03   # ~chi_40
   1000024     1.53780630E+03   # ~chi_1+
   1000037     3.12742690E+03   # ~chi_2+
   1000039     6.92500000E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.21756137E-01   # N_11
  1  2    -3.28387794E-02   # N_12
  1  3     4.90454618E-01   # N_13
  1  4    -4.87282219E-01   # N_14
  2  1     2.83808140E-03   # N_21
  2  2    -3.32589281E-03   # N_22
  2  3    -7.07012486E-01   # N_23
  2  4    -7.07187549E-01   # N_24
  3  1     6.92140293E-01   # N_31
  3  2     3.62142821E-02   # N_32
  3  3    -5.08478416E-01   # N_33
  3  4     5.10959922E-01   # N_34
  4  1     1.35596285E-03   # N_41
  4  2    -9.98798818E-01   # N_42
  4  3    -3.22073510E-02   # N_43
  4  4     3.69021562E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.55124063E-02   # U_11
  1  2     9.98963774E-01   # U_12
  2  1     9.98963774E-01   # U_21
  2  2     4.55124063E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.21522086E-02   # V_11
  1  2     9.98639148E-01   # V_12
  2  1     9.98639148E-01   # V_21
  2  2     5.21522086E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99541969E-01   # cos(theta_t)
  1  2    -3.02630502E-02   # sin(theta_t)
  2  1     3.02630502E-02   # -sin(theta_t)
  2  2     9.99541969E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73836882E-01   # cos(theta_b)
  1  2     7.38880137E-01   # sin(theta_b)
  2  1    -7.38880137E-01   # -sin(theta_b)
  2  2     6.73836882E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04327551E-01   # cos(theta_tau)
  1  2     7.09875130E-01   # sin(theta_tau)
  2  1    -7.09875130E-01   # -sin(theta_tau)
  2  2     7.04327551E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90164531E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52198204E+02   # vev(Q)              
         4     3.13550771E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52760149E-01   # gprime(Q) DRbar
     2     6.27031509E-01   # g(Q) DRbar
     3     1.06610883E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02373448E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.70835500E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79789205E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     7.04556605E+05   # M^2_Hd              
        22    -6.57246336E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36984535E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73242346E+00   # gluino decays
#          BR         NDA      ID1       ID2
     4.98990629E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.98990629E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99944812E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99944812E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.01125378E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.01125378E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.00620670E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.00620670E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.98990629E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.98990629E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99944812E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99944812E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.01125378E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.01125378E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.00620670E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.00620670E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.91933687E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.91933687E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.06702039E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.06702039E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.59213360E-07    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     6.14648764E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.79670153E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.96152814E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.25274120E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     7.74060102E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.70821716E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.53444183E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
    -1.49618864E-02    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
    -1.73562157E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     1.21357903E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.48984230E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23721569E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.40988253E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.09653586E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.46398419E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.20968338E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.97194127E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.90222967E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.64221132E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.03145461E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.22529250E-03    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.44178691E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.54013656E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.91559543E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.80594657E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.74002607E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     7.42592078E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.35955271E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     3.72097734E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.26413583E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.09525932E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.43585628E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.49893331E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.76310086E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.31191134E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     1.27291884E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.07187037E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.82275759E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.72415172E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.18816310E-01    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.45988722E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.37755759E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     7.19752676E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     5.28763281E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.03421672E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.71228100E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.85070190E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
#
#         PDG            Width
DECAY   1000001     1.27201773E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.88759569E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.49315692E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.66346126E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.19465387E-01    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.16143302E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.38818830E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     1.79943077E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.28762924E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.03421768E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.71228457E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.85111147E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
#
#         PDG            Width
DECAY   1000004     1.27291884E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.07187037E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.82275759E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.72415172E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.18816310E-01    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.45988722E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.37755759E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     7.19752676E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.28763281E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.03421672E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.71228100E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.85070190E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
#
#         PDG            Width
DECAY   1000003     1.27201773E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.88759569E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.49315692E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.66346126E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.19465387E-01    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.16143302E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.38818830E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     1.79943077E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.28762924E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.03421768E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.71228457E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.85111147E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
#
#         PDG            Width
DECAY   1000011     2.68805379E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.40798085E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78139039E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06785760E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68553922E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.47057160E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.38108156E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03125138E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.26835099E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.03934030E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.73156031E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.30514824E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.68805379E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.40798085E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78139039E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06785760E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68553922E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.47057160E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.38108156E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03125138E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.26835099E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.03934030E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.73156031E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.30514824E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36458236E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.81752971E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.65610975E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.60475627E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51006900E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.72092690E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.02577933E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.17182286E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.36262174E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.67157081E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.57115050E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.68169925E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54415266E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.99813010E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.09400800E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.68818870E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16262610E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.44340384E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.35243012E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69315856E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.24363404E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37639165E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.68818870E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16262610E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.44340384E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.35243012E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69315856E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.24363404E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37639165E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.69082086E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.16148882E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.44199190E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.34523798E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69052412E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.21708368E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.37114823E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22551030E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.35400381E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33609263E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33609263E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203180E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203180E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10366760E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.87867984E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53242129E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11233955E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46239632E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35633705E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53650577E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.84499254E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.55498754E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.34047958E-09   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98014009E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90776752E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12092389E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.48961707E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.16208441E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     9.96712055E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.66694002E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     6.57416946E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18546707E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53501505E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18546707E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53501505E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40397323E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50578691E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50578691E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47173882E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00129820E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00129820E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00129820E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.00006530E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.00006530E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.00006530E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.00006530E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.66688474E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.66688474E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.66688474E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.66688474E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.31918828E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.31918828E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.08003733E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.91391559E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     9.10255468E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     4.37604779E-06    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.86098037E-06    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.10841110E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.05714792E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.20743109E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.05714792E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.20743109E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.76551580E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.06756041E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.06756041E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.46084360E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.96787698E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.96787698E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.96787698E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.31177791E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.99343590E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.31177791E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.99343590E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.58349749E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.83665065E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.83665065E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.73842593E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36531893E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36531893E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36531893E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.34497456E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.34497456E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.34497456E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.34497456E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.48323972E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.48323972E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.48323972E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.48323972E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.44308757E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.44308757E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.87868921E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.39415121E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51843615E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.22998673E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46468707E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46468707E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13937023E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.14120947E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38978322E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.41992580E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.40279222E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.49234317E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.49964540E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.52509185E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22424605E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02423250E-01    2           5        -5   # BR(h -> b       bb     )
     6.21450727E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.19966464E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65407493E-04    2           3        -3   # BR(h -> s       sb     )
     2.00818240E-02    2           4        -4   # BR(h -> c       cb     )
     6.63411603E-02    2          21        21   # BR(h -> g       g      )
     2.30348314E-03    2          22        22   # BR(h -> gam     gam    )
     1.62438047E-03    2          22        23   # BR(h -> Z       gam    )
     2.16353635E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80418195E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78042757E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46231920E-03    2           5        -5   # BR(H -> b       bb     )
     2.46473391E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71375199E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11568264E-06    2           3        -3   # BR(H -> s       sb     )
     1.00670800E-05    2           4        -4   # BR(H -> c       cb     )
     9.96089953E-01    2           6        -6   # BR(H -> t       tb     )
     7.97618768E-04    2          21        21   # BR(H -> g       g      )
     2.71386571E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030002E-06    2          23        22   # BR(H -> Z       gam    )
     3.23347395E-04    2          24       -24   # BR(H -> W+      W-     )
     1.61232720E-04    2          23        23   # BR(H -> Z       Z      )
     9.03127598E-04    2          25        25   # BR(H -> h       h      )
     7.00278841E-24    2          36        36   # BR(H -> A       A      )
     2.82138880E-11    2          23        36   # BR(H -> Z       A      )
     6.10816485E-12    2          24       -37   # BR(H -> W+      H-     )
     6.10816485E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82374667E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46504286E-03    2           5        -5   # BR(A -> b       bb     )
     2.43902569E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62283692E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13679656E-06    2           3        -3   # BR(A -> s       sb     )
     9.96196289E-06    2           4        -4   # BR(A -> c       cb     )
     9.97015814E-01    2           6        -6   # BR(A -> t       tb     )
     9.43694409E-04    2          21        21   # BR(A -> g       g      )
     3.15268467E-06    2          22        22   # BR(A -> gam     gam    )
     1.35271790E-06    2          23        22   # BR(A -> Z       gam    )
     3.15079523E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74466475E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34482758E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49243259E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81164958E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49070084E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45706166E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08734023E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99413661E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.22926686E-04    2          24        25   # BR(H+ -> W+      h      )
     2.89014535E-13    2          24        36   # BR(H+ -> W+      A      )
