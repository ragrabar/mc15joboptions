#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z->invisible l+l- production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.vdecaymode = 1 # Z->e+e-
PowhegConfig.hdecaymode = -1



#
#
##

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations

PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ] 

PowhegConfig.generate()

import random, string, shutil

fname_in = "PowhegOTF._1.events"
fname_tmp = "tmp.events"

id_replace = '11'
id_list = ['11', '13', '15']

file_out = open(fname_tmp, 'w')

counter = 1
id_random = id_replace

for line in open(fname_in, 'r') :
  if line.find(id_replace, 0, 8) != -1 and line.find("<", 0, 8) == -1:
    keep = line[8:]
    change = line[:8]
    if counter == 1 :
      id_random = random.choice(id_list)
      counter = 2
    else :
      counter = 1
    modline = string.replace(change, id_replace, id_random) + keep
    file_out.write(modline)
  else :
    file_out.write(line)

file_out.close()

shutil.move(fname_tmp, fname_in)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2',
                              'Main31:pTHard = 0',
                              'Main31:pTdef = 2' ]

#--------------------------------------------------------------
# Higgs->invisible at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 12 14 16']
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->inv.ll production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'hideki.okawa@cern.ch' ]

evgenConfig.process = "gg->ZH, H->inv, Z->ll"
evgenConfig.minevents   = 500
