include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa tth(h to bb) + 0,1,2 jets at LO"
evgenConfig.keywords = ["top", "SM" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "alan.james.taylor@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.inputconfcheck = "Sherpa_ttH_Htobb"
evgenConfig.process=""" 



(run){

  %scales, tags for scale variations                        
  FSF:=1.; RSF:=1.; QSF:=1.;                                                                          
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2}
  QCUT:=30.; NJET:=2; LJET:=0;


  % decays and force H to bb
  HARD_DECAYS=1
  STABLE[6]=0
  WIDTH[6]=0
  STABLE[24]=0
  STABLE[25]=0 
  MASSIVE[5]=1
  HDH_STATUS[25,5,-5]=2



}(run)

(processes){
  Process 93 93 -> 6 -6 25 93{NJET}
  CKKW sqr(QCUT/E_CMS)
  Order (*,1); 
  End process;
}(processes)


(selector){



    }(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]



include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 20000
filtSeq.LeptonFilter.Etacut = 4.5                                     

filtSeq.Expression = "LeptonFilter"
