######################################################################
# SM QCD diphoton (up to 1 brem) with Pythia 8
######################################################################

evgenConfig.description = "Pythia8 diphoton (up to 1 brem) sample (pt>40GeV)"
evgenConfig.process = "QCD diphoton (up to 1 brem) "
evgenConfig.keywords = ["exotic", "diphoton","SM","egamma"]
evgenConfig.contact = ["leonardo.carminati@cern.ch","giovanni.marchiori@cern.ch"]
evgenConfig.minevents = 1000

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

## Configure Pythia
genSeq.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 30",
                            "PhaseSpace:mHatMin = 100."]
                            
#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

## Filter
if not hasattr( filtSeq, "DirectPhotonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
    filtSeq += DirectPhotonFilter()
    pass

DirectPhotonFilter = filtSeq.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 40000.
DirectPhotonFilter.Etacut = 2.7
DirectPhotonFilter.NPhotons = 2
