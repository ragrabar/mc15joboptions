#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17330169E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.49900000E+02   # M_1(MX)             
         2     8.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04045441E+01   # W+
        25     1.23432651E+02   # h
        35     3.00006356E+03   # H
        36     3.00000002E+03   # A
        37     3.00087270E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04354038E+03   # ~d_L
   2000001     3.03837407E+03   # ~d_R
   1000002     3.04263471E+03   # ~u_L
   2000002     3.03921660E+03   # ~u_R
   1000003     3.04354038E+03   # ~s_L
   2000003     3.03837407E+03   # ~s_R
   1000004     3.04263471E+03   # ~c_L
   2000004     3.03921660E+03   # ~c_R
   1000005     1.10737384E+03   # ~b_1
   2000005     3.03718702E+03   # ~b_2
   1000006     1.10595985E+03   # ~t_1
   2000006     3.02233910E+03   # ~t_2
   1000011     3.00627795E+03   # ~e_L
   2000011     3.00232206E+03   # ~e_R
   1000012     3.00489099E+03   # ~nu_eL
   1000013     3.00627795E+03   # ~mu_L
   2000013     3.00232206E+03   # ~mu_R
   1000014     3.00489099E+03   # ~nu_muL
   1000015     2.98544960E+03   # ~tau_1
   2000015     3.02205520E+03   # ~tau_2
   1000016     3.00455558E+03   # ~nu_tauL
   1000021     2.35428079E+03   # ~g
   1000022     4.51666352E+02   # ~chi_10
   1000023     9.36863748E+02   # ~chi_20
   1000025    -2.99385640E+03   # ~chi_30
   1000035     2.99502000E+03   # ~chi_40
   1000024     9.37027079E+02   # ~chi_1+
   1000037     2.99547329E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99879767E-01   # N_11
  1  2    -6.50557947E-04   # N_12
  1  3     1.51965908E-02   # N_13
  1  4    -3.01536464E-03   # N_14
  2  1     1.11802569E-03   # N_21
  2  2     9.99535214E-01   # N_22
  2  3    -2.88034311E-02   # N_23
  2  4     9.92308946E-03   # N_24
  3  1    -8.60242609E-03   # N_31
  3  2     1.33597599E-02   # N_32
  3  3     7.06893573E-01   # N_33
  3  4     7.07141423E-01   # N_34
  4  1    -1.28530270E-02   # N_41
  4  2     2.73943227E-02   # N_42
  4  3     7.06569814E-01   # N_43
  4  4    -7.06996074E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99169549E-01   # U_11
  1  2    -4.07456993E-02   # U_12
  2  1     4.07456993E-02   # U_21
  2  2     9.99169549E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99901453E-01   # V_11
  1  2    -1.40386426E-02   # V_12
  2  1     1.40386426E-02   # V_21
  2  2     9.99901453E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98707970E-01   # cos(theta_t)
  1  2    -5.08172280E-02   # sin(theta_t)
  2  1     5.08172280E-02   # -sin(theta_t)
  2  2     9.98707970E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99900364E-01   # cos(theta_b)
  1  2     1.41160218E-02   # sin(theta_b)
  2  1    -1.41160218E-02   # -sin(theta_b)
  2  2     9.99900364E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06975742E-01   # cos(theta_tau)
  1  2     7.07237796E-01   # sin(theta_tau)
  2  1    -7.07237796E-01   # -sin(theta_tau)
  2  2     7.06975742E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01764088E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73301694E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43997094E+02   # higgs               
         4     7.83615452E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73301694E+03  # The gauge couplings
     1     3.62657679E-01   # gprime(Q) DRbar
     2     6.36660235E-01   # g(Q) DRbar
     3     1.02307174E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73301694E+03  # The trilinear couplings
  1  1     2.85287646E-06   # A_u(Q) DRbar
  2  2     2.85291594E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73301694E+03  # The trilinear couplings
  1  1     8.16339405E-07   # A_d(Q) DRbar
  2  2     8.16468301E-07   # A_s(Q) DRbar
  3  3     1.70398357E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73301694E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.61064100E-07   # A_mu(Q) DRbar
  3  3     1.62946344E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73301694E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.48040358E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73301694E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.18448092E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73301694E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.08217428E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73301694E+03  # The soft SUSY breaking masses at the scale Q
         1     4.49900000E+02   # M_1(Q)              
         2     8.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.73618831E+04   # M^2_Hd              
        22    -9.00884122E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40680067E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.81444011E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48127276E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48127276E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51872724E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51872724E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.42152206E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.23423697E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.76576303E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.63585847E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.60949443E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.41017360E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.87197169E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.50188278E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.45215739E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.52723691E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.46819927E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.87667296E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.91576999E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.34739139E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.65260861E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.17661188E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.85518411E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.32953818E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.93264192E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.81126125E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.83702697E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29964054E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.89776779E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.21350019E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.03138842E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.43507204E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.22870109E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.49453804E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.88257188E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.24564169E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.98978148E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.73999483E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.45339115E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.12677697E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56277690E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.67851469E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.21326454E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.57528513E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43722104E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.44052610E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.31141031E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.49264364E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.87797666E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.84477494E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.98415624E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.75749463E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.46007693E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63627765E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43258087E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.76042422E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.32020252E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.04062770E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55674133E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.43507204E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.22870109E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.49453804E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.88257188E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.24564169E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.98978148E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.73999483E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.45339115E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.12677697E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56277690E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.67851469E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.21326454E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.57528513E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43722104E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.44052610E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.31141031E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.49264364E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.87797666E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.84477494E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.98415624E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.75749463E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.46007693E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63627765E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43258087E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.76042422E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.32020252E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.04062770E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55674133E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.33605477E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.12347124E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96294845E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.76835069E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.18134951E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91357959E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.21107497E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.50045690E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998927E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.06636997E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.45737358E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.08298527E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.33605477E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.12347124E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96294845E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.76835069E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.18134951E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91357959E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.21107497E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.50045690E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998927E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.06636997E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.45737358E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.08298527E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.44141751E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.83631705E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05754617E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.10613678E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.39215388E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.92587036E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.02764294E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.85678932E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.03498271E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04588653E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.10991912E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.33600198E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12806503E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95350317E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.50780209E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.88307137E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.91843149E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.60697093E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.33600198E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12806503E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95350317E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.50780209E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.88307137E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.91843149E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.60697093E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33582021E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12798894E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95319241E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.11988776E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.75776223E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.91879018E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.82301819E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.69749432E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.67381250E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.86334449E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.94304706E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.87539093E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.64722170E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.42991498E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.44212622E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.18689324E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.40136427E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.09957270E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.39004273E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.67890368E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.73439829E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.16108470E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.42311531E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.42311531E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.07161165E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.49483465E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51650527E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.51650527E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.31307339E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.31307339E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.92414978E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.92414978E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.61308679E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.83917715E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.48292365E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.49396484E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.49396484E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.82835430E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.73652539E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.47485545E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.47485545E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.34158414E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.34158414E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.60335407E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.60335407E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.56312042E-03   # h decays
#          BR         NDA      ID1       ID2
     6.90120967E-01    2           5        -5   # BR(h -> b       bb     )
     5.64744246E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.99927315E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.25357658E-04    2           3        -3   # BR(h -> s       sb     )
     1.83651857E-02    2           4        -4   # BR(h -> c       cb     )
     5.82381004E-02    2          21        21   # BR(h -> g       g      )
     1.95614523E-03    2          22        22   # BR(h -> gam     gam    )
     1.18701272E-03    2          22        23   # BR(h -> Z       gam    )
     1.54235720E-01    2          24       -24   # BR(h -> W+      W-     )
     1.87971592E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39975142E+01   # H decays
#          BR         NDA      ID1       ID2
     7.52125847E-01    2           5        -5   # BR(H -> b       bb     )
     1.77653606E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28140697E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.71359246E-04    2           3        -3   # BR(H -> s       sb     )
     2.17613303E-07    2           4        -4   # BR(H -> c       cb     )
     2.17083176E-02    2           6        -6   # BR(H -> t       tb     )
     1.97335716E-05    2          21        21   # BR(H -> g       g      )
     2.11465084E-08    2          22        22   # BR(H -> gam     gam    )
     8.38622841E-09    2          23        22   # BR(H -> Z       gam    )
     2.99586867E-05    2          24       -24   # BR(H -> W+      W-     )
     1.49608793E-05    2          23        23   # BR(H -> Z       Z      )
     7.92714611E-05    2          25        25   # BR(H -> h       h      )
    -3.00215536E-23    2          36        36   # BR(H -> A       A      )
     1.76567883E-19    2          23        36   # BR(H -> Z       A      )
     1.38481050E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.02136254E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.90895994E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.68245263E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.42171425E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.32685298E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.34035830E+01   # A decays
#          BR         NDA      ID1       ID2
     7.85539603E-01    2           5        -5   # BR(A -> b       bb     )
     1.85526067E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.55974920E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.05673416E-04    2           3        -3   # BR(A -> s       sb     )
     2.27349617E-07    2           4        -4   # BR(A -> c       cb     )
     2.26671014E-02    2           6        -6   # BR(A -> t       tb     )
     6.67524422E-05    2          21        21   # BR(A -> g       g      )
     7.70260348E-08    2          22        22   # BR(A -> gam     gam    )
     6.57960494E-08    2          23        22   # BR(A -> Z       gam    )
     3.11730291E-05    2          23        25   # BR(A -> Z       h      )
     2.54104185E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22055787E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.26752471E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.76662555E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04618165E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.12143347E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37763431E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40673500E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.17716309E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94046978E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01641350E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.28389275E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.99984370E-05    2          24        25   # BR(H+ -> W+      h      )
     6.36602851E-14    2          24        36   # BR(H+ -> W+      A      )
     1.75268773E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.97475141E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.90195026E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
