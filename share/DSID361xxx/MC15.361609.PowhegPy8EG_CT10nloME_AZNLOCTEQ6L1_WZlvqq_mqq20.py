#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.decay_mode = 'WZlvqq'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 20.0     #GeV - where mll is effectively cutting on mqq

PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Diboson WZ->lvqq production with AZNLO CTEQ6L1 tune and mqqmin20'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WZ', '2jet', 'neutrino', '1lepton' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch' ]
evgenConfig.minevents   = 5000
