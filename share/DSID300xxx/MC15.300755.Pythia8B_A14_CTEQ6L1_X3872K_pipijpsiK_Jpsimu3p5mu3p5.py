evgenConfig.description = "Signal B-->X(3872)K-->J/psi(mumu)pipiK"
evgenConfig.keywords = ["bottom","exclusive","Jpsi","2muon"]
evgenConfig.contact = [ 'wesong.cern.ch' ]
evgenConfig.process = "pp->bb->B->X(3872)K->J/psi(mumu)pipiK"
evgenConfig.minevents = 500
 
include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveAntiB_Common.py")
 
#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
genSeq.Pythia8B.QuarkPtCut = 8.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
 
genSeq.Pythia8B.NHadronizationLoops = 1
 
# define pseudo-J/psi for exclusive decay
genSeq.Pythia8B.Commands += ['1099443:all = mypsi(2S) void 3 0 0 3.8717 0.0001 3.8712 3.8722 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
	
# force its decay to mu+ mu-
genSeq.Pythia8B.Commands += ['1099443:onMode = off'] 
genSeq.Pythia8B.Commands += ['1099443:oneChannel = on 1. 0 443 -211 211'] # onMode bRatio meMode products
#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
 
#
# B+ decays:
#
genSeq.Pythia8B.Commands += ['521:addChannel = 3 1.0 0 1099443 321']
 
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

