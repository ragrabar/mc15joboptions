f = open("WJD_TEST.DEC","w")

f.write("Alias MyJ/psi J/psi\n")
f.write("Decay MyJ/psi\n")
f.write("1.0 mu+ mu- PHOTOS VLL;\n")
f.write("Enddecay\n")

f.write("Alias Myphi  phi\n")
f.write("Decay Myphi\n")
f.write("1.0 K- K+ VSS;\n")
f.write("Enddecay\n")

f.write("Alias MyD_s+  D_s+\n")
f.write("Decay MyD_s+\n")
f.write("0.583 K- K+ pi+ PHSP;\n")
f.write("0.417 Myphi pi+ SVS;\n")
f.write("Enddecay\n")

f.write("Alias MyD_s-  D_s-\n")
f.write("Decay MyD_s-\n")
f.write("0.583 K- K+ pi- PHSP;\n")
f.write("0.417 Myphi pi- SVS;\n")
f.write("Enddecay\n")

f.write("Decay W+\n")
f.write("1.0 MyJ/psi MyD_s+ PHSP;\n")
f.write("Enddecay\n")

f.write("Decay W-\n")
f.write("1.0 MyJ/psi MyD_s- PHSP;\n")
f.write("Enddecay\n")

f.write("End\n")
f.close()


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "W production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["W","Jpsi","2muon"]
evgenConfig.contact = ["vtskhay@cern.ch"]
evgenConfig.generators += ['Pythia8']
evgenConfig.maxeventsstrategy = 'INPUTEVENTS'
evgenConfig.minevents = 5000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
#include("MC15JobOptions/BSignalFilter.py")

#genSeq.Pythia8.Commands += ["WeakDoubleBoson:ffbar2WW = on"]  # create W bosons
genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on"]
#genSeq.Pythia8.Commands += ["24:isResonance = false"]
#genSeq.Pythia8.Commands += ["24:doExternalDecays = true"]
genSeq.Pythia8.Commands += ["ProcessLevel:resonanceDecays = off"]
#genSeq.Pythia8.Commands += ["24:onMode = off"]
#genSeq.Pythia8.Commands += ["-24:onMode = off"]
#genSeq.Pythia8.Commands += ["24:onIfAny = -3 4"]
#genSeq.Pythia8.Commands += ["-24:onIfAny = 3 -4"]

genSeq.EvtInclusiveDecay.userDecayFile   = "WJD_TEST.DEC"
#genSeq.EvtInclusiveDecay.whiteList+=[24,-24]
genSeq.EvtInclusiveDecay.allowAllKnownDecays = True















