######################################################
# Job options for Pythia8B_i generation of Bs->mu3p5mu3p5
######################################################
evgenConfig.description = "Exclusive Bs->mu3p5mu3p5 production"
evgenConfig.keywords = ["exclusive","Bs","2muon","rareDecay"]
evgenConfig.minevents = 500
 
include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")

include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.Commands += ['531:addChannel = 2 1.0 0 -13 13']
genSeq.Pythia8B.SignalPDGCodes = [531,-13,13]

genSeq.Pythia8B.NHadronizationLoops = 2

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

