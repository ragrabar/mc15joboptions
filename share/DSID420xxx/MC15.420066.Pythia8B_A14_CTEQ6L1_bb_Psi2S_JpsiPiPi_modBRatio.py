#######################################################################
# Job options fragment for pp->X X(3872)(->pi,pi,Jpsi(mu4mu4))
#######################################################################
evgenConfig.description = "bb->X Psi(2S)(->pi,pi,Jpsi(mu4mu4))"
evgenConfig.keywords = ["charmonium","bottom","pi+","pi-","2muon","Jpsi"]
evgenConfig.minevents = 200

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')

# Hard process
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] # Equivalent of CKIN3
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

# Close B decays and open antiB decays
include ("MC15JobOptions/Pythia8B_CloseBDecays.py")

# Open only decays into Psi(2S)
# B0
genSeq.Pythia8B.Commands += ['511:onIfAny = 100443']
# B+/-
genSeq.Pythia8B.Commands += ['521:onIfAny = 100443']
# Bs
genSeq.Pythia8B.Commands += ['531:onIfAny = 100443']
# Bc
genSeq.Pythia8B.Commands += ['541:onIfAny = 100443']
# LambdaB
genSeq.Pythia8B.Commands += ['5122:onIfAny = 100443']
# Xb+/-
genSeq.Pythia8B.Commands += ['5132:onIfAny = 100443']
# Xb
genSeq.Pythia8B.Commands += ['5232:onIfAny = 100443']
# Omega_b+/-
genSeq.Pythia8B.Commands += ['5332:onIfAny = 100443']

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

# Quark cuts
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 4.0
genSeq.Pythia8B.QuarkEtaCut = 103.1
genSeq.Pythia8B.AntiQuarkEtaCut = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Turn on colour-octet decays into Psi(2S)
genSeq.Pythia8B.Commands += ['9940103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9941103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9942103:0:products = 100443 21']

# Turn all decays off, then allow decay number 44 for Psi(2S)
# Psi(2S) -> Jpsi pi+ pi-
genSeq.Pythia8B.Commands += ['100443:onMode = off']
genSeq.Pythia8B.Commands += ['100443:44:onMode = on']

# Turn all decays off, then allow decay number 2 for Jpsi
# Jpsi -> mu+ mu-
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

# Only one open decay channel
# Set branching ratio to 1.0
# To increase efficiency of event production
genSeq.Pythia8B.Commands += ['100443:44:bRatio = 1.0']
genSeq.Pythia8B.Commands += ['443:2:bRatio = 1.0']

# Select signal particles
genSeq.Pythia8B.SignalPDGCodes = [100443,443,-13,13,-211,211] #mu+ mu- pi- pi+
genSeq.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.38, 0.38]
genSeq.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]

# Number of repeat-hadronization loop
genSeq.Pythia8B.NHadronizationLoops = 2

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [4.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]
