###############################################################
#
# Job options file for Hijing (with diffraction) generation of
# p + Pb collisions at 8160 GeV CMS (6.5 TeV p, 2.562 TeV Pb/n) with diffraction
#
# Andrzej Olszewski
#
# Created: January 2014
# Updated: August 2016
#==============================================================

# use common fragment
include("MC15JobOptions/Hijing_Common.py")

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["inclusive"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

evgenConfig.minevents = 1000

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
Hijing.McEventsRW = "HIJING_EVENT"
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 8160.", "frame CMS", "proj A", "targ P",
                     "iap 208", "izp 82", "iat 1", "izt 1",
# simulation of minimum-bias events
                     "bmin 0", "bmax 10",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1",
# turning OFF string radiation
                     "ihpr2 1 0",
# Nov 10,11, set minimum pt for hard scatterings to default, 2 GeV
                     "hipr1 8 2"
                     ]

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
genSeq += BoostEvent()

boost=genSeq.BoostEvent
boost.BetaZ=-0.4345
boost.McInputKey="HIJING_EVENT"
boost.McOutputKey="GEN_EVENT"
