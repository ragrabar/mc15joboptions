#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13067891E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.69990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04174693E+01   # W+
        25     1.25481108E+02   # h
        35     4.00001552E+03   # H
        36     3.99999791E+03   # A
        37     4.00099924E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02602751E+03   # ~d_L
   2000001     4.02258248E+03   # ~d_R
   1000002     4.02537321E+03   # ~u_L
   2000002     4.02363850E+03   # ~u_R
   1000003     4.02602751E+03   # ~s_L
   2000003     4.02258248E+03   # ~s_R
   1000004     4.02537321E+03   # ~c_L
   2000004     4.02363850E+03   # ~c_R
   1000005     8.40284772E+02   # ~b_1
   2000005     4.02548530E+03   # ~b_2
   1000006     8.29162349E+02   # ~t_1
   2000006     2.27888050E+03   # ~t_2
   1000011     4.00472457E+03   # ~e_L
   2000011     4.00300043E+03   # ~e_R
   1000012     4.00360959E+03   # ~nu_eL
   1000013     4.00472457E+03   # ~mu_L
   2000013     4.00300043E+03   # ~mu_R
   1000014     4.00360959E+03   # ~nu_muL
   1000015     4.00345846E+03   # ~tau_1
   2000015     4.00848100E+03   # ~tau_2
   1000016     4.00501431E+03   # ~nu_tauL
   1000021     1.98156622E+03   # ~g
   1000022     5.43532480E+02   # ~chi_10
   1000023    -5.81919508E+02   # ~chi_20
   1000025     6.02664657E+02   # ~chi_30
   1000035     2.05232838E+03   # ~chi_40
   1000024     5.79838363E+02   # ~chi_1+
   1000037     2.05249267E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48239113E-01   # N_11
  1  2    -2.25951900E-02   # N_12
  1  3    -4.85045754E-01   # N_13
  1  4    -4.52060066E-01   # N_14
  2  1    -2.89119382E-02   # N_21
  2  2     2.23579335E-02   # N_22
  2  3    -7.05384129E-01   # N_23
  2  4     7.07882373E-01   # N_24
  3  1     6.62797683E-01   # N_31
  3  2     2.83665981E-02   # N_32
  3  3     5.16784949E-01   # N_33
  3  4     5.41135736E-01   # N_34
  4  1    -1.24943375E-03   # N_41
  4  2     9.99092046E-01   # N_42
  4  3    -9.85715032E-03   # N_43
  4  4    -4.14289656E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.39463152E-02   # U_11
  1  2     9.99902745E-01   # U_12
  2  1    -9.99902745E-01   # U_21
  2  2     1.39463152E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.85965953E-02   # V_11
  1  2    -9.98281743E-01   # V_12
  2  1    -9.98281743E-01   # V_21
  2  2    -5.85965953E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96237646E-01   # cos(theta_t)
  1  2    -8.66634450E-02   # sin(theta_t)
  2  1     8.66634450E-02   # -sin(theta_t)
  2  2     9.96237646E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998320E-01   # cos(theta_b)
  1  2    -1.83302951E-03   # sin(theta_b)
  2  1     1.83302951E-03   # -sin(theta_b)
  2  2     9.99998320E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06361923E-01   # cos(theta_tau)
  1  2     7.07850856E-01   # sin(theta_tau)
  2  1    -7.07850856E-01   # -sin(theta_tau)
  2  2    -7.06361923E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00221506E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30678907E+03  # DRbar Higgs Parameters
         1    -5.69990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43777199E+02   # higgs               
         4     1.62553159E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30678907E+03  # The gauge couplings
     1     3.61831621E-01   # gprime(Q) DRbar
     2     6.35684680E-01   # g(Q) DRbar
     3     1.03009030E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30678907E+03  # The trilinear couplings
  1  1     1.38100824E-06   # A_u(Q) DRbar
  2  2     1.38102148E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30678907E+03  # The trilinear couplings
  1  1     5.11864315E-07   # A_d(Q) DRbar
  2  2     5.11911193E-07   # A_s(Q) DRbar
  3  3     9.12889187E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30678907E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13650812E-07   # A_mu(Q) DRbar
  3  3     1.14799194E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30678907E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65654134E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30678907E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88014449E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30678907E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02618162E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30678907E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55695521E+07   # M^2_Hd              
        22    -3.29786468E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40192818E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.70176619E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44928384E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44928384E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55071616E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55071616E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.49531277E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.80661733E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.87233407E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.91304508E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.40800352E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.30101318E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.58756332E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11336486E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.68347406E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.15511018E-10    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.21257851E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.81277173E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.50961653E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.43960083E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.81590621E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.07480160E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.78249784E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.08447351E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.60489623E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.31860020E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.49920301E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67075991E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58108334E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80836464E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53473527E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.24176587E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62116589E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.44496736E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12839839E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.60879441E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.97774881E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.36599939E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.72872446E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78867790E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.19856406E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38180062E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98133588E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78951506E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.74721419E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56697011E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52656709E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60992891E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.01214028E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.47335825E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34379449E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.79066285E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46395871E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78888724E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.27796055E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.48674931E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.91174291E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79537420E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25580515E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60069058E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52872792E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54428649E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.84819453E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.16553954E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.10677831E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24778646E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86033359E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78867790E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.19856406E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38180062E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98133588E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78951506E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.74721419E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56697011E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52656709E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60992891E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.01214028E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.47335825E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34379449E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.79066285E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46395871E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78888724E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.27796055E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.48674931E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.91174291E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79537420E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25580515E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60069058E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52872792E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54428649E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.84819453E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.16553954E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.10677831E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24778646E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86033359E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12825718E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.06429202E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71714730E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.08758935E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78878078E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91867029E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59394070E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00147168E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.61987855E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.34471363E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.37176789E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.83765953E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12825718E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.06429202E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71714730E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.08758935E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78878078E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91867029E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59394070E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00147168E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.61987855E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.34471363E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.37176789E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.83765953E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04317472E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.00869997E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52514471E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.71885257E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41654721E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.61957975E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84142781E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03122820E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.01444323E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.80773354E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.54943284E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45823875E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.72173612E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92493821E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12929826E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96646224E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.42389964E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.97966169E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79391471E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.38493523E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.57019964E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12929826E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96646224E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.42389964E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.97966169E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79391471E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.38493523E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.57019964E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44523688E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05592227E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.74567574E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.43339100E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53986412E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.40687880E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06377100E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.39475917E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33768255E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33768255E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11257431E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11257431E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09948629E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.54465933E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.68671433E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.55226308E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.01815054E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.58297799E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.83775145E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.25444290E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.91690299E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.62442365E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.12114202E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18919361E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54271510E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18919361E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54271510E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34608937E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53845449E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53845449E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48944344E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07408023E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07408023E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07408013E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.21287309E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.21287309E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.21287309E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.21287309E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.40429196E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.40429196E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.40429196E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.40429196E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.17318676E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.17318676E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.13589993E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.67957158E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.36680713E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18188527E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.49892887E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.18188527E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.49892887E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.51627464E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.17850079E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.17850079E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.22999773E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.00725144E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.00725144E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.00717213E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.16250172E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.80542151E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.16250172E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.80542151E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.72031857E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.43492978E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.43492978E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.17148476E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.28636112E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.28636112E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.28636113E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.19626119E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.19626119E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.19626119E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.19626119E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.98748377E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.98748377E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.98748377E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.98748377E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.86998944E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.86998944E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.54301181E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.83354402E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.08828444E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.14762713E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.84014331E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.84014331E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.83729107E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.90315255E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.15928182E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80286029E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.80286029E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.81717580E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.81717580E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98379392E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90132283E-01    2           5        -5   # BR(h -> b       bb     )
     6.53597643E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31373362E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90723572E-04    2           3        -3   # BR(h -> s       sb     )
     2.13166387E-02    2           4        -4   # BR(h -> c       cb     )
     6.97118968E-02    2          21        21   # BR(h -> g       g      )
     2.40223555E-03    2          22        22   # BR(h -> gam     gam    )
     1.63812816E-03    2          22        23   # BR(h -> Z       gam    )
     2.20823076E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78938802E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54690880E+01   # H decays
#          BR         NDA      ID1       ID2
     3.71264916E-01    2           5        -5   # BR(H -> b       bb     )
     5.97739916E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11346356E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49986234E-04    2           3        -3   # BR(H -> s       sb     )
     7.01033726E-08    2           4        -4   # BR(H -> c       cb     )
     7.02581767E-03    2           6        -6   # BR(H -> t       tb     )
     8.42783376E-07    2          21        21   # BR(H -> g       g      )
     4.24768668E-09    2          22        22   # BR(H -> gam     gam    )
     1.81014657E-09    2          23        22   # BR(H -> Z       gam    )
     1.53531173E-06    2          24       -24   # BR(H -> W+      W-     )
     7.67124039E-07    2          23        23   # BR(H -> Z       Z      )
     6.41452093E-06    2          25        25   # BR(H -> h       h      )
     2.72846197E-24    2          36        36   # BR(H -> A       A      )
     1.58122790E-20    2          23        36   # BR(H -> Z       A      )
     1.88153068E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.49652307E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.49652307E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.50496780E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.43182047E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66367370E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.04032047E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.11194034E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.14455085E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.66477361E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.30131076E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.57092994E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.65793457E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.44759152E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.44759152E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44818417E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54620093E+01   # A decays
#          BR         NDA      ID1       ID2
     3.71337529E-01    2           5        -5   # BR(A -> b       bb     )
     5.97817860E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11373749E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50059474E-04    2           3        -3   # BR(A -> s       sb     )
     7.05782876E-08    2           4        -4   # BR(A -> c       cb     )
     7.04145073E-03    2           6        -6   # BR(A -> t       tb     )
     1.44683944E-05    2          21        21   # BR(A -> g       g      )
     7.20493971E-08    2          22        22   # BR(A -> gam     gam    )
     1.59054094E-08    2          23        22   # BR(A -> Z       gam    )
     1.53227738E-06    2          23        25   # BR(A -> Z       h      )
     1.95904056E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.49619539E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.49619539E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.24450084E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.58453414E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.48379839E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.35687534E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.24331344E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.88868859E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.79831340E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.64282111E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.47855768E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.49225924E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.49225924E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.58263610E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.00451418E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94064868E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10046783E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.84288585E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18995539E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44812090E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.81893217E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.52395344E-06    2          24        25   # BR(H+ -> W+      h      )
     2.37287474E-14    2          24        36   # BR(H+ -> W+      A      )
     4.18148333E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.21031025E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.83750017E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.48888491E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.00602669E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.48424590E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.02083020E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.93647396E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.92415560E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
