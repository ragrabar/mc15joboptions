#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419613E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03963690E+01   # W+
        25     1.25023610E+02   # h
        35     3.00002871E+03   # H
        36     2.99999995E+03   # A
        37     3.00109166E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04042257E+03   # ~d_L
   2000001     3.03521665E+03   # ~d_R
   1000002     3.03951077E+03   # ~u_L
   2000002     3.03666091E+03   # ~u_R
   1000003     3.04042257E+03   # ~s_L
   2000003     3.03521665E+03   # ~s_R
   1000004     3.03951077E+03   # ~c_L
   2000004     3.03666091E+03   # ~c_R
   1000005     9.97052277E+02   # ~b_1
   2000005     3.03289384E+03   # ~b_2
   1000006     9.94930205E+02   # ~t_1
   2000006     3.02408647E+03   # ~t_2
   1000011     3.00651548E+03   # ~e_L
   2000011     3.00171378E+03   # ~e_R
   1000012     3.00512164E+03   # ~nu_eL
   1000013     3.00651548E+03   # ~mu_L
   2000013     3.00171378E+03   # ~mu_R
   1000014     3.00512164E+03   # ~nu_muL
   1000015     2.98602127E+03   # ~tau_1
   2000015     3.02179069E+03   # ~tau_2
   1000016     3.00500789E+03   # ~nu_tauL
   1000021     2.35014846E+03   # ~g
   1000022     1.51066600E+02   # ~chi_10
   1000023     3.22833614E+02   # ~chi_20
   1000025    -2.99594462E+03   # ~chi_30
   1000035     2.99596361E+03   # ~chi_40
   1000024     3.22995750E+02   # ~chi_1+
   1000037     2.99688890E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891825E-01   # N_11
  1  2     3.81644336E-04   # N_12
  1  3    -1.47034822E-02   # N_13
  1  4     3.31792698E-07   # N_14
  2  1     1.66833283E-06   # N_21
  2  2     9.99659506E-01   # N_22
  2  3     2.60606958E-02   # N_23
  2  4     1.30870359E-03   # N_24
  3  1     1.04001706E-02   # N_31
  3  2    -1.93507682E-02   # N_32
  3  3     7.06764217E-01   # N_33
  3  4     7.07108002E-01   # N_34
  4  1    -1.04006966E-02   # N_41
  4  2     1.75007054E-02   # N_42
  4  3    -7.06816093E-01   # N_43
  4  4     7.07104350E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321073E-01   # U_11
  1  2     3.68428216E-02   # U_12
  2  1    -3.68428216E-02   # U_21
  2  2     9.99321073E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998291E-01   # V_11
  1  2    -1.84890774E-03   # V_12
  2  1    -1.84890774E-03   # V_21
  2  2    -9.99998291E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481734E-01   # cos(theta_t)
  1  2    -5.50838168E-02   # sin(theta_t)
  2  1     5.50838168E-02   # -sin(theta_t)
  2  2     9.98481734E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99705928E-01   # cos(theta_b)
  1  2    -2.42498974E-02   # sin(theta_b)
  2  1     2.42498974E-02   # -sin(theta_b)
  2  2     9.99705928E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06931008E-01   # cos(theta_tau)
  1  2     7.07282511E-01   # sin(theta_tau)
  2  1    -7.07282511E-01   # -sin(theta_tau)
  2  2    -7.06931008E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00169974E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64196132E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43893369E+02   # higgs               
         4     1.02397980E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64196132E+03  # The gauge couplings
     1     3.62454433E-01   # gprime(Q) DRbar
     2     6.38913530E-01   # g(Q) DRbar
     3     1.02429985E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64196132E+03  # The trilinear couplings
  1  1     2.56200639E-06   # A_u(Q) DRbar
  2  2     2.56204581E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64196132E+03  # The trilinear couplings
  1  1     8.91388005E-07   # A_d(Q) DRbar
  2  2     8.91497270E-07   # A_s(Q) DRbar
  3  3     1.85508831E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64196132E+03  # The trilinear couplings
  1  1     4.03629295E-07   # A_e(Q) DRbar
  2  2     4.03649452E-07   # A_mu(Q) DRbar
  3  3     4.09377638E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64196132E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50744671E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64196132E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.84573555E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64196132E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02925232E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64196132E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.60446267E+04   # M^2_Hd              
        22    -9.03603985E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41713713E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.43095238E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47921508E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47921508E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52078492E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52078492E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.82873707E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.39177620E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.13770062E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.72312176E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13024133E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.84481382E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.96915197E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.97239721E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.33062652E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.99223585E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66817696E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60349719E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14133644E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.67102051E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.47841711E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.43682346E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.41533483E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.36738146E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.10315566E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.39014922E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.22502529E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.14709297E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.47189081E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.46603429E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.12479051E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.02344768E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.48667435E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.01302834E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98494569E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63904710E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.70729028E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.75545220E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28024304E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.02060319E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.02085955E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16643679E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60378770E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.39194261E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.42410936E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.42296399E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39621202E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.01803814E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.93442778E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63840160E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.36137032E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.13405976E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27451575E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.21832429E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02773281E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65359960E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.57000189E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.25146607E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.78675322E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.78358075E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54299973E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.01302834E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98494569E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63904710E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.70729028E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.75545220E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28024304E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.02060319E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.02085955E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16643679E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60378770E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.39194261E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.42410936E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.42296399E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39621202E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.01803814E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.93442778E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63840160E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.36137032E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.13405976E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27451575E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.21832429E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02773281E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65359960E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.57000189E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.25146607E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.78675322E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.78358075E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54299973E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96570423E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.86835218E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00579544E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.74439892E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.03395114E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00736895E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.41624234E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56077230E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999997E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73373624E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.60357448E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.59320630E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96570423E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.86835218E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00579544E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.74439892E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.03395114E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00736895E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.41624234E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56077230E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999997E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73373624E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.60357448E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.59320630E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79082630E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49503253E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16953562E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33543185E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73351834E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57624593E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14231304E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.51247316E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.30947137E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28100715E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.51687463E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96595901E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.83658876E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00413252E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.29556984E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.23997013E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01220847E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.29785583E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96595901E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.83658876E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00413252E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.29556984E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.23997013E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01220847E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.29785583E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96614133E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.83576048E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00387540E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.11604339E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.08610267E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01253085E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.75674711E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.42633581E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73033988E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34503826E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.47969296E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.87675771E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.52789355E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.08052026E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.35771043E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.78653486E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.31584903E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.22924154E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.77075846E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.68887736E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07021168E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.94960337E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.39006803E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.39006803E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.07325329E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.46767020E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32498576E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32498576E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.66263610E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.66263610E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.30143334E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.30143334E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.81579556E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.05547787E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.55975001E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.29740384E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.29740384E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33721947E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.58393740E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.18662292E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.18662292E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.56815166E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.56815166E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.28811962E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.28811962E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.63207467E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64350047E-01    2           5        -5   # BR(h -> b       bb     )
     7.14122996E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.52801542E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.36553808E-04    2           3        -3   # BR(h -> s       sb     )
     2.33122548E-02    2           4        -4   # BR(h -> c       cb     )
     7.57330290E-02    2          21        21   # BR(h -> g       g      )
     2.59220927E-03    2          22        22   # BR(h -> gam     gam    )
     1.72382422E-03    2          22        23   # BR(h -> Z       gam    )
     2.31185104E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89018764E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.51943257E+01   # H decays
#          BR         NDA      ID1       ID2
     8.96073778E-01    2           5        -5   # BR(H -> b       bb     )
     7.06568372E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.49825692E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.06787468E-04    2           3        -3   # BR(H -> s       sb     )
     8.59998565E-08    2           4        -4   # BR(H -> c       cb     )
     8.57903305E-03    2           6        -6   # BR(H -> t       tb     )
     1.21245772E-05    2          21        21   # BR(H -> g       g      )
     5.84122151E-08    2          22        22   # BR(H -> gam     gam    )
     3.50703401E-09    2          23        22   # BR(H -> Z       gam    )
     8.60877306E-07    2          24       -24   # BR(H -> W+      W-     )
     4.29907691E-07    2          23        23   # BR(H -> Z       Z      )
     5.94615341E-06    2          25        25   # BR(H -> h       h      )
    -7.29232009E-25    2          36        36   # BR(H -> A       A      )
     1.07940334E-20    2          23        36   # BR(H -> Z       A      )
     9.16520253E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.39259929E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.58297993E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.84995349E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24052450E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.24511224E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.44110225E+01   # A decays
#          BR         NDA      ID1       ID2
     9.16507795E-01    2           5        -5   # BR(A -> b       bb     )
     7.22650416E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.55511561E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.13821254E-04    2           3        -3   # BR(A -> s       sb     )
     8.85559097E-08    2           4        -4   # BR(A -> c       cb     )
     8.82915838E-03    2           6        -6   # BR(A -> t       tb     )
     2.60010264E-05    2          21        21   # BR(A -> g       g      )
     6.60050639E-08    2          22        22   # BR(A -> gam     gam    )
     2.55924193E-08    2          23        22   # BR(A -> Z       gam    )
     8.77202460E-07    2          23        25   # BR(A -> Z       h      )
     9.73139806E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.53862492E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.86579757E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.96507981E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.76528807E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46236544E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.60671601E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.33597364E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35912727E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37279194E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.82427446E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20569510E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.03054907E-07    2          24        25   # BR(H+ -> W+      h      )
     5.41881438E-14    2          24        36   # BR(H+ -> W+      A      )
     5.37939593E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.99794794E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08231100E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
