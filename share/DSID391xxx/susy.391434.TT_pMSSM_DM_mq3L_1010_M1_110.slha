#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14033200E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04150735E+01   # W+
        25     1.25562970E+02   # h
        35     4.00000150E+03   # H
        36     3.99999683E+03   # A
        37     4.00108282E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02862782E+03   # ~d_L
   2000001     4.02469509E+03   # ~d_R
   1000002     4.02797354E+03   # ~u_L
   2000002     4.02544037E+03   # ~u_R
   1000003     4.02862782E+03   # ~s_L
   2000003     4.02469509E+03   # ~s_R
   1000004     4.02797354E+03   # ~c_L
   2000004     4.02544037E+03   # ~c_R
   1000005     1.07282825E+03   # ~b_1
   2000005     4.02682491E+03   # ~b_2
   1000006     1.05464171E+03   # ~t_1
   2000006     1.97833723E+03   # ~t_2
   1000011     4.00514934E+03   # ~e_L
   2000011     4.00352315E+03   # ~e_R
   1000012     4.00403168E+03   # ~nu_eL
   1000013     4.00514934E+03   # ~mu_L
   2000013     4.00352315E+03   # ~mu_R
   1000014     4.00403168E+03   # ~nu_muL
   1000015     4.00554654E+03   # ~tau_1
   2000015     4.00675205E+03   # ~tau_2
   1000016     4.00523947E+03   # ~nu_tauL
   1000021     1.98412829E+03   # ~g
   1000022     9.30655704E+01   # ~chi_10
   1000023    -1.37317262E+02   # ~chi_20
   1000025     1.53049977E+02   # ~chi_30
   1000035     2.05152941E+03   # ~chi_40
   1000024     1.32148028E+02   # ~chi_1+
   1000037     2.05169480E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.62482550E-01   # N_11
  1  2     1.37653538E-02   # N_12
  1  3     5.33366027E-01   # N_13
  1  4     3.65993930E-01   # N_14
  2  1    -1.35770516E-01   # N_21
  2  2     2.72082366E-02   # N_22
  2  3    -6.85365755E-01   # N_23
  2  4     7.14912485E-01   # N_24
  3  1     6.32602496E-01   # N_31
  3  2     2.38523851E-02   # N_32
  3  3     4.95776353E-01   # N_33
  3  4     5.94517412E-01   # N_34
  4  1    -8.99834489E-04   # N_41
  4  2     9.99250364E-01   # N_42
  4  3    -5.20216951E-04   # N_43
  4  4    -3.86992125E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.39987823E-04   # U_11
  1  2     9.99999726E-01   # U_12
  2  1    -9.99999726E-01   # U_21
  2  2     7.39987823E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47422067E-02   # V_11
  1  2    -9.98500521E-01   # V_12
  2  1    -9.98500521E-01   # V_21
  2  2    -5.47422067E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90820852E-01   # cos(theta_t)
  1  2    -1.35181505E-01   # sin(theta_t)
  2  1     1.35181505E-01   # -sin(theta_t)
  2  2     9.90820852E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999917E-01   # cos(theta_b)
  1  2    -4.07430967E-04   # sin(theta_b)
  2  1     4.07430967E-04   # -sin(theta_b)
  2  2     9.99999917E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03519249E-01   # cos(theta_tau)
  1  2     7.10676204E-01   # sin(theta_tau)
  2  1    -7.10676204E-01   # -sin(theta_tau)
  2  2    -7.03519249E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00324529E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40331995E+03  # DRbar Higgs Parameters
         1    -1.26990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43392868E+02   # higgs               
         4     1.60530371E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40331995E+03  # The gauge couplings
     1     3.62384260E-01   # gprime(Q) DRbar
     2     6.37353097E-01   # g(Q) DRbar
     3     1.02833885E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40331995E+03  # The trilinear couplings
  1  1     1.60790973E-06   # A_u(Q) DRbar
  2  2     1.60792509E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40331995E+03  # The trilinear couplings
  1  1     5.90010024E-07   # A_d(Q) DRbar
  2  2     5.90064936E-07   # A_s(Q) DRbar
  3  3     1.05457315E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40331995E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.25609685E-07   # A_mu(Q) DRbar
  3  3     1.26906930E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40331995E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65488093E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40331995E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.78997531E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40331995E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04290296E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40331995E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58787216E+07   # M^2_Hd              
        22     3.04915334E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40940200E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.31912642E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40315052E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40315052E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59684948E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59684948E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.60262609E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09307535E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.44008878E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.44853060E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.01830527E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14886415E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.92549143E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27868424E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.47196818E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.44135336E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.50209989E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.09383579E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.14428075E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.63629681E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.42244968E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.54474624E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.55469181E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.94781123E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65848203E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.81143432E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71370043E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42559882E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.95730502E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58522239E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.73219514E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14574832E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.61218175E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.83250055E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.08432619E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.41408534E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78347404E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.50108579E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.91164541E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.81163636E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.84150899E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25813598E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.67070756E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51039108E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60551980E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.25720799E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03164458E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.23853398E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.50355362E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44010911E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78367385E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.19992404E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45859508E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.08762256E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84616036E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.60859509E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.70246305E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51259124E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53745603E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.50190625E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.69277926E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.84297816E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.53306122E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85385831E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78347404E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.50108579E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.91164541E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.81163636E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.84150899E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25813598E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.67070756E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51039108E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60551980E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.25720799E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03164458E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.23853398E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.50355362E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44010911E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78367385E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.19992404E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45859508E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.08762256E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84616036E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.60859509E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.70246305E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51259124E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53745603E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.50190625E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.69277926E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.84297816E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.53306122E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85385831E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16548084E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.99874489E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27451474E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.49862032E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77470963E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.58691960E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56280312E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08805045E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81822453E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84242005E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.99752905E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41111842E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16548084E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.99874489E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27451474E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.49862032E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77470963E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.58691960E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56280312E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08805045E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81822453E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84242005E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.99752905E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41111842E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.12244449E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.28022994E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00049533E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.59804836E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39191273E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.39228554E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79053088E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12992840E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.12580458E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.32882762E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35393326E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41935130E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22470831E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84555727E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16652984E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02115962E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.55633198E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.74573114E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77759784E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.05564039E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54054970E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16652984E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02115962E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.55633198E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.74573114E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77759784E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.05564039E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54054970E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.50044250E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.24028860E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.02782812E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.19921391E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51447551E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75607832E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01568812E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.55739119E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33711056E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33711056E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11237787E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11237787E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10102314E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.75694950E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.39143473E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.20683774E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.21812459E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.61362900E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.05271753E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.50850380E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.21684105E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.51150855E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.63613506E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.71786389E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18422981E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53595041E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18422981E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53595041E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37681474E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52093622E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52093622E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47823432E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03983857E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03983857E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03983834E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.94800949E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.94800949E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.94800949E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.94800949E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.82672258E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.82672258E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.82672258E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.82672258E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.58464138E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.58464138E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.71777036E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03119250E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.26720664E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.20948355E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.73858378E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.20948355E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.73858378E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.31319867E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.53037897E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.53037897E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.51563051E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.09555680E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.09555680E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.09555322E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.95731415E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.72736527E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.95731415E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.72736527E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.00161844E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.77188443E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.77188443E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.64649130E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.54134093E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.54134093E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.54134100E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.41039116E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.41039116E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.41039116E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.41039116E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.80341727E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.80341727E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.80341727E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.80341727E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.70499354E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.70499354E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.75587732E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.11926045E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.51906095E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.97352097E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.39915178E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.39915178E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.75348090E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.14830310E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.36395817E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.65000961E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.65000961E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.65510262E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.65510262E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06134518E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95601189E-01    2           5        -5   # BR(h -> b       bb     )
     6.41800540E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27196836E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81801771E-04    2           3        -3   # BR(h -> s       sb     )
     2.09205563E-02    2           4        -4   # BR(h -> c       cb     )
     6.83617582E-02    2          21        21   # BR(h -> g       g      )
     2.36027402E-03    2          22        22   # BR(h -> gam     gam    )
     1.61837305E-03    2          22        23   # BR(h -> Z       gam    )
     2.18609151E-01    2          24       -24   # BR(h -> W+      W-     )
     2.76396460E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43139216E+01   # H decays
#          BR         NDA      ID1       ID2
     3.36460155E-01    2           5        -5   # BR(H -> b       bb     )
     6.10450076E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15840361E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55301977E-04    2           3        -3   # BR(H -> s       sb     )
     7.16236041E-08    2           4        -4   # BR(H -> c       cb     )
     7.17817623E-03    2           6        -6   # BR(H -> t       tb     )
     1.32007830E-06    2          21        21   # BR(H -> g       g      )
     6.79728904E-10    2          22        22   # BR(H -> gam     gam    )
     1.84012174E-09    2          23        22   # BR(H -> Z       gam    )
     2.11562846E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05708123E-06    2          23        23   # BR(H -> Z       Z      )
     7.82897517E-06    2          25        25   # BR(H -> h       h      )
     5.17548105E-24    2          36        36   # BR(H -> A       A      )
     4.79112266E-21    2          23        36   # BR(H -> Z       A      )
     1.85894349E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67769928E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67769928E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35980214E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60195380E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68717972E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48833951E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.15519130E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95550390E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04827688E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.45574869E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.41403270E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.78584250E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.89241646E-06    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.06994125E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.06994125E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.24202724E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43126794E+01   # A decays
#          BR         NDA      ID1       ID2
     3.36493818E-01    2           5        -5   # BR(A -> b       bb     )
     6.10468331E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15846646E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55351010E-04    2           3        -3   # BR(A -> s       sb     )
     7.20718028E-08    2           4        -4   # BR(A -> c       cb     )
     7.19045567E-03    2           6        -6   # BR(A -> t       tb     )
     1.47745668E-05    2          21        21   # BR(A -> g       g      )
     4.07035212E-08    2          22        22   # BR(A -> gam     gam    )
     1.62352154E-08    2          23        22   # BR(A -> Z       gam    )
     2.11123808E-06    2          23        25   # BR(A -> Z       h      )
     1.86211188E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67775899E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67775899E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93494037E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22196670E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33465365E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97777663E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.69973146E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.63691027E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30889260E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.33987613E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.84397409E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.29662397E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.29662397E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41761718E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.35698523E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12172685E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16449264E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.42846757E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22622346E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52273597E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.41661630E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.11902378E-06    2          24        25   # BR(H+ -> W+      h      )
     3.66906798E-14    2          24        36   # BR(H+ -> W+      A      )
     4.86207621E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.29110238E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07319459E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.68420444E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.62715175E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58527760E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.28807281E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.02562737E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     6.34590490E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
