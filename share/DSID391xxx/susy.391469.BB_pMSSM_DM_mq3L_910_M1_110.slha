#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13017533E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04166914E+01   # W+
        25     1.26007878E+02   # h
        35     4.00000072E+03   # H
        36     3.99999648E+03   # A
        37     4.00107090E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02595797E+03   # ~d_L
   2000001     4.02259346E+03   # ~d_R
   1000002     4.02530540E+03   # ~u_L
   2000002     4.02338317E+03   # ~u_R
   1000003     4.02595797E+03   # ~s_L
   2000003     4.02259346E+03   # ~s_R
   1000004     4.02530540E+03   # ~c_L
   2000004     4.02338317E+03   # ~c_R
   1000005     9.67408823E+02   # ~b_1
   2000005     4.02536754E+03   # ~b_2
   1000006     9.45258991E+02   # ~t_1
   2000006     1.90073047E+03   # ~t_2
   1000011     4.00458941E+03   # ~e_L
   2000011     4.00347214E+03   # ~e_R
   1000012     4.00347421E+03   # ~nu_eL
   1000013     4.00458941E+03   # ~mu_L
   2000013     4.00347214E+03   # ~mu_R
   1000014     4.00347421E+03   # ~nu_muL
   1000015     4.00561104E+03   # ~tau_1
   2000015     4.00678779E+03   # ~tau_2
   1000016     4.00492109E+03   # ~nu_tauL
   1000021     1.97813871E+03   # ~g
   1000022     9.31233149E+01   # ~chi_10
   1000023    -1.37088430E+02   # ~chi_20
   1000025     1.52997581E+02   # ~chi_30
   1000035     2.05225034E+03   # ~chi_40
   1000024     1.31981423E+02   # ~chi_1+
   1000037     2.05241507E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.62021206E-01   # N_11
  1  2     1.37852249E-02   # N_12
  1  3     5.33741641E-01   # N_13
  1  4     3.66406209E-01   # N_14
  2  1    -1.35788793E-01   # N_21
  2  2     2.72178315E-02   # N_22
  2  3    -6.85352047E-01   # N_23
  2  4     7.14921790E-01   # N_24
  3  1     6.33154227E-01   # N_31
  3  2     2.38483997E-02   # N_32
  3  3     4.95390920E-01   # N_33
  3  4     5.94251474E-01   # N_34
  4  1    -8.99879859E-04   # N_41
  4  2     9.99249924E-01   # N_42
  4  3    -5.18621700E-04   # N_43
  4  4    -3.87105954E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.37735406E-04   # U_11
  1  2     9.99999728E-01   # U_12
  2  1    -9.99999728E-01   # U_21
  2  2     7.37735406E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47583464E-02   # V_11
  1  2    -9.98499636E-01   # V_12
  2  1    -9.98499636E-01   # V_21
  2  2    -5.47583464E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89989128E-01   # cos(theta_t)
  1  2    -1.41143638E-01   # sin(theta_t)
  2  1     1.41143638E-01   # -sin(theta_t)
  2  2     9.89989128E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999919E-01   # cos(theta_b)
  1  2    -4.02492228E-04   # sin(theta_b)
  2  1     4.02492228E-04   # -sin(theta_b)
  2  2     9.99999919E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03390031E-01   # cos(theta_tau)
  1  2     7.10804097E-01   # sin(theta_tau)
  2  1    -7.10804097E-01   # -sin(theta_tau)
  2  2    -7.03390031E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00329072E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30175330E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43499653E+02   # higgs               
         4     1.60676307E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30175330E+03  # The gauge couplings
     1     3.62136608E-01   # gprime(Q) DRbar
     2     6.37264200E-01   # g(Q) DRbar
     3     1.03008549E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30175330E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37247614E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30175330E+03  # The trilinear couplings
  1  1     5.02210205E-07   # A_d(Q) DRbar
  2  2     5.02257125E-07   # A_s(Q) DRbar
  3  3     8.99759647E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30175330E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.07402712E-07   # A_mu(Q) DRbar
  3  3     1.08507809E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30175330E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68644479E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30175330E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.79858212E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30175330E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04279848E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30175330E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58738683E+07   # M^2_Hd              
        22     2.42138216E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40893786E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.96204469E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40107608E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40107608E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59892392E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59892392E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.41967513E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09684421E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.41506780E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.44857539E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.03951260E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20816381E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.46097282E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.17681516E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.66495868E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.24351738E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.59018261E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.21539128E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.36150042E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.46187630E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.46731904E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.58284122E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.56684139E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.93829984E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65948374E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.80975791E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71313145E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42362002E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.92296996E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58415979E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.66619643E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14626708E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.65040122E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.85372428E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.11892002E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.94420860E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78286953E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.49579182E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96279975E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.81128152E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83132835E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25764408E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65036370E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51350045E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60591354E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.24610056E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02965594E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.23750280E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49527034E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44134286E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78306544E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.19435814E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45683781E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.08936768E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83597796E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.54368532E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68208889E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51570257E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53794531E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.47219916E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.68736179E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.83979378E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.51079243E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85419264E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78286953E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.49579182E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96279975E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.81128152E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83132835E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25764408E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65036370E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51350045E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60591354E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.24610056E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02965594E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.23750280E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49527034E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44134286E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78306544E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.19435814E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45683781E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.08936768E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83597796E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.54368532E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68208889E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51570257E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53794531E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.47219916E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.68736179E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.83979378E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.51079243E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85419264E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16174356E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.98338746E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27339434E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50880651E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77488412E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.55722294E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56315698E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08516848E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81118483E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84293263E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00451749E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.40928008E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16174356E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.98338746E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27339434E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50880651E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77488412E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.55722294E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56315698E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08516848E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81118483E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84293263E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00451749E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.40928008E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11952440E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27766556E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00373272E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60035436E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39167498E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.39875457E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79005637E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12724945E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.12159298E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33281264E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35557712E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42000048E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22687185E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84686098E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16280192E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01975047E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.55825052E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75427850E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77776281E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.05978125E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54087855E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16280192E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01975047E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.55825052E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75427850E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77776281E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.05978125E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54087855E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49706840E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22611894E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.02879171E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20614895E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51445337E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76388079E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01564384E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.46014066E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33715475E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33715475E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11239262E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11239262E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10090525E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.18986425E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.52830241E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.42220907E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.36061845E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.90408722E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88859652E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.80971289E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.70112271E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.80184990E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.49302603E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.71926556E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18459012E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53628749E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18459012E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53628749E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37554760E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52096682E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52096682E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47786779E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03963524E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03963524E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03963496E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.86287441E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.86287441E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.86287441E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.86287441E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.54293826E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.54293826E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.54293826E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.54293826E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.34814002E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.34814002E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.78300426E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.88814253E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.37586283E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.08094126E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.57167272E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.08094126E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.57167272E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.15245685E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.49210290E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.49210290E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.47764655E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.01816616E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.01816616E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.01816195E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.23567288E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.08774472E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.23567288E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.08774472E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.22560244E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.85412803E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.85412803E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.72574988E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.70557727E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.70557727E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.70557736E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.56328001E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.56328001E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.56328001E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.56328001E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.85437928E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.85437928E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.85437928E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.85437928E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.75522049E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.75522049E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.18868258E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02949273E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.14664431E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.72633022E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.70906583E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.70906583E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.96292471E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.80361411E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.08064358E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.72232288E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.72232288E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.72261966E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.72261966E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.12113692E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88548189E-01    2           5        -5   # BR(h -> b       bb     )
     6.34746942E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.24697970E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.76174720E-04    2           3        -3   # BR(h -> s       sb     )
     2.06758174E-02    2           4        -4   # BR(h -> c       cb     )
     6.78128591E-02    2          21        21   # BR(h -> g       g      )
     2.36367975E-03    2          22        22   # BR(h -> gam     gam    )
     1.65850294E-03    2          22        23   # BR(h -> Z       gam    )
     2.25994065E-01    2          24       -24   # BR(h -> W+      W-     )
     2.87713194E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43827414E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37536326E-01    2           5        -5   # BR(H -> b       bb     )
     6.09677422E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15567169E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54978843E-04    2           3        -3   # BR(H -> s       sb     )
     7.15342522E-08    2           4        -4   # BR(H -> c       cb     )
     7.16922130E-03    2           6        -6   # BR(H -> t       tb     )
     1.30544371E-06    2          21        21   # BR(H -> g       g      )
     6.70896543E-10    2          22        22   # BR(H -> gam     gam    )
     1.83818804E-09    2          23        22   # BR(H -> Z       gam    )
     2.13895282E-06    2          24       -24   # BR(H -> W+      W-     )
     1.06873543E-06    2          23        23   # BR(H -> Z       Z      )
     7.91905407E-06    2          25        25   # BR(H -> h       h      )
    -1.60342534E-24    2          36        36   # BR(H -> A       A      )
     6.56193998E-20    2          23        36   # BR(H -> Z       A      )
     1.85778272E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67481846E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67481846E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35629174E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.59937695E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68544137E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48347902E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.97516711E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95386555E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04795577E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.44226063E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.39968185E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.82489150E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.25973146E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.27723275E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.27723275E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.28605932E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43817317E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37568631E-01    2           5        -5   # BR(A -> b       bb     )
     6.09693123E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15572551E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55026753E-04    2           3        -3   # BR(A -> s       sb     )
     7.19802826E-08    2           4        -4   # BR(A -> c       cb     )
     7.18132489E-03    2           6        -6   # BR(A -> t       tb     )
     1.47558070E-05    2          21        21   # BR(A -> g       g      )
     4.06444395E-08    2          22        22   # BR(A -> gam     gam    )
     1.62191691E-08    2          23        22   # BR(A -> Z       gam    )
     2.13446084E-06    2          23        25   # BR(A -> Z       h      )
     1.86093761E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67487133E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67487133E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93169774E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.21873719E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33316015E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97164286E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.58258053E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.63542710E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30875912E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.32567162E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.83122200E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.55407864E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.55407864E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42470686E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37476624E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11370801E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16165737E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43984741E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22461769E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51943239E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42762853E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.14223379E-06    2          24        25   # BR(H+ -> W+      h      )
     3.47315408E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84842808E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.28204065E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07396217E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.68123273E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.62358514E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58242898E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.26049459E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.04652536E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.11293657E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
