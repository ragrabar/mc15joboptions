#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14468504E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03986685E+01   # W+
        25     1.25678095E+02   # h
        35     3.00009704E+03   # H
        36     2.99999995E+03   # A
        37     3.00110631E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03337483E+03   # ~d_L
   2000001     3.02798887E+03   # ~d_R
   1000002     3.03245113E+03   # ~u_L
   2000002     3.03011556E+03   # ~u_R
   1000003     3.03337483E+03   # ~s_L
   2000003     3.02798887E+03   # ~s_R
   1000004     3.03245113E+03   # ~c_L
   2000004     3.03011556E+03   # ~c_R
   1000005     7.90065859E+02   # ~b_1
   2000005     3.02701660E+03   # ~b_2
   1000006     7.87550565E+02   # ~t_1
   2000006     3.02224285E+03   # ~t_2
   1000011     3.00691275E+03   # ~e_L
   2000011     3.00102328E+03   # ~e_R
   1000012     3.00550730E+03   # ~nu_eL
   1000013     3.00691275E+03   # ~mu_L
   2000013     3.00102328E+03   # ~mu_R
   1000014     3.00550730E+03   # ~nu_muL
   1000015     2.98609975E+03   # ~tau_1
   2000015     3.02215534E+03   # ~tau_2
   1000016     3.00563667E+03   # ~nu_tauL
   1000021     2.34108083E+03   # ~g
   1000022     5.06062673E+01   # ~chi_10
   1000023     1.10483544E+02   # ~chi_20
   1000025    -3.00034319E+03   # ~chi_30
   1000035     3.00051214E+03   # ~chi_40
   1000024     1.10634985E+02   # ~chi_1+
   1000037     3.00137322E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890683E-01   # N_11
  1  2     1.91349936E-03   # N_12
  1  3    -1.46533704E-02   # N_13
  1  4     4.89077884E-04   # N_14
  2  1    -1.53072461E-03   # N_21
  2  2     9.99658741E-01   # N_22
  2  3     2.60742973E-02   # N_23
  2  4    -4.34983468E-04   # N_24
  3  1     1.00468104E-02   # N_31
  3  2    -1.81123309E-02   # N_32
  3  3     7.06792331E-01   # N_33
  3  4     7.07117816E-01   # N_34
  4  1    -1.07396660E-02   # N_41
  4  2     1.87265399E-02   # N_42
  4  3    -7.06788519E-01   # N_43
  4  4     7.07095443E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321534E-01   # U_11
  1  2     3.68303226E-02   # U_12
  2  1    -3.68303226E-02   # U_21
  2  2     9.99321534E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999810E-01   # V_11
  1  2     6.15674299E-04   # V_12
  2  1     6.15674299E-04   # V_21
  2  2    -9.99999810E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98579302E-01   # cos(theta_t)
  1  2    -5.32858107E-02   # sin(theta_t)
  2  1     5.32858107E-02   # -sin(theta_t)
  2  2     9.98579302E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99707081E-01   # cos(theta_b)
  1  2    -2.42023180E-02   # sin(theta_b)
  2  1     2.42023180E-02   # -sin(theta_b)
  2  2     9.99707081E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06877227E-01   # cos(theta_tau)
  1  2     7.07336261E-01   # sin(theta_tau)
  2  1    -7.07336261E-01   # -sin(theta_tau)
  2  2    -7.06877227E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00168210E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.44685039E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44174419E+02   # higgs               
         4     1.06819315E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.44685039E+03  # The gauge couplings
     1     3.61988711E-01   # gprime(Q) DRbar
     2     6.41535321E-01   # g(Q) DRbar
     3     1.02716071E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.44685039E+03  # The trilinear couplings
  1  1     1.98347157E-06   # A_u(Q) DRbar
  2  2     1.98350295E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.44685039E+03  # The trilinear couplings
  1  1     6.93800814E-07   # A_d(Q) DRbar
  2  2     6.93886645E-07   # A_s(Q) DRbar
  3  3     1.46107787E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.44685039E+03  # The trilinear couplings
  1  1     3.33197806E-07   # A_e(Q) DRbar
  2  2     3.33214637E-07   # A_mu(Q) DRbar
  3  3     3.38044872E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.44685039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53888093E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.44685039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.98356804E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.44685039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03806426E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.44685039E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.26668671E+04   # M^2_Hd              
        22    -9.08240099E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42868914E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.43224298E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48019097E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48019097E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51980903E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51980903E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.42570605E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.19442870E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.09254354E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.78801359E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18334656E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.58362179E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.39697826E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.82655405E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.52308956E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.90748847E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69180761E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62792211E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.20644379E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.20429472E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.26888848E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.49720683E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.37590432E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.59963304E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.97100345E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -2.44711226E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.81751731E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.72824015E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.95932082E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.24953199E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.61906413E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.09381454E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.59709277E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11677153E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98458027E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65345769E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.25742487E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.36967300E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31105930E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.10335109E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.97563674E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18950588E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59270818E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.72678716E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.19305542E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.12889987E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40728794E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12180577E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.74363637E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65471433E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.62264501E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.85869775E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30527230E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.30980534E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98257395E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67182907E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53986033E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06228190E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.77752755E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.00706732E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54601287E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11677153E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98458027E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65345769E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.25742487E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.36967300E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31105930E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.10335109E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.97563674E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18950588E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59270818E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.72678716E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.19305542E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.12889987E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40728794E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12180577E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.74363637E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65471433E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.62264501E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.85869775E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30527230E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.30980534E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98257395E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67182907E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53986033E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06228190E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.77752755E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.00706732E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54601287E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07104959E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.68510914E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00839617E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.92018823E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.77434929E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02309279E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.11163059E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56343053E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997661E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.33860592E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.07468897E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.33921266E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07104959E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.68510914E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00839617E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.92018823E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.77434929E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02309279E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.11163059E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56343053E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997661E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.33860592E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.07468897E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.33921266E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84607359E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43819472E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18605978E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37574550E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78653316E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51862408E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15893940E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.07412566E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.00868302E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32213437E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.04647249E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07139298E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.54932836E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01713818E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.01490889E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.04807274E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02792894E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.73154066E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07139298E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.54932836E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01713818E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.01490889E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.04807274E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02792894E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.73154066E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07190771E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.54853270E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01688735E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.11667765E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.15507494E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02825488E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.45682588E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.46809984E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39918241E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.73969356E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39918241E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.73969356E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06875143E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.79905141E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06875143E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.79905141E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06413185E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.47219067E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.43206504E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.49634991E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.26886475E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.25085995E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.96522547E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.09273943E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.40697299E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.11155167E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.06595982E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.73096146E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     4.63731018E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.73096146E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     4.63731018E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.07491144E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     4.10692320E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.10692320E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     4.09738556E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.24087844E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.24087844E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.24417358E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.51858866E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.05390973E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.21925484E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.07457518E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.07457518E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.84177902E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.68387680E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29602675E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29602675E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.04448732E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.04448732E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.03716493E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.03716493E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.45948983E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.28174795E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.03187010E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.11259001E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.11259001E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16268018E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.82352739E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.23245743E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.23245743E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.08966120E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.08966120E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.82967202E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.82967202E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.72914221E-03   # h decays
#          BR         NDA      ID1       ID2
     5.55130367E-01    2           5        -5   # BR(h -> b       bb     )
     6.99179694E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47508463E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.24786021E-04    2           3        -3   # BR(h -> s       sb     )
     2.28010714E-02    2           4        -4   # BR(h -> c       cb     )
     7.48225707E-02    2          21        21   # BR(h -> g       g      )
     2.57804620E-03    2          22        22   # BR(h -> gam     gam    )
     1.77947108E-03    2          22        23   # BR(h -> Z       gam    )
     2.41640367E-01    2          24       -24   # BR(h -> W+      W-     )
     3.05325910E-02    2          23        23   # BR(h -> Z       Z      )
     2.52517919E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     3.81697314E+01   # H decays
#          BR         NDA      ID1       ID2
     9.02702144E-01    2           5        -5   # BR(H -> b       bb     )
     6.51504826E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.30356538E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.82878375E-04    2           3        -3   # BR(H -> s       sb     )
     7.92970073E-08    2           4        -4   # BR(H -> c       cb     )
     7.91038516E-03    2           6        -6   # BR(H -> t       tb     )
     7.45332287E-06    2          21        21   # BR(H -> g       g      )
     5.55250172E-08    2          22        22   # BR(H -> gam     gam    )
     3.23635539E-09    2          23        22   # BR(H -> Z       gam    )
     7.89052551E-07    2          24       -24   # BR(H -> W+      W-     )
     3.94039732E-07    2          23        23   # BR(H -> Z       Z      )
     5.29307261E-06    2          25        25   # BR(H -> h       h      )
    -2.50412565E-24    2          36        36   # BR(H -> A       A      )
     2.92758190E-19    2          23        36   # BR(H -> Z       A      )
     8.93525245E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.04005792E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.48277462E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.69444433E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20525530E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.48518998E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.73311894E+01   # A decays
#          BR         NDA      ID1       ID2
     9.22996303E-01    2           5        -5   # BR(A -> b       bb     )
     6.66122352E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.35524616E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.89273136E-04    2           3        -3   # BR(A -> s       sb     )
     8.16287787E-08    2           4        -4   # BR(A -> c       cb     )
     8.13851292E-03    2           6        -6   # BR(A -> t       tb     )
     2.39671417E-05    2          21        21   # BR(A -> g       g      )
     4.91807096E-08    2          22        22   # BR(A -> gam     gam    )
     2.35998486E-08    2          23        22   # BR(A -> Z       gam    )
     8.03680075E-07    2          23        25   # BR(A -> Z       h      )
     9.21644066E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.16311737E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.62375524E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.77575030E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.05846240E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47117215E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.12949140E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16723865E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.41549023E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.27362990E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.62026626E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.25493358E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.40535533E-07    2          24        25   # BR(H+ -> W+      h      )
     5.37388594E-14    2          24        36   # BR(H+ -> W+      A      )
     5.11269575E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.19726030E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07276424E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
