#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13087816E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.69990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04180948E+01   # W+
        25     1.25661381E+02   # h
        35     4.00001452E+03   # H
        36     3.99999777E+03   # A
        37     4.00100776E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02610019E+03   # ~d_L
   2000001     4.02266553E+03   # ~d_R
   1000002     4.02544743E+03   # ~u_L
   2000002     4.02359595E+03   # ~u_R
   1000003     4.02610019E+03   # ~s_L
   2000003     4.02266553E+03   # ~s_R
   1000004     4.02544743E+03   # ~c_L
   2000004     4.02359595E+03   # ~c_R
   1000005     8.83910105E+02   # ~b_1
   2000005     4.02554346E+03   # ~b_2
   1000006     8.69878429E+02   # ~t_1
   2000006     2.14899144E+03   # ~t_2
   1000011     4.00467226E+03   # ~e_L
   2000011     4.00312745E+03   # ~e_R
   1000012     4.00355887E+03   # ~nu_eL
   1000013     4.00467226E+03   # ~mu_L
   2000013     4.00312745E+03   # ~mu_R
   1000014     4.00355887E+03   # ~nu_muL
   1000015     4.00348871E+03   # ~tau_1
   2000015     4.00850313E+03   # ~tau_2
   1000016     4.00495663E+03   # ~nu_tauL
   1000021     1.98059805E+03   # ~g
   1000022     5.43443192E+02   # ~chi_10
   1000023    -5.81981922E+02   # ~chi_20
   1000025     6.02783097E+02   # ~chi_30
   1000035     2.05217247E+03   # ~chi_40
   1000024     5.79920654E+02   # ~chi_1+
   1000037     2.05233688E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.48249292E-01   # N_11
  1  2    -2.25904330E-02   # N_12
  1  3    -4.85035762E-01   # N_13
  1  4    -4.52054178E-01   # N_14
  2  1    -2.89084971E-02   # N_21
  2  2     2.23534512E-02   # N_22
  2  3    -7.05384685E-01   # N_23
  2  4     7.07882101E-01   # N_24
  3  1     6.62786343E-01   # N_31
  3  2     2.83611502E-02   # N_32
  3  3     5.16793607E-01   # N_33
  3  4     5.41141642E-01   # N_34
  4  1    -1.24903648E-03   # N_41
  4  2     9.99092408E-01   # N_42
  4  3    -9.85519138E-03   # N_43
  4  4    -4.14207001E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.39435395E-02   # U_11
  1  2     9.99902784E-01   # U_12
  2  1    -9.99902784E-01   # U_21
  2  2     1.39435395E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.85848950E-02   # V_11
  1  2    -9.98282430E-01   # V_12
  2  1    -9.98282430E-01   # V_21
  2  2    -5.85848950E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94910656E-01   # cos(theta_t)
  1  2    -1.00761037E-01   # sin(theta_t)
  2  1     1.00761037E-01   # -sin(theta_t)
  2  2     9.94910656E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998298E-01   # cos(theta_b)
  1  2    -1.84499244E-03   # sin(theta_b)
  2  1     1.84499244E-03   # -sin(theta_b)
  2  2     9.99998298E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06366406E-01   # cos(theta_tau)
  1  2     7.07846382E-01   # sin(theta_tau)
  2  1    -7.07846382E-01   # -sin(theta_tau)
  2  2    -7.06366406E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00226339E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30878156E+03  # DRbar Higgs Parameters
         1    -5.69990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43740337E+02   # higgs               
         4     1.62190285E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30878156E+03  # The gauge couplings
     1     3.61843153E-01   # gprime(Q) DRbar
     2     6.35653381E-01   # g(Q) DRbar
     3     1.03002245E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30878156E+03  # The trilinear couplings
  1  1     1.38604041E-06   # A_u(Q) DRbar
  2  2     1.38605338E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30878156E+03  # The trilinear couplings
  1  1     5.13593747E-07   # A_d(Q) DRbar
  2  2     5.13640846E-07   # A_s(Q) DRbar
  3  3     9.16210694E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30878156E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.14358012E-07   # A_mu(Q) DRbar
  3  3     1.15517064E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30878156E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66322406E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30878156E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88499357E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30878156E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02647400E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30878156E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55694677E+07   # M^2_Hd              
        22    -3.12696161E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40176282E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.45219085E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43723653E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43723653E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56276347E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56276347E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.42856483E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.54362770E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.96258644E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.21184771E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.28193814E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04979381E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.25934018E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.22162566E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.35426329E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.42150468E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.36096512E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.81736616E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.17192226E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.06094856E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.79105624E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.47965851E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.03718716E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.10995244E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.63732019E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66968182E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.58518992E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81596374E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53924768E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.26359799E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63634287E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.48839447E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12526063E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.60462143E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.97850913E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.35642176E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.78783941E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78720053E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.19985695E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38238563E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.98287677E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79376581E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.74922668E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57546547E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52526207E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60834349E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.01542510E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.47695405E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34613591E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.79306760E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46339572E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78740766E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.27996482E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.48944897E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.91982899E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79962980E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.25695177E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60920487E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52742242E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54280752E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.85680776E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.16648492E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.11292452E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24847281E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86018590E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78720053E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.19985695E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38238563E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.98287677E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79376581E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.74922668E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57546547E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52526207E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60834349E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.01542510E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.47695405E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34613591E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.79306760E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46339572E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78740766E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.27996482E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.48944897E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.91982899E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79962980E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.25695177E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60920487E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52742242E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54280752E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.85680776E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.16648492E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.11292452E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24847281E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86018590E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12822158E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.06537931E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71471311E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.08736778E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78875435E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91768909E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59388178E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00166621E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.62010455E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.34265863E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.37154395E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.83338190E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12822158E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.06537931E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71471311E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.08736778E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78875435E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91768909E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59388178E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00166621E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.62010455E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.34265863E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.37154395E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.83338190E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04344225E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.00883620E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.52624434E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.71871883E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41648231E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.62043603E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84129463E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03147052E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.01454138E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.80871697E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.54942096E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45813869E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.72292823E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92473445E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12926542E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96715400E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.42112262E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.97995815E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79388673E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.38319822E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.57014895E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12926542E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96715400E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.42112262E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.97995815E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79388673E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.38319822E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.57014895E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44527360E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05634231E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.74299715E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.43353523E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53977406E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.40899083E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06359610E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.47790023E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33764152E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33764152E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11256062E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11256062E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09959572E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.39088436E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.65512740E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.50698853E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.21258041E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.65475360E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.02690455E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.37192550E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.11267661E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.69793435E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.03528021E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18917214E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54270656E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18917214E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54270656E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34688413E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53854811E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53854811E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48984145E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.07430307E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.07430307E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.07430297E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.93494979E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.93494979E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.93494979E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.93494979E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.31165080E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.31165080E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.31165080E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.31165080E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.06446770E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.06446770E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.14473106E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.59688658E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.33285467E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19798814E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.51944505E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.19798814E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.51944505E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.50489137E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.22269286E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.22269286E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.27466939E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.10311426E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.10311426E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.10303445E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.17472658E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.82131620E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.17472658E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.82131620E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.73373156E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.47159521E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.47159521E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.20800654E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.29369719E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.29369719E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.29369720E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.19630924E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.19630924E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.19630924E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.19630924E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.98764395E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.98764395E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.98764395E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.98764395E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.87050801E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.87050801E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.38926007E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.05165683E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.22935081E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.17940064E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.02927134E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.02927134E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.88926267E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.95668923E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24905531E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78569699E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78569699E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79592966E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79592966E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.00876351E-03   # h decays
#          BR         NDA      ID1       ID2
     5.87459839E-01    2           5        -5   # BR(h -> b       bb     )
     6.50474496E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30266976E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88240482E-04    2           3        -3   # BR(h -> s       sb     )
     2.12083564E-02    2           4        -4   # BR(h -> c       cb     )
     6.94151833E-02    2          21        21   # BR(h -> g       g      )
     2.40310342E-03    2          22        22   # BR(h -> gam     gam    )
     1.65406042E-03    2          22        23   # BR(h -> Z       gam    )
     2.23749103E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83443983E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56516012E+01   # H decays
#          BR         NDA      ID1       ID2
     3.71619393E-01    2           5        -5   # BR(H -> b       bb     )
     5.95779409E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10653169E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49166320E-04    2           3        -3   # BR(H -> s       sb     )
     6.98747977E-08    2           4        -4   # BR(H -> c       cb     )
     7.00290968E-03    2           6        -6   # BR(H -> t       tb     )
     8.76050691E-07    2          21        21   # BR(H -> g       g      )
     5.11064520E-09    2          22        22   # BR(H -> gam     gam    )
     1.80413752E-09    2          23        22   # BR(H -> Z       gam    )
     1.55356410E-06    2          24       -24   # BR(H -> W+      W-     )
     7.76243931E-07    2          23        23   # BR(H -> Z       Z      )
     6.46333237E-06    2          25        25   # BR(H -> h       h      )
     3.52196805E-25    2          36        36   # BR(H -> A       A      )
     7.39864486E-20    2          23        36   # BR(H -> Z       A      )
     1.87456708E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.49177896E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.49177896E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.49663230E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.41927660E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.65806888E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.03358560E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.10852739E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.13484282E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.65914736E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27501394E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.55960009E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.22964835E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.51253375E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.51253375E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42769787E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56504725E+01   # A decays
#          BR         NDA      ID1       ID2
     3.71652218E-01    2           5        -5   # BR(A -> b       bb     )
     5.95793298E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10657914E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49212626E-04    2           3        -3   # BR(A -> s       sb     )
     7.03392684E-08    2           4        -4   # BR(A -> c       cb     )
     7.01760427E-03    2           6        -6   # BR(A -> t       tb     )
     1.44193967E-05    2          21        21   # BR(A -> g       g      )
     7.17958847E-08    2          22        22   # BR(A -> gam     gam    )
     1.58532713E-08    2          23        22   # BR(A -> Z       gam    )
     1.55031512E-06    2          23        25   # BR(A -> Z       h      )
     1.95163151E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.49129363E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.49129363E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.23672897E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.56738235E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.47869977E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.34884685E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.21414077E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.87578087E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.79199962E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.62112986E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.46403446E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.57570677E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.57570677E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.60051705E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.00757833E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.92169440E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09376606E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.84484691E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18615839E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44030925E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.82062630E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.54220308E-06    2          24        25   # BR(H+ -> W+      h      )
     2.46940096E-14    2          24        36   # BR(H+ -> W+      A      )
     4.16808282E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.20587530E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.82473723E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.48430405E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.98455495E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.47968968E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.99628175E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.27825297E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.07646584E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
