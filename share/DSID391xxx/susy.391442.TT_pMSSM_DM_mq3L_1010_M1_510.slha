#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13998409E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.21990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181097E+01   # W+
        25     1.25653800E+02   # h
        35     4.00000812E+03   # H
        36     3.99999750E+03   # A
        37     4.00105615E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02852068E+03   # ~d_L
   2000001     4.02462256E+03   # ~d_R
   1000002     4.02786982E+03   # ~u_L
   2000002     4.02529866E+03   # ~u_R
   1000003     4.02852068E+03   # ~s_L
   2000003     4.02462256E+03   # ~s_R
   1000004     4.02786982E+03   # ~c_L
   2000004     4.02529866E+03   # ~c_R
   1000005     1.12241050E+03   # ~b_1
   2000005     4.02683549E+03   # ~b_2
   1000006     1.09949094E+03   # ~t_1
   2000006     1.88214791E+03   # ~t_2
   1000011     4.00506505E+03   # ~e_L
   2000011     4.00344165E+03   # ~e_R
   1000012     4.00395294E+03   # ~nu_eL
   1000013     4.00506505E+03   # ~mu_L
   2000013     4.00344165E+03   # ~mu_R
   1000014     4.00395294E+03   # ~nu_muL
   1000015     4.00372455E+03   # ~tau_1
   2000015     4.00834036E+03   # ~tau_2
   1000016     4.00513847E+03   # ~nu_tauL
   1000021     1.98338838E+03   # ~g
   1000022     4.93302622E+02   # ~chi_10
   1000023    -5.34352029E+02   # ~chi_20
   1000025     5.53996702E+02   # ~chi_30
   1000035     2.05149593E+03   # ~chi_40
   1000024     5.32224790E+02   # ~chi_1+
   1000037     2.05165975E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.58974249E-01   # N_11
  1  2    -2.13242552E-02   # N_12
  1  3    -4.78073807E-01   # N_13
  1  4    -4.41530068E-01   # N_14
  2  1    -3.16460510E-02   # N_21
  2  2     2.27652964E-02   # N_22
  2  3    -7.05136074E-01   # N_23
  2  4     7.07999566E-01   # N_24
  3  1     6.50350056E-01   # N_31
  3  2     2.78255087E-02   # N_32
  3  3     5.23595773E-01   # N_33
  3  4     5.49652629E-01   # N_34
  4  1    -1.19237094E-03   # N_41
  4  2     9.99125997E-01   # N_42
  4  3    -8.71887519E-03   # N_43
  4  4    -4.08631983E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.23364820E-02   # U_11
  1  2     9.99923903E-01   # U_12
  2  1    -9.99923903E-01   # U_21
  2  2     1.23364820E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.77973569E-02   # V_11
  1  2    -9.98328336E-01   # V_12
  2  1    -9.98328336E-01   # V_21
  2  2    -5.77973569E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86742741E-01   # cos(theta_t)
  1  2    -1.62292215E-01   # sin(theta_t)
  2  1     1.62292215E-01   # -sin(theta_t)
  2  2     9.86742741E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998496E-01   # cos(theta_b)
  1  2    -1.73435802E-03   # sin(theta_b)
  2  1     1.73435802E-03   # -sin(theta_b)
  2  2     9.99998496E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06332838E-01   # cos(theta_tau)
  1  2     7.07879878E-01   # sin(theta_tau)
  2  1    -7.07879878E-01   # -sin(theta_tau)
  2  2    -7.06332838E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00239468E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39984087E+03  # DRbar Higgs Parameters
         1    -5.21990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43545265E+02   # higgs               
         4     1.60898294E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39984087E+03  # The gauge couplings
     1     3.62101000E-01   # gprime(Q) DRbar
     2     6.35731713E-01   # g(Q) DRbar
     3     1.02835621E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39984087E+03  # The trilinear couplings
  1  1     1.59783519E-06   # A_u(Q) DRbar
  2  2     1.59785044E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39984087E+03  # The trilinear couplings
  1  1     5.91838756E-07   # A_d(Q) DRbar
  2  2     5.91892789E-07   # A_s(Q) DRbar
  3  3     1.05515635E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39984087E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.31285256E-07   # A_mu(Q) DRbar
  3  3     1.32629565E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39984087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65290800E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39984087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87377699E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39984087E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02874022E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39984087E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56250341E+07   # M^2_Hd              
        22    -2.19177428E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40210767E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.01607602E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37545919E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37545919E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62454081E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62454081E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.87198487E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.81727602E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.15452713E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.89381159E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13438526E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01943416E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.83804679E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.14553093E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.60620456E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.23354785E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62238045E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.22546330E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.32865233E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.03706565E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.35738892E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.11984280E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.90281362E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86199547E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65857824E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60198452E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83477710E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56368976E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.65077683E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.67643973E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.26841696E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11845045E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.92621323E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.03639375E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.71385824E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.04778611E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77745512E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.28526519E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38189105E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.93676073E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83129590E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.66889175E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65047069E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51369600E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59813071E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.14850235E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.44558233E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.29381057E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.40912666E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45522371E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77765726E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.33342353E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.02340317E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.74088146E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83700120E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.58280575E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68399520E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51586462E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53225065E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.20820016E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.41967025E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.97998802E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14921119E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85797604E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77745512E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.28526519E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38189105E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.93676073E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83129590E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.66889175E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65047069E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51369600E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59813071E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.14850235E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.44558233E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.29381057E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.40912666E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45522371E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77765726E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.33342353E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.02340317E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.74088146E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83700120E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.58280575E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68399520E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51586462E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53225065E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.20820016E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.41967025E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.97998802E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14921119E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85797604E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13492616E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.41355357E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.11337047E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.83732236E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78581013E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.50857595E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58748236E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01874980E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.78010437E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99530752E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.20989232E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.99860337E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13492616E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.41355357E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.11337047E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.83732236E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78581013E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.50857595E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58748236E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01874980E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.78010437E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99530752E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.20989232E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.99860337E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05848583E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.07900992E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51540782E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66576654E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41222097E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.58964953E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83249684E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04797055E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.08351047E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86188972E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.49318112E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44998879E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.78978620E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.90815202E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13597388E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02439374E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.23889145E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.79634388E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79058306E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.30922328E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56405768E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13597388E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02439374E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.23889145E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.79634388E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79058306E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.30922328E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56405768E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45446517E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.30240046E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.48166560E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.26361804E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53511777E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.46054568E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05474414E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.65798538E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33711270E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33711270E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11238460E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11238460E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10100541E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.46284450E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.38023960E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.23186625E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.17792974E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.73825838E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.06311724E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.51206698E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.44801985E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.64452548E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.89329435E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.71030021E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18692153E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53992461E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18692153E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53992461E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36405647E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53297731E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53297731E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48889065E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06345951E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06345951E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06345942E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.48037793E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.48037793E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.48037793E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.48037793E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.16012679E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.16012679E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.16012679E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.16012679E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.12436369E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.12436369E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.38696500E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.93549962E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.65273984E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.63540844E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.36015881E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.63540844E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.36015881E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.21631508E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     7.25710908E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     7.25710908E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.33414519E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.56428450E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.56428450E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.56427304E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.04176896E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.64906945E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.04176896E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.64906945E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.51938269E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.07792194E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.07792194E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.80171617E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.21504121E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.21504121E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.21504122E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.16855664E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.16855664E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.16855664E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.16855664E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.89513738E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.89513738E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.89513738E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.89513738E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.76924675E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.76924675E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.46084516E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.00083832E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.15442218E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.58710429E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.51481621E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.51481621E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.11545230E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.55517226E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.95127294E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.63994328E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.63994328E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.64036198E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.64036198E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01917631E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88833279E-01    2           5        -5   # BR(h -> b       bb     )
     6.48784051E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29668594E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86977102E-04    2           3        -3   # BR(h -> s       sb     )
     2.11523801E-02    2           4        -4   # BR(h -> c       cb     )
     6.91435655E-02    2          21        21   # BR(h -> g       g      )
     2.39648869E-03    2          22        22   # BR(h -> gam     gam    )
     1.64867551E-03    2          22        23   # BR(h -> Z       gam    )
     2.22986014E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82445469E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54977020E+01   # H decays
#          BR         NDA      ID1       ID2
     3.66928868E-01    2           5        -5   # BR(H -> b       bb     )
     5.97430514E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11236960E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49856893E-04    2           3        -3   # BR(H -> s       sb     )
     7.00721428E-08    2           4        -4   # BR(H -> c       cb     )
     7.02268764E-03    2           6        -6   # BR(H -> t       tb     )
     5.35158430E-07    2          21        21   # BR(H -> g       g      )
     9.27782333E-09    2          22        22   # BR(H -> gam     gam    )
     1.80822849E-09    2          23        22   # BR(H -> Z       gam    )
     1.62219478E-06    2          24       -24   # BR(H -> W+      W-     )
     8.10535497E-07    2          23        23   # BR(H -> Z       Z      )
     6.63235210E-06    2          25        25   # BR(H -> h       h      )
    -2.01703689E-24    2          36        36   # BR(H -> A       A      )
     2.52126588E-20    2          23        36   # BR(H -> Z       A      )
     1.86498821E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.51911937E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.51911937E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.53430692E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.85695902E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68213453E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.08852938E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.55851162E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.16856808E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63616892E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27817520E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.79957339E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.13100779E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.33231007E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.18313077E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.18313077E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.30165316E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54940754E+01   # A decays
#          BR         NDA      ID1       ID2
     3.66978616E-01    2           5        -5   # BR(A -> b       bb     )
     5.97472362E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11251590E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49914960E-04    2           3        -3   # BR(A -> s       sb     )
     7.05374991E-08    2           4        -4   # BR(A -> c       cb     )
     7.03738134E-03    2           6        -6   # BR(A -> t       tb     )
     1.44600348E-05    2          21        21   # BR(A -> g       g      )
     6.94483945E-08    2          22        22   # BR(A -> gam     gam    )
     1.58979913E-08    2          23        22   # BR(A -> Z       gam    )
     1.61888327E-06    2          23        25   # BR(A -> Z       h      )
     1.92345262E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.51884803E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.51884803E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.24295799E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.07906301E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.47651779E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.44852421E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.28337338E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.77539656E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.78944551E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.87871018E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.58114467E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.31740293E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.31740293E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.57569711E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.91680115E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94812646E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10311179E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.78674954E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19145112E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45119809E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.76423769E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.61309417E-06    2          24        25   # BR(H+ -> W+      h      )
     3.13827401E-14    2          24        36   # BR(H+ -> W+      A      )
     4.37635544E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.39676195E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.77365953E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.51407334E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.92801278E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.50835978E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.36386329E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.09562707E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.51787239E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
