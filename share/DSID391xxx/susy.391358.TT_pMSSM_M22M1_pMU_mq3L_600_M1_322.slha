#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13425185E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.21990000E+02   # M_1(MX)             
         2     6.43990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04039257E+01   # W+
        25     1.24312704E+02   # h
        35     3.00019200E+03   # H
        36     2.99999959E+03   # A
        37     3.00094506E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02894024E+03   # ~d_L
   2000001     3.02394606E+03   # ~d_R
   1000002     3.02802667E+03   # ~u_L
   2000002     3.02561628E+03   # ~u_R
   1000003     3.02894024E+03   # ~s_L
   2000003     3.02394606E+03   # ~s_R
   1000004     3.02802667E+03   # ~c_L
   2000004     3.02561628E+03   # ~c_R
   1000005     6.86874674E+02   # ~b_1
   2000005     3.02329759E+03   # ~b_2
   1000006     6.84639070E+02   # ~t_1
   2000006     3.00913679E+03   # ~t_2
   1000011     3.00634694E+03   # ~e_L
   2000011     3.00143930E+03   # ~e_R
   1000012     3.00495686E+03   # ~nu_eL
   1000013     3.00634694E+03   # ~mu_L
   2000013     3.00143930E+03   # ~mu_R
   1000014     3.00495686E+03   # ~nu_muL
   1000015     2.98561754E+03   # ~tau_1
   2000015     3.02207853E+03   # ~tau_2
   1000016     3.00495391E+03   # ~nu_tauL
   1000021     2.33581538E+03   # ~g
   1000022     3.24622215E+02   # ~chi_10
   1000023     6.74113616E+02   # ~chi_20
   1000025    -3.00023664E+03   # ~chi_30
   1000035     3.00110415E+03   # ~chi_40
   1000024     6.74273525E+02   # ~chi_1+
   1000037     3.00164324E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99884543E-01   # N_11
  1  2    -7.47498066E-04   # N_12
  1  3     1.49946201E-02   # N_13
  1  4    -2.34583642E-03   # N_14
  2  1     1.17564224E-03   # N_21
  2  2     9.99596950E-01   # N_22
  2  3    -2.74410179E-02   # N_23
  2  4     7.17956430E-03   # N_24
  3  1    -8.93078877E-03   # N_31
  3  2     1.43366722E-02   # N_32
  3  3     7.06873813E-01   # N_33
  3  4     7.07137973E-01   # N_34
  4  1    -1.22376146E-02   # N_41
  4  2     2.44916071E-02   # N_42
  4  3     7.06648119E-01   # N_43
  4  4    -7.07035245E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99246217E-01   # U_11
  1  2    -3.88200591E-02   # U_12
  2  1     3.88200591E-02   # U_21
  2  2     9.99246217E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99948406E-01   # V_11
  1  2    -1.01580039E-02   # V_12
  2  1     1.01580039E-02   # V_21
  2  2     9.99948406E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98869998E-01   # cos(theta_t)
  1  2    -4.75260675E-02   # sin(theta_t)
  2  1     4.75260675E-02   # -sin(theta_t)
  2  2     9.98869998E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99917022E-01   # cos(theta_b)
  1  2     1.28821238E-02   # sin(theta_b)
  2  1    -1.28821238E-02   # -sin(theta_b)
  2  2     9.99917022E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06945360E-01   # cos(theta_tau)
  1  2     7.07268166E-01   # sin(theta_tau)
  2  1    -7.07268166E-01   # -sin(theta_tau)
  2  2     7.06945360E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01900424E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.34251846E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44503275E+02   # higgs               
         4     6.90008595E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.34251846E+03  # The gauge couplings
     1     3.61802801E-01   # gprime(Q) DRbar
     2     6.37085329E-01   # g(Q) DRbar
     3     1.02883141E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.34251846E+03  # The trilinear couplings
  1  1     1.69180322E-06   # A_u(Q) DRbar
  2  2     1.69182785E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.34251846E+03  # The trilinear couplings
  1  1     4.55565862E-07   # A_d(Q) DRbar
  2  2     4.55646377E-07   # A_s(Q) DRbar
  3  3     9.94612862E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.34251846E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.30508732E-08   # A_mu(Q) DRbar
  3  3     9.41368912E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.34251846E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54357471E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.34251846E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14847685E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.34251846E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07103564E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.34251846E+03  # The soft SUSY breaking masses at the scale Q
         1     3.21990000E+02   # M_1(Q)              
         2     6.43990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.74730536E+04   # M^2_Hd              
        22    -9.09551480E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40870706E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.83756888E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48248742E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48248742E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51751258E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51751258E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.09448448E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.99593757E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.00406243E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03677632E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.21384695E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.50919308E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.05329477E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.07332377E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.25357883E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.61444540E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51023825E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.98969960E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.72861368E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.40518572E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.94814284E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.24413767E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83587371E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.23466181E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.92461888E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.48877167E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.74457394E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27198023E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.98184427E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.29023503E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.17068327E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.75266857E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99262416E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.56172778E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.07844694E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.11759560E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.12410961E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.25790106E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.25423582E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18064588E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55967811E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99776531E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.17816588E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     7.33164991E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44031978E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.75796624E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08523705E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55969401E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.84275329E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.85687547E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11843682E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.96083400E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.26101417E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67807772E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42935145E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.67298165E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.04047418E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.81688791E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55706426E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.75266857E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99262416E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.56172778E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.07844694E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.11759560E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.12410961E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.25790106E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.25423582E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18064588E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55967811E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99776531E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.17816588E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     7.33164991E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44031978E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.75796624E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08523705E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55969401E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.84275329E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.85687547E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11843682E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.96083400E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.26101417E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67807772E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42935145E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.67298165E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.04047418E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.81688791E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55706426E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.66279337E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04097297E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99049976E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.69053538E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.23828519E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96852716E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     9.76440270E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52655869E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998724E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.27617972E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     5.24339099E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.64793885E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.66279337E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04097297E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99049976E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.69053538E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.23828519E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96852716E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     9.76440270E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52655869E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998724E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.27617972E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     5.24339099E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.64793885E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62105341E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.63730900E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12396869E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.23872232E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.56615473E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.72340726E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09531391E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.01178925E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.14031175E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.18094943E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.14185090E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.66293289E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04592220E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98073606E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.22945863E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.15014229E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97334170E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.32094177E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.66293289E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04592220E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98073606E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.22945863E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.15014229E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97334170E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.32094177E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.66326103E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.04582744E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98046551E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.22781477E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.14666275E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97370406E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.95836937E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.56517039E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.73967516E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.79585198E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.24053931E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.43593899E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.83368209E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.13310402E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.66016136E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.17174305E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.39959502E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.37159457E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.46284054E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.75665458E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.43537833E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.07053239E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.63636241E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.63636241E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.87928038E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.76472760E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.65398158E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.65398158E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29448201E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29448201E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.66336030E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.66336030E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.67841562E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.71523663E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.75681130E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.70014102E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.70014102E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.50090052E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.43436950E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.62468313E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.62468313E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.32114693E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.32114693E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.83144937E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.83144937E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.78430988E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85207305E-01    2           5        -5   # BR(h -> b       bb     )
     5.42779332E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92148068E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08237728E-04    2           3        -3   # BR(h -> s       sb     )
     1.76164907E-02    2           4        -4   # BR(h -> c       cb     )
     5.69134593E-02    2          21        21   # BR(h -> g       g      )
     1.91978437E-03    2          22        22   # BR(h -> gam     gam    )
     1.22769867E-03    2          22        23   # BR(h -> Z       gam    )
     1.62174849E-01    2          24       -24   # BR(h -> W+      W-     )
     2.00620936E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39167093E+01   # H decays
#          BR         NDA      ID1       ID2
     7.43326253E-01    2           5        -5   # BR(H -> b       bb     )
     1.78692526E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31814071E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75865751E-04    2           3        -3   # BR(H -> s       sb     )
     2.19003836E-07    2           4        -4   # BR(H -> c       cb     )
     2.18470527E-02    2           6        -6   # BR(H -> t       tb     )
     3.20026018E-05    2          21        21   # BR(H -> g       g      )
     6.21072103E-08    2          22        22   # BR(H -> gam     gam    )
     8.39172138E-09    2          23        22   # BR(H -> Z       gam    )
     3.40235919E-05    2          24       -24   # BR(H -> W+      W-     )
     1.69908273E-05    2          23        23   # BR(H -> Z       Z      )
     8.66329046E-05    2          25        25   # BR(H -> h       h      )
    -1.24068895E-23    2          36        36   # BR(H -> A       A      )
     2.05654809E-17    2          23        36   # BR(H -> Z       A      )
     1.90887483E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.07842650E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.52105364E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.55312249E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.09261805E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.23282385E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32217154E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82460383E-01    2           5        -5   # BR(A -> b       bb     )
     1.88077992E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.64997904E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.16755528E-04    2           3        -3   # BR(A -> s       sb     )
     2.30476833E-07    2           4        -4   # BR(A -> c       cb     )
     2.29788895E-02    2           6        -6   # BR(A -> t       tb     )
     6.76706402E-05    2          21        21   # BR(A -> g       g      )
     4.84876338E-08    2          22        22   # BR(A -> gam     gam    )
     6.66939827E-08    2          23        22   # BR(A -> Z       gam    )
     3.56752705E-05    2          23        25   # BR(A -> Z       h      )
     2.65330081E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22876902E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32329009E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.97823041E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.99951292E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.09206997E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48761855E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.79561242E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.98923679E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.16898910E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06342727E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11382873E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.72452609E-05    2          24        25   # BR(H+ -> W+      h      )
     9.94138629E-14    2          24        36   # BR(H+ -> W+      A      )
     1.96339497E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.61870511E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.47508922E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
