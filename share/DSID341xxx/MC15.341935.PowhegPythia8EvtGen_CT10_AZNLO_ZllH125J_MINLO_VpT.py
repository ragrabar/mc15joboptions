#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+Z+jet-> llvv + ll production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HZj_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.vdecaymode = 1 # Z->e+e-
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.nEvents *= 1.15/0.36

#PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations
#PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
#PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.generate()

import random, string, shutil

fname_in = "PowhegOTF._1.events"
fname_tmp = "tmp.events"

id_replace = '11'
id_list = ['11', '13', '15']

file_out = open(fname_tmp, 'w')

counter = 1
id_random = id_replace

for line in open(fname_in, 'r') :
  if line.find(id_replace, 0, 8) != -1 and line.find("<", 0, 8) == -1:
    keep = line[8:]
    change = line[:8]
    if counter == 1 :
      id_random = random.choice(id_list)
      counter = 2
    else :
      counter = 1
    modline = string.replace(change, id_replace, id_random) + keep
    file_out.write(modline)
  else :
    file_out.write(line)

file_out.close()

shutil.move(fname_tmp, fname_in)


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->ZZ at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 12 13 14 15 16' ]

#--------------------------------------------------------------
# ZZ->llvv filter
#--------------------------------------------------------------
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 23
filtSeq.XtoVVDecayFilterExtended.StatusParent = 22
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [11,13,15]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [12,14,16]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-vv + l+l- production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'lkaplan@cern.ch' ]
evgenConfig.inputconfcheck = "ZllH125J_MINLO_VpT"
evgenConfig.process = "qq->ZH, H->llvv, Z->ll"
evgenConfig.minevents   = 500
