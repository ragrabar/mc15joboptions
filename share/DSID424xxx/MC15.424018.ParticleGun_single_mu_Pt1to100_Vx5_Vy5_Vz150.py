evgenConfig.description = "Single muon with flat phi, eta in [-2.6, 2.6], and flat pT [1,100] GeV, Flat Vtx in vx [-5,5], vy [-5,5], vz [-150,150]"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG

# cycle mu+-
genSeq.ParticleGun.sampler.pid = (-13,13)

# flat in pt (1-100 GeV), eta (-2.6 to 2.6) and phi (2pi) 
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[1000, 100000], eta=[-2.6, 2.6])

# flat in Vx (-5mm to 5mm) Vy (-5mm to 5mm) and Vz (-150mm to 150mm)
genSeq.ParticleGun.sampler.pos = PG.PosSampler(x=[-5.,5.], y=[-5.,5.], z=[-150.,150.])


