include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak gammagammajj + 0,1j@LO"
evgenConfig.keywords = ["SM", "2photon", "diphoton", "jets", "VBS"]
evgenConfig.contact  = ["atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch"]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "gammagammajj_EWK"

Sherpa_iRunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3])};

  %tags for process setup
  NJET:=1; QCUT:=20.;

  EXCLUSIVE_CLUSTER_MODE=1
  SOFT_SPIN_CORRELATIONS=1

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)


(processes){
  Process 93 93 -> 22 22 93 93 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  End process;
}(processes)

(selector){
  "PT" 22 20,E_CMS:20,E_CMS:20,E_CMS [PT_UP]
  IsolationCut  22  0.1  2  0.10;
  DeltaRNLO 22 22 0.2 1000.0;
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

Sherpa_iNCores = 96

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
