#Pythia8 minimum bias DD with A3

evgenConfig.description = "DD Minbias with Pythia8 A3"
evgenConfig.keywords = ["minBias","diffraction","DD"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.minevents = 100

#include ("../Pythia8_A2_MSTW2008LO_Common.py")
include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
if  "ForwardProtonFilter" not in filtSeq:
    filtSeq += ForwardProtonFilter()

filtSeq.ForwardProtonFilter.xi_min = 0.00
filtSeq.ForwardProtonFilter.xi_max = 0.20
filtSeq.ForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.ForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.ForwardProtonFilter.pt_max = 1.5*GeV

evgenConfig.findJets = True

from JetRec.JetRecFlags import jetFlags
jetFlags.truthFlavorTags = []
jetFlags.useTracks = False

from JetRec.JetRecStandard import jtm
jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth",
                 modifiersin="none",
                 ptmin=7000.)
        
from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

