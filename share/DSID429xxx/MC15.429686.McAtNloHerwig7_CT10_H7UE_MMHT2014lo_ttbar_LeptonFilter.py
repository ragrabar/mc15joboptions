evgenConfig.description = "McAtNlo+Herwig7 ttbar production with TTbarWToLeptonFilter - CT10 PDF, EE3 tune"
evgenConfig.generators = ["McAtNlo", "Herwig7"]
evgenConfig.keywords = ["top", "ttbar", "lepton"]
evgenConfig.contact  = ["daniel.rauch@desy.de"]
evgenConfig.inputfilecheck = "ttbar"

include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_Common.py")

from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.mg5amc_cmds().splitlines()

include("MC15JobOptions/TTbarWToLeptonFilter.py")


