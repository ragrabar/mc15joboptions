include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )

## Add to commands
cmds = """

## Z+jet
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEZJet

"""

## Set commands
genSeq.Herwigpp.Commands += cmds.splitlines()

evgenConfig.description = "Z + jets with CTEQ6L1 LO PDF and UEEE5 tune"
evgenConfig.keywords = ["SM", "Z", "jets"]
evgenConfig.minevents = 5000
evgenConfig.contact = ["Orel Gueta"]
