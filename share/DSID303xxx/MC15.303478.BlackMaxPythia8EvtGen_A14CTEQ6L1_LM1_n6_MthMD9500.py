evgenConfig.description = "BlackMax + Pythia8 - black hole production"
evgenConfig.process = "BH"
evgenConfig.keywords = ["exotic", "blackhole", "extraDimensions","BSM"]
evgenConfig.generators += ["BlackMax"]
evgenConfig.contact = ["<james.frost@cern.ch>"]
evgenConfig.inputfilecheck = "BlackMax"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")
