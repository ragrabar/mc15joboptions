##############################################################
# Job options fragment for bb->mu3.5mu3.5X 
##############################################################
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description = "bb->mu3.5mu3.5X Mmumu>6 production"
evgenConfig.keywords = ["bottom","2muon","inclusive"]
evgenConfig.minevents = 100

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']    
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.QuarkPtCut = 3.0
genSeq.Pythia8B.AntiQuarkPtCut = 3.0
genSeq.Pythia8B.QuarkEtaCut = 3
genSeq.Pythia8B.AntiQuarkEtaCut = 3
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True
genSeq.Pythia8B.NHadronizationLoops = 40

include("MC15JobOptions/Pythia8B_BPDGCodes.py")

include('MC15JobOptions/MassRangeFilter.py')
filtSeq.MassRangeFilter.PtCut = 3500.
filtSeq.MassRangeFilter.PtCut2 = 3500.
filtSeq.MassRangeFilter.EtaCut = 2.5
filtSeq.MassRangeFilter.EtaCut2 = 2.5
filtSeq.MassRangeFilter.InvMassMin = 6000.
filtSeq.MassRangeFilter.PartId = 13
filtSeq.MassRangeFilter.PartId2 = 13
filtSeq.MassRangeFilter.PartStatus = 1

