include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4j@LO."
evgenConfig.keywords = ["SM", "Z", "2tau", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Ztautau_m10_E_CMS"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  SOFT_SPIN_CORRELATIONS=1
  DECAYFILE=HadronDecaysTauLL.dat
}(run)

(processes){
  Process 93 93 -> 15 -15 93{4};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.05 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 15 -15 10.0 E_CMS
}(selector)
"""

###############

#include('MC15JobOptions/MultiElectronFilter.py')
#filtSeq.MultiElectronFilter.Ptcut = 9
#filtSeq.MultiElectronFilter.Etacut = 2.6
#filtSeq.MultiElectronFilter.NElectrons = 1
from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
filtSeq += MultiElectronFilter()
filtSeq.MultiElectronFilter.Ptcut = 7000
filtSeq.MultiElectronFilter.Etacut = 2.6
filtSeq.MultiElectronFilter.NElectrons = 1
include('MC15JobOptions/MultiMuonFilter.py')
filtSeq.MultiMuonFilter.Ptcut = 7000
filtSeq.MultiMuonFilter.Etacut = 2.6
filtSeq.MultiMuonFilter.NMuons = 1
include('MC15JobOptions/MultiLeptonFilter.py')
filtSeq.MultiLeptonFilter.Ptcut = 17000
filtSeq.MultiLeptonFilter.Etacut = 2.6
filtSeq.MultiLeptonFilter.NLeptons = 1
