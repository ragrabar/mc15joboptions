# JO for Pythia 8 jet jet JZ2 slice

evgenConfig.description = "Dijet truth jet slice JZ2, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["javier.llorente.merino@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 15."]


include("MC15JobOptions/JetFilter_JZ2.py")
evgenConfig.minevents = 5000
filtSeq.QCDTruthJetFilter.SymEta = 1
filtSeq.QCDTruthJetFilter.MinEta = 3.0
