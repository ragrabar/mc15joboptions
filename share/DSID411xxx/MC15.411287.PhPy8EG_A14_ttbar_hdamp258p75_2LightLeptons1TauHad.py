evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, two light leptons + 1 tau hadronic filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'antonio.policicchio@cern.ch']
evgenConfig.minevents   = 5000
evgenConfig.inputFilesPerJob  = 100

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/MultiElecMuTauFilter_2SSLeptons1HadTau.py')
MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
filtSeq.MultiElecMuTauFilter.NLeptons  = 3
filtSeq.MultiElecMuTauFilter.MaxEta = 10.0
filtSeq.MultiElecMuTauFilter.MinPt = 10000.0
filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 15000.0
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = True
filtSeq.MultiElecMuTauFilter.TwoSameSignLightLeptonsOneHadTau = True
