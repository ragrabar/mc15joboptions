#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_ggF_H_Common.py')

evgenConfig.process     = "ggH H->ZZ->llbb"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, H+jet production with NNLOPS and A14 tune, HZZllbb filtered mh=125 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125" ]
evgenConfig.contact     = [ 'christopher.hayes@cern.ch, ljiljana.morvaj@cern.ch' ]
evgenConfig.minevents   = 10000
evgenConfig.inputFilesPerJob = 12
#evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 5 5' ]


from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
filtSeq += XtoVVDecayFilter()
filtSeq.XtoVVDecayFilter.PDGGrandParent = 25
filtSeq.XtoVVDecayFilter.PDGParent = 23
filtSeq.XtoVVDecayFilter.StatusParent = 22
filtSeq.XtoVVDecayFilter.PDGChild1 = [11,13]
filtSeq.XtoVVDecayFilter.PDGChild2 = [5]
