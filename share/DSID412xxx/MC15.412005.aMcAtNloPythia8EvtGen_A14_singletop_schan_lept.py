#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph5_aMC@NLO+Pythia8+EvtGen single-top-quark s-channel production, MadSpin (lept), A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords = [ 'top', 'singleTop', 'sChannel', 'lepton' ]
evgenConfig.contact = ["Marc de Beurs <marcus.de.beurs@cern.ch>"]
evgenConfig.process = "singleTop"

#--------------------------------------------------------------
# Common MG_Control file
#--------------------------------------------------------------
include("MC15JobOptions/MadGraphControl_schan_NLO_Pythia8_A14.py")

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

